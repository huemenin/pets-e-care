<?php

namespace Modules\Owners\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 
use Illuminate\Foundation\Auth\User as Authenticatable;

class Owner extends Authenticatable
{
    use SoftDeletes;
    protected $table = "owners";
    protected $fillable = ['name','email','password']; 
    protected $hidden = ['password']; 
    protected $dates = ['deleted_at'];
}
