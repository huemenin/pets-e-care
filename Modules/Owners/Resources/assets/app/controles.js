     
$(function() { 
  
    
   if($('#owners-table').length){
       
        $('#owners-table').DataTable({
        processing: true,
        serverSide: true,  
        ajax: base_url+"/admin/owners/AdminOwnersList",
        columns: [ 
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' }, 
             { data: 'mobile', name: 'mobile' }, 
             { data: 'created_at', name: 'created_at' }, 
            {
                    data: "null",
                    'searchable': false, 
                    'orderable': false,
                   
                    render: function (data, type, full) 
                    { 
                        
                       var u = '' 
//                               +''+'<a href="'+base_url+'/admin/owners/view/'+full.id+'" class="iconclass"><i class="fa  fa-eye"></i></a>' 
                       +''+'<a href="'+base_url+'/admin/owners/edit/'+full.id+'" class="iconclass"><i class="fa  fa-pencil"></i></a>'
                          +''+'<a href="'+base_url+'/admin/owners/delete/'+full.id+'" class="iconclass"  Onclick="return ConfirmDelete();"><i class="fa  fa-trash"></i></a>';

                        return u;
                    }
            }
        ]
       
   });
    }
/* ************************************************************************* */  
/* *************************** data table listing end ********************** */  
/* ************************************************************************* */  
   
    /*
     * create form   
     */
      
    $("#owner_create").submit(function(e)
    {
        
        e.preventDefault(); 
        
          
        var Name         =   $("[name='name']").val().trim();
        var email        =   $("[name='email']").val().trim();
        var mobile       =   $("[name='mobile']").val().trim();  
        var Status       =   $("[name='Status']").val().trim();
        
        var a=0;
        var b=0;
        var c=0; 
        var d=0; 
        
        
        //Name
        if(Name.length > 0)
        {
            a=1; 
            $( "#Name_Err" ).removeClass( "has-error" );
            $("#Name_Err .help-block").html(' ');
        }
        else{
            a=0; 
            $( "#Name_Err" ).addClass( "has-error" );
            $("#Name_Err .help-block").html('This field is required');
        }
        
        //email
        if(email.length > 0)
        {  
            if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) )
            {
                b=1;  
                $("#Email_Err").removeClass("has-error");
                $("#Email_Err .help-block").html(' ');
            }
            else{
                b=0; 
                $("#Email_Err").addClass("has-error");
                $("#Email_Err .help-block").html('Please enter a valid enail id ');
            }
        }
        else 
        { 
            b=0 
            $("#Email_Err").addClass("has-error");
            $("#Email_Err .help-block").html('This field is required');
        }
        
        
        
         //Name
        if(mobile.length > 0)
        {
            
            if( /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(mobile) )
            {
                c=1; 
                $("#Mobile_Err").removeClass( "has-error" );
                $("#Mobile_Err .help-block").html(' ');  
            }else
            {
                c=0 
                $("#Mobile_Err").addClass("has-error");
                $("#Mobile_Err .help-block").html('Please enter a valid mobile no.');
            } 
        }
        else
        {
            c=1; 
            $( "#Mobile_Err" ).removeClass( "has-error" );
            $("#Mobile_Err .help-block").html(' ');  
        }
        
        
        
        //Status
        if(Status=='0' || Status=='1'){d=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{d=0; $( "#Status_Err" ).addClass( "has-error" );}
        
        
        /* ------------------------------------------------------------------ */
        /* ----------------- form submitting -------------------------------- */
        /* ------------------------------------------------------------------ */
        
        if(a==1 && b==1 && c==1 && d==1)
        {    
               $.ajax({
                    type: "POST",
                    url:base_url+"/admin/owners/create",
                    dataType: "json",
                    async: false, 
                    data: new FormData($('#owner_create')[0]),
                    processData: false,
                    contentType: false, 
                    success: function(response)
                    {     
                         if(response.status==true){window.location.href = response.url; }
                         else{$(".showinfo").html(response.alert);}
                    },
                    error: function (request, textStatus, errorThrown) {

                        var obj = request.responseJSON.errors ;

                        if(obj.hasOwnProperty("email") )
                        {
                            $("#Email_Err").addClass("has-error"); 
                            $("#Email_Err .help-block").html("<div class='mad'>"+request.responseJSON.errors.email[0]+"</div>");   
                        }
                        
                        if(obj.hasOwnProperty("mobile") )
                        {
                            $("#Mobile_Err").addClass("has-error"); 
                            $("#Mobile_Err .help-block").html("<div class='mad'>"+request.responseJSON.errors.mobile[0]+"</div>");   
                        }

                         
                    }
                });
        }
        
        return false;
        
    });
  
/* ************************************************************************* */  
/* *************************** owner create end *********************** */  
/* ************************************************************************* */  
 
 
 /*
     * edit form   
     */
       $("#owner_update").submit(function(e)
    {
        
        e.preventDefault(); 
        
        var action        =   $("#owner_update").attr('action'); 
        var Name         =   $("[name='name']").val().trim();
        var email        =   $("[name='email']").val().trim();
        var mobile       =   $("[name='mobile']").val().trim();  
        var Status       =   $("[name='Status']").val().trim();
        
        var a=0;
        var b=0;
        var c=0; 
        var d=0; 
        
        
        //Name
        if(Name.length > 0)
        {
            a=1; 
            $( "#Name_Err" ).removeClass( "has-error" );
            $("#Name_Err .help-block").html(' ');
        }
        else{
            a=0; 
            $( "#Name_Err" ).addClass( "has-error" );
            $("#Name_Err .help-block").html('This field is required');
        }
        
        //email
        if(email.length > 0)
        {  
            if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) )
            {
                b=1;  
                $("#Email_Err").removeClass("has-error");
                $("#Email_Err .help-block").html(' ');
            }
            else{
                b=0; 
                $("#Email_Err").addClass("has-error");
                $("#Email_Err .help-block").html('Please enter a valid enail id ');
            }
        }
        else 
        { 
            b=0 
            $("#Email_Err").addClass("has-error");
            $("#Email_Err .help-block").html('This field is required');
        }
        
        
        
         //Name
        if(mobile.length > 0)
        {
            
            if( /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(mobile) )
            {
                c=1; 
                $("#Mobile_Err").removeClass( "has-error" );
                $("#Mobile_Err .help-block").html(' ');  
            }else
            {
                c=0 
                $("#Mobile_Err").addClass("has-error");
                $("#Mobile_Err .help-block").html('Please enter a valid mobile no.');
            } 
        }
        else
        {
            c=1; 
            $( "#Mobile_Err" ).removeClass( "has-error" );
            $("#Mobile_Err .help-block").html(' ');  
        }
        
        
        
        //Status
        if(Status=='0' || Status=='1'){d=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{d=0; $( "#Status_Err" ).addClass( "has-error" );}
        
        
        /* ------------------------------------------------------------------ */
        /* ----------------- form submitting -------------------------------- */
        /* ------------------------------------------------------------------ */
        
        if(a==1 && b==1 && c==1 && d==1)
        {    
               $.ajax({
                    type: "POST",
                    url:action,
                    dataType: "json",
                    async: false, 
                    data: new FormData($('#owner_update')[0]),
                    processData: false,
                    contentType: false, 
                    success: function(response)
                    {     
                         if(response.status==true){window.location.href = response.url; }
                         else{$(".showinfo").html(response.alert);}
                    },
                    error: function (request, textStatus, errorThrown) {

                        var obj = request.responseJSON.errors ;

                        if(obj.hasOwnProperty("email") )
                        {
                            $("#Email_Err").addClass("has-error"); 
                            $("#Email_Err .help-block").html("<div class='mad'>"+request.responseJSON.errors.email[0]+"</div>");   
                        }
                        
                        if(obj.hasOwnProperty("mobile") )
                        {
                            $("#Mobile_Err").addClass("has-error"); 
                            $("#Mobile_Err .help-block").html("<div class='mad'>"+request.responseJSON.errors.mobile[0]+"</div>");   
                        }

                         
                    }
                });
        }
        
        return false;
        
    });
     
/* ************************************************************************* */  
/* ****************************** function end ***************************** */  
/* ************************************************************************* */    
   
   
});
