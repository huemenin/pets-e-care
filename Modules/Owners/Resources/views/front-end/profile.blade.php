@extends('website::layouts.master')
@section('title', "pet e care- Owner")
@section('content')
 


  <section class="banner inner">
        <div class="banner-contant">
            <h3>User Profile</h3>
        </div>
        <div class="banner-image">
            <div class="banner-slide-inner">
                <img src="{{asset('public/web/images/banner-inner.jpg')}}" alt="">
            </div>
        </div>
        <div class="banner-gradient"></div>
    </section>
    <section class="inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-sm-12 left-box">
                    <div class="profile-box">
                        <div class="row">
                            <div class="col-lg-3 col-sm-4 profile-pic">
                                <div class="img-box">  
                                    @if(Auth::guard('owner')->user()->image)
                                    @if(file_exists( public_path()."/owners/img/".Auth::guard('owner')->user()->image ))
                                        <img src="{{asset('public/owners/img/'.Auth::guard('owner')->user()->image)}}" alt="">
                                    @else
                                        <img src="{{asset('public/owners/default-profile.png')}}" alt="">
                                    @endif
                                    @else
                                        <img src="{{asset('public/owners/default-profile.png')}}" alt="">
                                    @endif
                                    
                                </div>
                                <div>edit your profile</div>
                            </div>
                            <div class="col-lg-9 col-sm-12 profile-details">
                                <div class="row">
                                    <div class="col-lg-7 col-sm-12">
                                        <h2>{{Auth::guard('owner')->user()->name}}</h2>
                                    </div>
                                    <div class="col-lg-5 col-sm-12 text-right">
                                        <h5>Unique ID: PEC{{Auth::guard('owner')->user()->id}}</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8 col-sm-12">
                                        <span class="desig">3 Pets</span>
                                        <div class="contact-det">
                                                  <span><i class="fas fa-phone"></i> 
                                                @if(Auth::guard('owner')->user()->mobile)
                                                   {{Auth::guard('owner')->user()->mobile}}</span>
                                                @endif
                                                </span>
                                                
                                                 <span><i class="fas fa-envelope"></i>
                                                @if(Auth::guard('owner')->user()->email)
                                                    {{Auth::guard('owner')->user()->email}} 
                                                @endif 
                                                </span>

                                                <span><i class="fas fa-map-marker"></i> 
                                                @if(Auth::guard('owner')->user()->address)
                                                    {{Auth::guard('owner')->user()->address}}
                                                @endif 
                                                </span>
                                                
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4>Description</h4>
                                        <p>
                                         @if(Auth::guard('owner')->user()->about_me)
                                        
                                             {{Auth::guard('owner')->user()->about_me}}
                                           @else
                                           say something about yourself
                                    @endif 
                                    </p>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="profile-box p-0">
                        <div class="tab-section">
                                <div class="tab-head">
                                    <ul class="nav justify-content-center" id="myTab" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" id="tab1-tab" data-toggle="tab" href="#tab1" role="tab" aria-controls="home" aria-selected="true">My Pets</a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" id="tab2-tab" data-toggle="tab" href="#tab2" role="tab" aria-controls="profile" aria-selected="false">My Doctors</a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" id="tab3-tab" data-toggle="tab" href="#tab3" role="tab" aria-controls="contact" aria-selected="false">Enquiries
                                            <span class="new-message"></span>
                                            </a>
                                          </li>
                                        </ul>
                                </div>
                                <div class="tab-contant-main">
                                    <div class="tab-content" id="myTabContent">
                                          <div class="tab-pane fade show active" id="tab1" role="tabpanel" aria-labelledby="tab1-tab">
                                              <div class="dog-list row m-0">
                                                           <div class="col-lg-3 col-sm-12" data-toggle="modal" data-target=".add-pets-proile">
                                                      <div class="profile-list-box add-item">
                                                          <div class="addbox">
                                                              <button>
                                                                  <i class="fas fa-plus"></i>
                                                                  <span>Add Pets</span>
                                                              </button>
                                                          </div>
                                                      </div>
                                                  </div>
<!--                                                  <div class="col-lg-3 col-sm-12">
                                                      <div class="profile-list-box">
                                                          <div class="img-box">
                                                              <img src="{{asset('public/web/images/dog-dp.png')}}" alt="">
                                                          </div>
                                                          <div class="detaile-box">
                                                              <p class="name">Caeser</p>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-3 col-sm-12">
                                                      <div class="profile-list-box">
                                                          <div class="img-box">
                                                              <img src="{{asset('public/web/images/dog-dp.png')}}" alt="">
                                                          </div>
                                                          <div class="detaile-box">
                                                              <p class="name">Caeser</p>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-3 col-sm-12">
                                                      <div class="profile-list-box">
                                                          <div class="img-box">
                                                              <img src="{{asset('public/web/images/dog-dp.png')}}" alt="">
                                                          </div>
                                                          <div class="detaile-box">
                                                              <p class="name">Caeser</p>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-3 col-sm-12">
                                                      <div class="profile-list-box">
                                                          <div class="img-box">
                                                              <img src="{{asset('public/web/images/dog-dp.png')}}" alt="">
                                                          </div>
                                                          <div class="detaile-box">
                                                              <p class="name">Caeser</p>
                                                          </div>
                                                      </div>
                                                  </div>
                                                     <div class="col-lg-3 col-sm-12">
                                                      <div class="profile-list-box">
                                                          <div class="img-box">
                                                              <img src="{{asset('public/web/images/dog-dp.png')}}" alt="">
                                                          </div>
                                                          <div class="detaile-box">
                                                              <p class="name">Caeser</p>
                                                          </div>
                                                      </div>
                                                  </div>-->
                                              </div>  
                                          </div>
                                          <div class="tab-pane fade" id="tab2" role="tabpanel" aria-labelledby="tab2-tab">
                                            <div class="dr-list row m-0">
                                                  <div class="col-lg-3 col-sm-12">
                                                      <div class="profile-list-box">
                                                          <div class="img-box">
                                                              <img src="{{asset('public/web/images/dog-dp.png')}}" alt="">
                                                          </div>
                                                          <div class="detaile-box">
                                                              <p class="name">Caeser</p>
                                                              <span class="desig">test</span>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-3 col-sm-12">
                                                      <div class="profile-list-box">
                                                          <div class="img-box">
                                                              <img src="{{asset('public/web/images/dog-dp.png')}}" alt="">
                                                          </div>
                                                          <div class="detaile-box">
                                                              <p class="name">Caeser</p>
                                                              <span class="desig">test</span>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-3 col-sm-12">
                                                      <div class="profile-list-box">
                                                          <div class="img-box">
                                                              <img src="{{asset('public/web/images/dog-dp.png')}}" alt="">
                                                          </div>
                                                          <div class="detaile-box">
                                                              <p class="name">Caeser</p>
                                                              <span class="desig">test</span>
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div class="col-lg-3 col-sm-12">
                                                      <div class="profile-list-box">
                                                          <div class="img-box">
                                                              <img src="{{asset('public/web/images/dog-dp.png')}}" alt="">
                                                          </div>
                                                          <div class="detaile-box">
                                                              <p class="name">Caeser</p>
                                                              <span class="desig">test</span>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                         </div>
                                        <div class="tab-pane fade" id="tab3" role="tabpanel" aria-labelledby="tab3-tab">
                                            <div class="chat-box row m-0">
                                                <div class="col-lg-4 col-sm-12 chat-list pl-0">
                                                    <ul class="nav d-flex flex-row" id="chat-tab" role="tablist">
                                                      <li class="nav-item">
                                                        <a class="nav-link active d-flex align-self-center" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                                                            <div class="profile-pic">
                                                                <img src="{{asset('public/web/images/profile-dp.png')}}" alt="">
                                                            </div>  
                                                            <div class="profile-summery">
                                                                <p class="name">Jennifer Mullen</p>
                                                                <span class="desig">Veterinarian</span>
                                                                <span class="new-message"></span>
                                                            </div>
                                                        </a>
                                                      </li>
                                                      <li class="nav-item">
                                                        <a class="nav-link d-flex align-self-center" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                                                            <div class="profile-pic">
                                                                <img src="{{asset('public/web/images/profile-dp.png')}}" alt="">
                                                            </div>  
                                                            <div class="profile-summery">
                                                                <p class="name">Jennifer Mullen</p>
                                                                <span class="desig">Veterinarian</span>
                                                            </div>  
                                                        </a>
                                                      </li>
                                                      <li class="nav-item">
                                                        <a class="nav-link d-flex align-self-center" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">
                                                            <div class="profile-pic">
                                                                <img src="{{asset('public/web/images/profile-dp.png')}}" alt="">
                                                            </div>  
                                                            <div class="profile-summery">
                                                                <p class="name">Jennifer Mullen</p>
                                                                <span class="desig">Veterinarian</span>
                                                            </div>  
                                                        </a>
                                                      </li>
                                                    </ul>
                                                </div>
                                                <div class="col-lg-8 col-sm-12 chat-message">
                                                    <div class="tab-content" id="myTabContent">
                                                      <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                                          <div class="chat-message-box">
                                                                <div class="chat-message">
                                                                    <div class="chat-send">
                                                                        <div class="profile-pic">
                                                                            <img src="{{asset('public/web/images/profile-dp.png')}}" alt="">
                                                                        </div>
                                                                        <div class="chat-send-message">
                                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique purus at ante fringilla faucibus. Cras tincidunt lacinia nisi id mollis?</p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="chat-reply">
                                                                        <div class="profile-pic">
                                                                            <img src="{{asset('public/web/images/profile-dp.png')}}" alt="">
                                                                        </div>
                                                                        <div class="chat-reply-message">
                                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tristique purus at ante fringilla faucibus. Cras tincidunt lacinia nisi id mollis?</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="chat-text-area">
                                                                    <div class="profile-pic">
                                                                        <img src="{{asset('public/web/images/profile-dp.png')}}" alt="">
                                                                    </div>
                                                                    <div class="text-area">
                                                                        <textarea name="" id="" cols="30" rows="3" placeholder="Type a reply..."></textarea>
                                                                    </div>
                                                                    <div class="send-btn">
                                                                        <button class="btn gradient-btn">Send</button>
                                                                    </div>
                                                                </div>
                                                          </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                </div>
                            </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 right-box"></div>
            </div>
        </div>
    </section>
    
<!--Add Pets strat-->
            <div class="modal fade update-proile-box add-pets-proile" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <h2 class="head-2">Add Pets</h2>
                        <div class="box-contant">
                            <div class="row pt-3">
                                <div class="col-lg-6 col-sm-12">
                                    <div class="group input-wrap mb-4">
                                        <input type="text" required class="w-100 bg-transparent">
                                        <label>Name</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-sm-12">
                                    <div class="group input-wrap mb-4">
                                        <input type="date" class="w-100">
                                        <label>Age</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-head addpet-box">
                            <div class="left-box">
                                <form id="form1" runat="server">
                                    <input type='file' id="inputFile2" />
                                    <div class="list-box">
                                        <img id="image_upload_preview2" src="{{asset('public/web/images/white.jpg')}} " alt="your image" />
                                    </div>
                                    <div class="image-list-box">
                                        <label for="inputFile2"></label>
                                        <span>Upload photo</span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="text-center">
                            <button class="btn-all">Submit</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--Add Pets end-->



@stop
@section('js') 
<script src="{{asset('public/web/js/slick.min.js')}}"></script>
<script src="{{asset('public/web/js/pets.js')}}"></script>
@stop
