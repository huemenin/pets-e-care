 <!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> Pets e care :: Email verification success </title>
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    <link rel="stylesheet" href="{{asset('public/web/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('public/web/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('public/web/fonts/gotham/stylesheet.css')}}">
    <link rel="stylesheet" href="{{asset('public/web/css/fontawesome-all.css')}}">
    <link rel="stylesheet" href="{{asset('public/web/css/slick.css')}}">  
</head>
<body>
<!--    <div class="container">
        <div class="h-add hedarea">
           <div class="contant">
               <span>Add area</span>
           </div>
       </div>
    </div>-->
    <header>
        <div class="container">
            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand logo" href="#"> <img src="{{asset('public/web/images/logo.png')}}" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     
                </div>
            </nav>
        </div>
    </header>
    <section class="banner inner">
        <div class="banner-contant">
            <h3>Success!</h3>
        </div>
        <div class="banner-image">
            <div class="banner-slide-inner">
                <img src="{{asset('public/web/images/banner-inner.jpg')}}" alt="">
            </div>
        </div>
        <div class="banner-gradient"></div>
    </section>

    <section class="inner">
        <div class="fail-success-box">
            <img src="{{asset('public/web/images/success-icon.png')}}" alt="" width="130px">
            <h3>Email verification success! <br>
            Welcome to Pets E Care.</h3>
            <a href="{{URL('/')}}" class="btn-all">Continue To Login</a> 
        </div>
<!--        <div class="container add-box pb-5">
                <div class="h-add area2 mt-0">
                   <div class="contant">
                       <span>Add area</span>
                   </div>
               </div>    
            </div>-->
    </section>
  
    
     
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/slick.min.js"></script>
    <script src="js/pets.js"></script>
</body>

</html>