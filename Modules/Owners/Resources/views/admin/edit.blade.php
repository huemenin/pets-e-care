@extends('admin::admin.master')
@section('title', "Update Owner")
 
@section('content')

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small style="font-weight: bold;">Update Owner</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{URL('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li  class="active"><a href="{{URL('/admin/owners/')}}">Owners</a></li>
          <li  class="active"><a href="javascript:void(0)">Update Owner</a></li>
          
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="showinfo"></div>
      <!-- Default box -->
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
                <form method="post" autocomplete="off" name="owner_create" id="owner_update" action="{{URL('/').'/admin/owners/update/'.$owners->id}}"   enctype="multipart/form-data">
                    <div class="row"> 
                <!-- full column -->
                        <div class="col-md-12">
                            <!-- Name has-error-->
                            <div class="col-md-6">
                                <div class="form-group " id="Name_Err">
                                    <label>Name<span class="mad">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Name" name="name" id="name" value="{{$owners->name}}" >
                                     <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Name -->
                            
                            <!--Email-->
                            <div class="col-md-6">
                                <div class="form-group" id="Email_Err">
                                    <label>Email<span class="mad">*</span></label>
                                    <input   type="text" class="form-control" placeholder="Enter Email" name="email" id="email" value="{{$owners->email}}" >
                                    <span class="help-block"></span>
                                 </div>
                            </div>
                            <!-- /.Email -->
                            
                            <!--Mobile-->
                            <div class="col-md-6">
                                <div class="form-group" id="Mobile_Err">
                                    <label>Mobile</label>
                                    <input   type="text" class="form-control" placeholder="Enter Mobile" name="mobile" id="mobile"  value="{{$owners->mobile}}" >   
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Mobile --> 
                            
                            <!--Status -->
                            <div class="col-md-6">
                                <div class="form-group" id="Status_Err"  >
                                    <label>Status</label>
                                    <select class="form-control" style="width: 100%;" name="Status" id="Status">
                                             <option value="1" {{($owners->status==1)?'selected':''}} >Active</option>
                                        <option value="0" {{($owners->status==0)?'selected':''}}>Inactive</option>
                                   
                                    </select>
                                    <span class="help-block"><br/></span>
                                </div>
                            </div>
                            <!-- /.Status --> 
                           
                            <!--About me-->
                            <div class="col-md-6">
                                <div class="form-group"  >
                                    <label>About me</label>
                                    <textarea cols="3" name="about_me" rows="5"  class="form-control" placeholder="Enter About Yourself" style="resize: none"> {{$owners->about_me}}</textarea>
                                 </div>
                            </div>
                            <!-- /.About me -->
                            
                            <!--Address-->
                            <div class="col-md-6">
                                <div class="form-group"  >
                                    <label>Address</label>
                                    <textarea cols="3" rows="5" name="address" class="form-control" placeholder="Enter Your Address" style="resize: none">{{$owners->address}}</textarea>
                                 </div>
                            </div>
                            <!-- /.Address -->
                             
                            <!--Profile Pic -->
                            <div class="col-md-6">
                                <div class="form-group col-md-4" id="Status_Err"  >
                                    <label>Profile Pic</label>
                                     <input name="ProfileImage" type="file"  >
                                </div>
                                <div class="form-group col-md-2">
                                    <img src="{{URL('public/owners/img/').'/'.$owners->image}}" style="margin-left: 15px;width: 60px;height: auto" alt=""/>
                                </div>
                                
                            </div>
                            <!-- /.Profile Pic -->
                            
                            <button type="submit"  class="btn btn-info pull-right" style="margin-top: 20px;border:  none;">Submit</button>         
                            
                        </div>
                <!--/.col (full) -->
                   
                    </div> 
               </form>
            </div>
            <!-- /.box-body -->
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@stop

@section('js')
<!-- controls -->
 <script src="{{asset('Modules/Owners/Resources/assets/app/controles.js')}}"></script>
@stop
 