<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owners', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('name',255)->collation('utf8mb4_unicode_ci')->nullable(false);
            $table->string('email',255)->collation('utf8mb4_unicode_ci')->nullable(false);
            $table->string('email_verification_id',255)->nullable();
            $table->integer('is_email')->default('0')->comment('0-not verified,1-verified')->nullable(false);
            $table->string('mobile',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('password',255)->collation('utf8mb4_unicode_ci')->nullable(false);
            $table->string('image',255)->collation('utf8mb4_unicode_ci')->nullable(); 
            $table->text('about_me')->collation('utf8mb4_unicode_ci')->nullable();
            $table->text('address')->collation('utf8mb4_unicode_ci')->nullable();
            $table->tinyInteger('status')->default('0')->comment('0-inactive , 1-active')->nullable(false);
            $table->integer('is_admin')->unsigned()->comment('1-if admin created the owner')->nullable();
            $table->foreign('is_admin')->references('id')->on('admin_users');
            $table->string('created_ip',255)->collation('utf8mb4_unicode_ci')->nullable(false);
            $table->string('deleted_ip',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('remember_token',100)->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('owners');
    }
}
