<?php

Route::group(['middleware' => 'web', 'prefix' => 'admin/owners', 'namespace' => 'Modules\Owners\Http\Controllers'], function()
{
   
       
    /* logged users opertaions */
    Route::group(['middleware' =>  'admin_auth:admin'], function()
    {
        Route::get('/', 'AdminOwnersController@index');
        Route::get('/AdminOwnersList',[ 'middleware' => 'check_perm:view-owner', 'uses' => 'AdminOwnersController@AllOwners', ]); 

        Route::get('/add', ['middleware' => 'check_perm:create-owner', 'uses' => 'AdminOwnersController@create']);
        Route::post('/create', ['middleware' => 'check_perm:create-owner', 'uses' => 'AdminOwnersController@store']);
        Route::get('/edit/{id}', ['middleware' => 'check_perm:edit-owner', 'uses' => 'AdminOwnersController@edit']);
        Route::post('/update/{id}', ['middleware' => 'check_perm:edit-owner', 'uses' => 'AdminOwnersController@update']);
//        Route::get('/view/{id}', ['middleware' => 'check_perm:view-admin', 'uses' => 'AdminAssignController@show']);      
        Route::get('/delete/{id}', ['middleware' => 'check_perm:delete-admin', 'uses' => 'AdminOwnersController@destroy']); 
       
        
    });
    
  
    
});


//front user
Route::group(['middleware' => 'web', 'prefix' => 'owners', 'namespace' => 'Modules\Owners\Http\Controllers'], function()
{
        /* website users opertaions */
        Route::post('/register', 'OwnersController@store');
        Route::get('/verification/{id}', 'OwnersController@verify');
        Route::post('/login', 'OwnersController@login');
        Route::get('/logout', 'OwnersController@logout');
        
        /* logged  owner users opertaions */
    Route::group(['middleware' =>  'owner_auth:owner'], function()
    {   
        
        Route::get('/', 'OwnersController@index');   
        
    });
    
});
