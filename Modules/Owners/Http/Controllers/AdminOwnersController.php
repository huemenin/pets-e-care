<?php

namespace Modules\Owners\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Modules\Owners\Entities\Owner;
use \Illuminate\Support\Facades\Session;
class AdminOwnersController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('owners::admin.index');
    }

     /**
     * Display a listing of the resource.
     * @return Response
     */
    public function AllOwners()
    {
        return Datatables::of(Owner::orderBy('id', 'DESC')->get())->make(true);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('owners::admin.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {   
         
        if ($request->input('mobile'))  { $rules = ['email' => 'required|unique:owners,email,NULL,id','mobile' => 'required|unique:owners,mobile,NULL,id'];}
        else { $rules = ['email' => 'required|unique:owners,email,NULL,id']; }
        $this->validate($request, $rules); 
        
        $owner                  =   new Owner;
        $owner->name            =   $request->name; 
        $owner->email           =   $request->email; 
        $owner->mobile          =   $request->mobile; 
        $password               =   str_random(6);
        $owner->password        =   bcrypt($password);  
        if ($request->hasFile('ProfileImage'))
        {
            $image = $request->file('ProfileImage');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/owners/img');
            $image->move($destinationPath, $name);   
            $owner->image =$name;
        }
        $owner->about_me        =   $request->about_me; 
        $owner->address         =   $request->address; 
        $owner->status          =   $request->Status; 
        $owner->is_admin        =   1; 
        $owner->created_ip      =   $request->ip(); 
                
        
        try
        {
            $owner->save();
            // send email with password to owner
            
            
            $request->session()->flash('val', 1);
            $request->session()->flash('msg', "Owner created successfully !");
            return response()->json(['status'=>true,'url'=>URL('/admin/owners/'),'csrf' => csrf_token()]);
        }
        catch (\Exception $e)
        {
            $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
            return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);
   
        }
        
        
        
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('owners::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $owners = Owner::find($id);
        if($owners==null){return redirect('/admin/404');}
        else{return view('owners::admin.edit',compact('owners'));  } 
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id,Request $request)
    {
        
        if ($request->input('mobile'))  { $rules = ['email' => "required|unique:owners,email,$id,id",'mobile' => "required|unique:owners,mobile,$id,id"];}
        else { $rules = ['email' => "required|unique:owners,email,$id,id"]; }
        $this->validate($request, $rules); 
        
        $owner                  =   Owner::find($id);
        if($owner==null){return response()->json(['status'=>true,'url'=>URL('/admin/404'),'csrf' => csrf_token()]);}
        else
        {
            $owner->name            =   $request->name; 
            $owner->email           =   $request->email; 
            $owner->mobile          =   $request->mobile;   
            if ($request->hasFile('ProfileImage'))
            {
                $image = $request->file('ProfileImage');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/owners/img');
                $image->move($destinationPath, $name);   
                $owner->image =$name;
            }
            $owner->about_me        =   $request->about_me; 
            $owner->address         =   $request->address; 
            $owner->status          =   $request->Status; 
            $owner->is_admin        =   1; 
            $owner->created_ip      =   $request->ip();  
            try
            {
                $owner->save();
                // send email with password to owner


                $request->session()->flash('val', 1);
                $request->session()->flash('msg', "Owner created successfully !");
                return response()->json(['status'=>true,'url'=>URL('/admin/owners/'),'csrf' => csrf_token()]);
            }
            catch (\Exception $e)
            {
                $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
                return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);

            }
        }
        
        
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $Owner = Owner::find($id);
        if($Owner==null){return redirect('/admin/404');}
        else
        {
            try
            { 
                $Owner->delete();
                Session::flash('val', 1);
                Session::flash('msg', "Admin user deleted successfully !");

             } catch (Exception $ex) {
                Session::flash('val', 1);
                Session::flash('msg', $ex->getMessage());
             }
            return redirect('/admin/owners');
        }
    }
}
