<?php

namespace Modules\Owners\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Owners\Entities\Owner;
use \Crypt;
use Dirape\Token\Token;
use Mail;
use URL;
use Config;
use Auth;
use Cache;
use Session;
class OwnersController extends Controller
{
     /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('owners::front-end.profile');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function login(Request $request)
    {
        if($request->type)
        {
            if($request->type == 'owner')
            {
                 $this->validate($request, [
                    'logEmail' => 'required|email',
                    'logPassword' => 'required'
                ]);

            
                $credentials['status']  =   1;
                $credentials['is_email']  =   1; 
                $credentials['email']  =   $request->logEmail;
                $credentials['password']  =   $request->logPassword;
               
                if (Auth::guard('owner')->attempt($credentials, $request->has('remember')))
                {
                    Auth::guard('doctor')->logout();
                    Cache::flush();
                    Session::put('user_type','owner');
                    return response()->json(['status'=>1,'csrf' => csrf_token(),'url'=>URL::to('/owners/')]);   
                }
                else
                {
                    return response()->json(['status' => 0, 'message' => trans('owners::auth.failed')]); 
                }
                
            }else{return response()->json(['status'=>2,'url'=>URL('/404'),'csrf' => csrf_token()]);}
                 
            
        }else
        {
            return response()->json(['status'=>2,'url'=>URL('/404'),'csrf' => csrf_token()]);
        }
    }
    
    
    /**
     * logout for admin users.
     * @return Response
     */
    public function logout()
    {
        Auth::guard('owner')->logout();
        Cache::flush();
        Session::flush();
        return redirect('/');
    }
    
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
 
        if($request->type)
        {
            if($request->type == 'owner')
            {
                $this->validate($request, [ 'email' => 'required|unique:owners,email,NULL,id,deleted_at,NULL']);
                $owner                  =   new Owner;
                $owner->name            =   $request->name; 
                $owner->email           =   $request->email; 
                $owner->password        =   bcrypt($request->password);
                $owner->status          =   0; 
                $owner->created_ip      =   $request->ip(); 
                $Tokens                 =   new Token();
                $token                  =  $Tokens->Random(16);
                $owner->email_verification_id	=   $token;
                try
                {
                    $owner->save();
                    $id = $owner->id;
                    $toEmail=   $request->email; 
                    $contactName='';
                  /* send verification email */
                    $encrypted = Crypt::encrypt($token."==*==".$id);
                    $data=array('key' => $encrypted,'name'=>$request->name);
                    Mail::send('owners::emails.verification',  $data, function($message) use ($toEmail, $contactName)
                    {
                        $message->from(Config::get('constants.from'), Config::get('constants.mail_header'));
                        $message->to($toEmail);
                        $message->subject(Config::get('constants.subject_owner_reg'));

                    });
                    if( count(Mail::failures()) > 0 ) 
                    { 
                         
                        return response()->json(['status'=>0,'message'=>"some problem occured to send verification link to your email id",'csrf' => csrf_token()]);
                    } 
                    else {
                           $message = (String) view('owners::front-end.reg_success');                    
                           return response()->json(['status'=>1,'message'=>$message,'csrf' => csrf_token()]); }

                } 
                catch (Exception $ex)
                { 
                    $message="Sorry Somthing went wrong, please try again later"; 
                    return response()->json(['status'=>0,'message'=>$message,'csrf' => csrf_token()]);
                }
            }else{return response()->json(['status'=>2,'url'=>URL('/404'),'csrf' => csrf_token()]);}
        }else
        {
            return response()->json(['status'=>2,'url'=>URL('/404'),'csrf' => csrf_token()]);
        }
        
         
    }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function verify($id)
    {
        $decrypted = Crypt::decrypt($id); 
        $key_id = explode('==*==', $decrypted, 2);
        $owners = Owner::where('email_verification_id',$key_id[0])->find($key_id[1]);
       
        if($owners != null)
        { 
           
            if($owners->is_email == 0)
            {
                
                try{ 
                    $owners->is_email = 1;
                    $owners->status = 1;
                    $owners->save();
                    return view('owners::front-end.verification_success');
                  
                } catch (Exception $ex) {
                    echo $ex->getMessage();
                    return view('owners::front-end.verification_fail'); 
                }
               
            }else
            {
                return view('owners::front-end.verification_fail'); 
            }
        }
        else{     return redirect(URL('/404')); }
    }
    
    
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('owners::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('owners::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
