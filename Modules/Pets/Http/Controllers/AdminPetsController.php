<?php

namespace Modules\Pets\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Pets\Entities\Pets;
use Modules\Owners\Entities\Owner;
use Modules\Pets\Entities\PetsGallery;
use Yajra\DataTables\DataTables;
use \Illuminate\Support\Facades\Session;
use DB;
use Carbon\Carbon;


class AdminPetsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('pets::admin.index'); 
    }
     /**
     * Display a listing of the resource.
     * @return Response
     */
    public function allPets()
    {
        return Datatables::of(Pets::orderBy('id', 'DESC')->get())->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {  
        return view('pets::admin.create');
         
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
         
          $this->validate($request,[
                      'name' => 'required',
                       'dob' => 'required',
                   ]);
          
          $Owners=Owner::find($request->owner_id);
         if($Owners != null)
         {
                $tableStatus = DB::select("SHOW TABLE STATUS LIKE '".DB::getTablePrefix()."pets'");
                if (empty($tableStatus)) {
                    throw new \Exception("Table not found");
                }else
                {
                    $pets                =   new Pets;
                    $pets->name          =   $request->name; 
                    $pets->dob = Carbon::parse($request->dob);
                    $pets->owner_id      =   $request->owner_id;
                    $nextId = $tableStatus[0]->Auto_increment;

                    $path=public_path().'/pets/'.$nextId.'/profile/';
                    if (!file_exists($path)) {mkdir($path, 0777, true);}

                    if($request->file('ProfileImage'))
                    {
                        $extension = $request->file('ProfileImage')->getClientOriginalExtension();
                        $imagename = $nextId.time().'.'.$extension;
                        $destinationPath = $path;
                        $request->file('ProfileImage')->move($destinationPath, $imagename);
                        $pets->cover_pic           =  $imagename;
                    }

                    $Gpath=public_path().'/pets/'.$nextId.'/gallery/';
                    if (!file_exists($Gpath)) {mkdir($Gpath, 0777, true); }

                    /* upload gallery images */
                    if($request->file('multiFiles'))
                    {
                        foreach ($request->file('multiFiles')  as $gimage)
                        {

                            $extension = $gimage->getClientOriginalExtension();
                            $imagename = time().'_' . rand(100, 999) .'.'.$extension;
                            $destinationPath = $Gpath;
                            $gimage->move($destinationPath, $imagename);
                            $pets_gallery               =  new PetsGallery;
                            $pets_gallery->file         =  $imagename;
                            $pets_gallery->pet_id       =  $nextId;
                            $pets_gallery->save();
                        }
                    }

                        try
                        {
                            $pets->save();
                            $request->session()->flash('val', 1);
                            $request->session()->flash('msg', "pet created successfully !");
                            return response()->json(['status'=>true,'url'=>URL('/admin/module/'),'csrf' => csrf_token()]);
                        }
                        catch (\Exception $e)
                        {
                            $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
                            return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);

                        }
                }
         }
        
           
//           if($request->file('images')){
//                $extension = $request->file('images')->getClientOriginalExtension();
//                $imagename = time().'.'.$extension;
//                $destinationPath = public_path() . "/images/properties/";
//                $request->file('images')->move($destinationPath, $imagename);
//                $propertylists->images4property()->attach($imagename, ['is_featured' => 1]);
//            }
           
           /* upload gallery images */
//           if($request->file('multiFiles')){
//                foreach ($request->file('multiFiles')  as $gimage) {
//                    $extension = $gimage->getClientOriginalExtension();
//                    $imagename = time().'_' . rand(100, 999) .'.'.$extension;
//                    $destinationPath = public_path() . "/pets/properties/";
//                    $gimage->move($destinationPath, $imagename);
//                    $propertylists->images4property()->attach($imagename, ['is_featured' => 0]);
//                }
//            }
          
    }
 

public function autoComplete(Request $request) {
        $query = $request->get('term','');
        
        $Owners=Owner::where('email','LIKE','%'.$query.'%')->orWhere('name','LIKE','%'.$query.'%')->get();
        
        $data=array();
        foreach ($Owners as $Owner) {
                $data[]=array('value'=>$Owner->name,'id'=>$Owner->id);
        }
        if(count($data))
             return $data;
        else
            return ['value'=>'No Result Found','id'=>''];
    }
    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('pets::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('pets::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
