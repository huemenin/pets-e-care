<?php
//
//Route::group(['middleware' => 'web', 'prefix' => 'pets', 'namespace' => 'Modules\Pets\Http\Controllers'], function()
//{
//    Route::get('/', 'PetsController@index');
//});
Route::group(['middleware' => 'web', 'prefix' => 'admin/pets', 'namespace' => 'Modules\Pets\Http\Controllers'], function()
{
   
       
    /* logged users opertaions */
    Route::group(['middleware' =>  'admin_auth:admin'], function()
    {
        Route::get('typeahead-response',array('as'=>'typeahead.response','uses'=>'AdminPetsController@autoComplete'));
        Route::get('/', 'AdminPetsController@index');
        Route::get('/AdminPetsList',[ 'middleware' => 'check_perm:view-owner', 'uses' => 'AdminPetsController@allPets', ]); 
//
        Route::get('/add', ['middleware' => 'check_perm:create-pets', 'uses' => 'AdminPetsController@create']);
        Route::post('/create', ['middleware' => 'check_perm:create-pets', 'uses' => 'AdminPetsController@store']);
//        Route::get('/edit/{id}', ['middleware' => 'check_perm:edit-owner', 'uses' => 'AdminOwnersController@edit']);
//        Route::post('/update/{id}', ['middleware' => 'check_perm:edit-owner', 'uses' => 'AdminOwnersController@update']);
////        Route::get('/view/{id}', ['middleware' => 'check_perm:view-admin', 'uses' => 'AdminAssignController@show']);      
//        Route::get('/delete/{id}', ['middleware' => 'check_perm:delete-admin', 'uses' => 'AdminOwnersController@destroy']); 
//       
        
    });
    
  
    
});