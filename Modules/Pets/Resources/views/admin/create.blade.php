@extends('admin::admin.master')
@section('title', "Admin Create Pets")
@section('css') 
 <link rel="stylesheet" href="{{asset('public/web/css/datepicker.css')}}">  
 <link rel="stylesheet" href="{{asset('public/css/jquery-ui.css')}}"> 
 
 @stop
@section('content')

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small style="font-weight: bold;">Create Pets</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{URL('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li  class="active"><a href="{{URL('/admin/pets/')}}">Pets</a></li>
          <li  class="active"><a href="javascript:void(0)">Create Pets</a></li>
          
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="showinfo"></div>
      <!-- Default box -->
        <form method="post" autocomplete="off" name="Pets" id="Pets" action="{{URL('/pets/create')}}"  enctype="multipart/form-data">
                   
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
                   <div class="row"> 
                <!-- full column -->
                        <div class="col-md-12">
                            <!-- Name has-error-->
                            <div class="col-md-6">
                                <div class="form-group " id="ownerErr">
                                    <label>pet owner<span class="mad">*</span></label>
                                    <input type="text" class="form-control" placeholder="owner" name="owner" id="owner">
                                    <input type="hidden"  name="owner_id" id="owner_id">
                                    <div id="NameCheck"></div>
                                </div>
                            </div>
                            <!-- /.Name -->
                            
                            <!-- Name has-error-->
                            <div class="col-md-6">
                                <div class="form-group " id="Name_Err">
                                    <label>Name<span class="mad">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Name" name="name" id="name">
                                    <div id="NameCheck"></div>
                                </div>
                            </div>
                            <!-- /.Name -->
                            
                            <!--DOB-->
                            <div class="col-md-6">
                                <div class="form-group" id="dob_Err">
                                    <label>DOB<span class="mad">*</span></label>
                                    <input   type="text" class="form-control" placeholder="date of birth" name="dob" id="dob">
                                    <div id="DOBCheck"></div>
                                 </div>
                            </div>
                            <!-- /.DOB -->
                            
                             <!--Profile image-->
                            <div class="col-md-6">
                                <div class="form-group" id="icon_Err">
                                    <label>Cover Pic ( min size 800*350 )</label>
                                    <input type="file" class="form-control"  name="ProfileImage" id="ProfileImage" >
 
                                 </div>
                            </div>
                            <!-- /.Profile image -->
                            
                             <!--Gallery-->
                            <div class="col-md-6">
                                <div class="form-group" id="icon_Err">
                                    <label>Gallery</label>
                                    <input type="file" class="form-control"  name="multiFiles[]" id="multiFiles" multiple="multiple" >
 
                                 </div>
                            </div>
                            <!-- /.Gallery -->
                            <button type="submit"  class="btn btn-info pull-right" style="margin-top: 20px;border:  none;">Submit</button>         
                            
                        </div>
                <!--/.col (full) -->
                   
                    </div> 
              
            </div>
            <!-- /.box-body -->
        </div>
             
       </form>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@stop 
@section('js')
<!-- controls -->
<script src="{{asset('public/web/js/bootstrap-datepicker.js')}}"></script>  
<script src="{{asset('public/js/jquery-ui.js')}}"></script>  
<script src="{{asset('Modules/Pets/Resources/assets/app/controles.js')}}"></script>
<script type="text/javascript"> var url = "{{ route('typeahead.response') }}"; </script>
@stop
 