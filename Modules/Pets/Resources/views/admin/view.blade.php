@extends('admin::admin.master')
@section('title', "Admin View Permissions")
 
@section('content')

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small style="font-weight: bold;">View Modules</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{URL('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="{{URL('/admin/module/')}}">Modules</a></li>
          <li  class="active"><a href="javascript:void(0)">View Modules</a></li>
          
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="showinfo"></div>
      <!-- Default box -->
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
                     <div class="row">
                <!-- left column -->
                        <div class="col-md-12">
                             <!-- Name-->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input type="text" disabled="true" class="form-control" value="{{$modules->module_name}}">
                                </div>
                            </div>
                            <!-- /.Name -->

                            <!--Slug-->
                            <div class="col-md-6">
                                <div class="form-group" >
                                    <label>Slug</label>
                                    <input  disabled="true" type="text" class="form-control"  value={{$modules->slug}}>
                                 </div>
                            </div>
                            <!-- /.Slug -->
                            
                            
                            <!--Status -->
                            <div class="col-md-6">
                                <div class="form-group" id="Status_Err"  >
                                    <label>Status</label>
                                    @if($modules->status==1)
                                        <input type="text" disabled="true" class="form-control" value="Active">
                                    @endif

                                    @if($modules->status==0)
                                        <input type="text" disabled="true" class="form-control" value="Inactive">
                                    @endif
                                </div>
                            </div>
                            <!-- /.Status -->

                            <!--Icon-->
                            <div class="col-md-6">
                                <div class="form-group" id="icon_Err">
                                    <label>Icon </label>
                                    <input   type="text" disabled="true" class="form-control" placeholder="Module Icon" name="module_icon" value="{{$modules->icon}}" id="module_icon">
                                 </div>
                            </div>
                            <!-- /.Icon -->
                            
                        </div>
                <!--/.col (left) -->
 
                    </div> 
                    
            </div>
            <!-- /.box-body -->
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@stop

 
 