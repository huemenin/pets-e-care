@extends('admin::admin.master')
@section('title', "Pets")

@section('css')
<link rel="stylesheet" href="{{asset('public/admin/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css ')}}">
@stop

@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pets
        <!--<small>it all starts here</small>-->
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li  class="active"><a href="{{URL('/admin/pets/')}}">Pets</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
            
@if(Session::has('val')) 
    @if(Session::get('val')==1)
       <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4><i class="icon fa fa-check"></i> Success!</h4>
                {{Session::get('msg')}}
        </div>
    @endif
    
     @if(Session::get('val')==0)
    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"> Alert!</i></h4> 
                         {{Session::get('msg')}}
                          </div>
     @endif
   
@endif
      <!-- Default box -->
      <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
           
                <table class="table table-bordered" id="modules-table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th> 
                            <th>Operations</th>
                        </tr>
                    </thead>
                </table>
 
          </div>
            <!-- /.box-body -->
          </div>
      <!-- /.box -->
 <a href="{{ URL::to('admin/pets/add') }}"><img src="{{ URL::asset('Modules/Admin/Resources/Assets/add.png') }}" id="fixedbutton"></a>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@stop
@section('js')
<!-- DataTables -->
<script src="{{asset('public/admin/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/admin/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('Modules/Pets/Resources/assets/app/controles.js')}}"></script>
@stop
 @push('scripts')
 <style>
     .iconclass{margin-left: 2em;font-size: 20px}
  </style>
@endpush