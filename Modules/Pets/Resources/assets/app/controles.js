     
$(function() {
    
   if($('#dob').length){
   $("#dob").datepicker( {format: "dd/mm/yyyy"});
   }
    
   if($('#image1').length){
       
    $('#image1').cropper({
    aspectRatio: 16 / 9
  });
  if($('#inputImage').length){
   $("#inputImage").on('change', function(){
  		var file = $(this).get(0).files[0];
  		$("#image1").attr('src',URL.createObjectURL(file));
  		var image = document.getElementById('image1');
                var filename = $("input[name='file']").val().replace(/C:\\fakepath\\/i, '');
                $('#image1').data('name',filename);
          
                $().cropper('destroy');
  		$('#image1').cropper({
		  aspectRatio: 16 / 9
//                  
		});
  		
  	});
        }
}
 if($('#owner').length){
$("#owner").autocomplete({
        source: function(request, response) {
            $.ajax({
                url: url,
                dataType: "json",
                data: {
                    term : request.term
                },
                success: function(data) {
                    response(data);
                   
                }
            });
        },select: function (e, ui) {
            $("#owner_id").val(ui.item.id)
        
    },
        minLength: 1,
       
    });
 
     }
    
    
    
   if($('#modules-table').length){
       
        $('#modules-table').DataTable({
        processing: true,
        serverSide: true,  
        ajax: base_url+"/admin/pets/AdminPetsList",
        columns: [ 
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' }, 
            {
                    data: "null",
                    'searchable': false, 
                    'orderable': false,
                   
                    render: function (data, type, full) 
                    { 
                         var u='';
//                       var u = '<a href="'+base_url+'/admin/pets/view/'+full.id+'" class="iconclass"><i class="fa  fa-eye"></i></a>' 
//                              +''+'<a href="'+base_url+'/admin/pets/edit/'+full.id+'" class="iconclass"><i class="fa  fa-pencil"></i></a>'
//                              +''+'<a href="'+base_url+'/admin/pets/delete/'+full.id+'" class="iconclass"  Onclick="return ConfirmDelete();"><i class="fa  fa-trash"></i></a>';

                        return u;
                    }
            }
        ]
       
   });
    }
/* ************************************************************************* */  
/* *************************** data table listing end ********************** */  
/* ************************************************************************* */  
   
    /*
     * create form 
     * params : Name,dob,gallery,profile pic  
     */
      
    $("#Pets").submit(function(e)
    {
        e.preventDefault();  
        var name                =   $("[name='name']").val().trim(); 
        var DateOfBirth         =   $("[name='dob']").val().trim();
        var owner               =   $("[name='owner']").val().trim();
     
        var a=0;
        var b=0; 
        var c=0; 
        
        //Name
        if(name.length > 0){a=1; $( "#Name_Err" ).removeClass( "has-error" );}
        else{a=0; $( "#Name_Err" ).addClass( "has-error" );}
        
        //DateOfBirth
        if(DateOfBirth.length >0)
        {
            var regex1 = /^(\d{1,2})\/(\d{1,2})\/(\d{4})$/;
            if(regex1.test(DateOfBirth)==true) { b=1; $( "#dob_Err" ).removeClass( "has-error" ); }
            else { b=0; $( "#dob_Err" ).addClass( "has-error" ); } 
        } else { b=0; $( "#dob_Err" ).addClass( "has-error" ); }
            
      //owner
        if(owner.length > 0){c=1; $( "#ownerErr" ).removeClass( "has-error" );}
        else{c=0; $( "#ownerErr" ).addClass( "has-error" );}
        
//        
//        /* ------------------------------------------------------------------ */
//        /* ----------------- form submitting -------------------------------- */
//        /* ------------------------------------------------------------------ */
//        
        if(a==1 && b==1 && c==1 )
        {
           
               $.ajax({
                    type: "POST",
                    url:base_url+"/admin/pets/create",
                    dataType: "json",
                    async: false, 
                    data: new FormData($('#Pets')[0]),
                    processData: false,
                    contentType: false, 
                    success: function(response)
                    {     
//                         if(response.status==true){window.location.href = response.url; }
//                         else{$(".showinfo").html(response.alert);}
                    },
                    error: function (request, textStatus, errorThrown) {

//                        var obj = request.responseJSON.errors ;
//
//                        if(obj.hasOwnProperty("module_name") )
//                        {
//                           $( "#Name_Err" ).addClass( "has-error" );
//                           $("#NameCheck").html("<div class='mad'>"+request.responseJSON.errors.module_name[0]+"</div>");   
//                        }
//
//                        if(obj.hasOwnProperty("module_slug") )
//                        {
//                            $( "#Slug_Err" ).addClass( "has-error" );
//                            $("#SlugCheck").html("<div class='mad'>"+request.responseJSON.errors.module_slug[0]+"</div>");   
//                        } 
                    }
                });
        }
        
        return false;
        
    });
  
/* ************************************************************************* */  
/* *************************** module create end *********************** */  
/* ************************************************************************* */  
     
     $("#module_name").keyup(function() {
        $("#module_slug").val(generate_slug($("#module_name").val()));
    });
    

/* ************************************************************************* */  
/* *************************** generate slug end *************************** */  
/* ************************************************************************* */  
 
 
 /*
     * edit form 
     * params : Name,Status,Slug  
     */
      
    $("#module_update").submit(function(e)
    {   e.preventDefault();
         
        var action=   $("#module_update").attr('action');

        var Status       =   $("[name='Status']").val().trim();
        var Name         =   $("[name='module_name']").val().trim();
        var Slug         =   $("[name='module_slug']").val().trim(); 
        var icon         =   $("[name='module_icon']").val().trim(); 
        
        var a=0;
        var b=0;
        var c=0; 
        var d=0;
  
        //Name
        if(Name.length > 0){a=1; $( "#Name_Err" ).removeClass( "has-error" );}
        else{a=0; $( "#Name_Err" ).addClass( "has-error" );}
        
        //Status
        if(Status=='0' || Status=='1'){b=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{b=0; $( "#Status_Err" ).addClass( "has-error" );}
        
        //Name
        if(Slug.length > 0){c=1; $( "#Slug_Err" ).removeClass( "has-error" );}
        else{c=0; $( "#Slug_Err" ).addClass( "has-error" );}
        
        //Icon
        if(icon.length > 0){d=1; $( "#icon_Err" ).removeClass( "has-error" );}
        else{d=0; $( "#icon_Err" ).addClass( "has-error" );}
        
        
        /* ------------------------------------------------------------------ */
        /* ----------------- form submitting -------------------------------- */
        /* ------------------------------------------------------------------ */
        
        if(a==1 && b==1 && c==1  && d==1)
        {
            
            $("#NameCheck").html(" ");  
            $("#SlugCheck").html(" "); 
            $(".showinfo").html(" ");
            $( "#Name_Err" ).removeClass( "has-error" );
            $( "#Slug_Err" ).removeClass( "has-error" );
            
               $.ajax({
                type: "POST",
                url:action,
                dataType: "json",
                async: false, 
                data: new FormData($('#module_update')[0]),
                processData: false,
                contentType: false, 
                success: function(response)
                {     
                     if(response.status==true){window.location.href = response.url; }
                     else{$(".showinfo").html(response.alert);}
                },
                error: function (request, textStatus, errorThrown) {
                    
                        var obj = request.responseJSON.errors ;

                        if(obj.hasOwnProperty("module_name") )
                        {
                           $( "#Name_Err" ).addClass( "has-error" );
                           $("#NameCheck").html("<div class='mad'>"+request.responseJSON.errors.module_name[0]+"</div>");   
                        }

                        if(obj.hasOwnProperty("module_slug") )
                        {
                            $( "#Slug_Err" ).addClass( "has-error" );
                            $("#SlugCheck").html("<div class='mad'>"+request.responseJSON.errors.module_slug[0]+"</div>");   
                        } 
 
                }
                });
        }
        
        return false;
        
        
        
        
    });
/* ************************************************************************* */  
/* ****************************** function end ***************************** */  
/* ************************************************************************* */    
   
   
});
