<?php

namespace Modules\Pets\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PetsGallery extends Model
{
    use SoftDeletes;
    protected $table = "pets_gallery";
    protected $fillable = ['pet_id','file']; 
    protected $dates = ['deleted_at'];
}
