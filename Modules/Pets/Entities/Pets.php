<?php

namespace Modules\Pets\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pets extends Model
{ 
    use SoftDeletes;
    protected $table = "pets";
    protected $fillable = ['owner_id','name']; 
    protected $dates = ['deleted_at'];
    
}
