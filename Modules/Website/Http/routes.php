<?php

Route::group(['middleware' => 'web', 'prefix' => '', 'namespace' => 'Modules\Website\Http\Controllers'], function()
{
    Route::get('/', 'WebsiteController@index');
});
