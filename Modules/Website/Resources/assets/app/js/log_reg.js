 $(function() {
     
     /*
     * Registration Owners
     * params : Name,email,password  
     */
      
    $("#registration").submit(function(e)
    {
        e.preventDefault();
        var name            =   $("[name='name']").val().trim();
        var email           =   $("[name='email']").val().trim();
        var password        =   $("[name='password']").val().trim();
        var type             =   $('#registration').attr('data-type');
        var a=0;
        var b=0;
        var c=0;
        var d=0;
        
         //type
        if(typeof type === 'undefined') { var a=0;$("#emsg").html("Sorry you are doing somthing wrong !!"); }
        else{ var a=1; $("#emsg").html(" "); }
        
        //Name
        if(name.length > 0){ b=1; $(".nameBox .has-error").html(" ");  } 
        else{  b=0; $(".nameBox .has-error").html(" This field is required! "); }
        
        //email
        if(email.length > 0)
        {    
            if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) ){ c=1; $(".emailBox .has-error").html(" ");  }
            else{  c=0; $(".emailBox .has-error").html("Please enter a valid email address! "); }
        }
        else{c=0; $(".emailBox .has-error").html(" This field is required! ");}
        
        //password
        if(password.length > 0){
            if( /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{4,}$/.test(password) )  {  d=1;  $(".passwordBox .has-error").html(' ');   }
            else  {  d=0; $(".passwordBox .has-error").html('Minimum 4 characters, at least one letter and one number.');} 
        }
        else{d=0; $(".passwordBox .has-error").html(" This field is required! ");}
        
        if(a==1 && b==1 && c==1 && d==1)
        {
            if(type=="owner" || type=="doctor")
            {
                var url=url=base_url+"/owners/register";;
                if(type=="owner"){url=base_url+"/owners/register";}
                if(type=="doctor"){url=base_url+"/doctors/register"}
                var formdata= new FormData($('#registration')[0]);
                formdata.append("type", type);

                $("#registerBtn").html("loading...");
                $('#registerBtn').prop("disabled", "disabled");

                $.ajax({
                        type: "POST",
                        url:url,
                        dataType: "json",
                        async: false, 
                        data: formdata,
                        processData: false,
                        contentType: false, 
                        success: function(response)
                        {     
                            $("#registerBtn").html("Sign Up");
                            $('#registerBtn').prop("disabled", false);
                            
                            if(response.status=='0')
                            {
                                $("#registration_model").html(response.message);
                            }
                            else if (response.status=='1')
                            {
                                $('#registration')[0].reset();
                                $('.signup-popup').modal('toggle');
                                $("#signup").html(response.message);
                                $('#reg_success').modal('show');

                            }
                            else if (response.status=='2')
                            {
                                window.location.href=response.url;
                            }
                        },
                        error: function (request, textStatus, errorThrown) 
                        {
                            $("#registerBtn").html("Sign Up");
                            $('#registerBtn').prop("disabled", false);

                            var obj = request.responseJSON.errors ;

                            if(obj.hasOwnProperty("email") )
                            { 
                               $(".emailBox .has-error").html(request.responseJSON.errors.email[0]);   
                            } 
                        }
                    });
            }else{$("#registration_model").html("Sorry something went wrong! ");}
         }
        return false;
    });
 /* ************************************************************************* */  
/* ************************************************************************* */  
/* ************************************************************************* */  
    
    $(".reg").click(function(e){
      var type=$(this).attr('data-type'); 
      $('#registration').attr('data-type',type);
      $("#emsg").html(" ");
    });
    
 
/* ************************************************************************* */  
/* ************************************************************************* */  
/* ************************************************************************* */ 
    $(".log").click(function(e){
        var type=$(this).attr('data-type');
        $('#login').attr('data-type',type);
        $("#loguser").html("Hello "+type+" !");
        
    });
/* ************************************************************************* */  
/* ************************************************************************* */  
/* ************************************************************************* */  
  /*
     * login
     * params : Name,email,password  
     */
  
  $("#login").submit(function(e){
      e.preventDefault();
        var email            =   $("[name='logEmail']").val().trim();
        var password           =   $("[name='logPassword']").val().trim();
        var type             =   $('#login').attr('data-type');
        var a=0;
        var b=0;
        var c=0;
        //type
        if(typeof type === 'undefined') { var a=0;$("#logmsg").html("Sorry you are doing somthing wrong !!"); }
        else{ var a=1; $("#logmsg").html(" "); }
        
        //email
        if(email.length > 0)
        {    
            if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) ){ b=1; $(".logEmailBox .has-error").html(" ");  }
            else{  b=0; $(".logEmailBox .has-error").html("Please enter a valid email address! "); }
        }
        else{b=0; $(".logEmailBox .has-error").html(" This field is required! ");}
        
        //password
        if(password.length > 0){
              c=1;  $(".logPasswordBox .has-error").html(' ');   
        } 
        else{c=0; $(".logPasswordBox .has-error").html(" This field is required! ");}
        
        if(a==1 && b==1 && c==1 )
        {
            if(type=="owner" || type=="doctor")
            {
                $("#logmsg").html(" ");
                if(type=="owner"){url=base_url+"/owners/login";}
                if(type=="doctor"){url=base_url+"/doctors/login"}
                var formdata= new FormData($('#login')[0]);
                formdata.append("type", type);

                $.ajax({
                        type: "POST",
                        url:url,
                        dataType: "json",
                        async: false, 
                        data: formdata,
                        processData: false,
                        contentType: false, 
                        success: function(response)
                        {     

                            if(response.status=='0')
                            {
                                $("#logmsg").html(response.message);
                            }
                            else if (response.status=='1' || response.status=='2')
                            {
                               window.location.href=response.url;
                            }
                             
                        },
                        error: function (request, textStatus, errorThrown) 
                        {
    //                        $("#registerBtn").html("Sign Up");
    //                        $('#registerBtn').prop("disabled", false);
    //                        
                            var obj = request.responseJSON.errors ;
    
                            if(obj.hasOwnProperty("logEmail") )
                            { 
                                $(".logEmailBox .has-error").html(request.responseJSON.errors.logEmail[0]);   
                            }
                            
                            if(obj.hasOwnProperty("logPassword") )
                            { 
                                $(".logPasswordBox .has-error").html(request.responseJSON.errors.logPassword[0]);   
                            }


                        }
                    });
                
              
            }else{$("#logmsg").html("Sorry something went wrong! ");}
              
                
        }
        
   });
  
  
  
  
  
  
  
  /* ************************************************************************* */  
/* ************************************************************************* */  
/* ************************************************************************* */  
 
 });