 $(function() {
    /* ************************************************************************* */  
    /* ************************************************************************* */  
    /* ************************************************************************* */  
 
    $("#proceed").submit(function(e)
    {  
        e.preventDefault();
        var mobile            =   $("[name='mobile']").val().trim();
        var council_reg_no    =   $("[name='council_reg_no']").val().trim();
        var experience        =   $("[name='experience']").val().trim();
        var services          =   $("[name='services']").val().trim();
        var state             =   $("[name='state']").val().trim();
        var address           =   $("[name='address']").val().trim();
        
        var a=b=c=d=e=f=0;
       
        //mobile 
        if(mobile.length > 0)
        {  
             if( /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(mobile) ){
                 $(".mobileBox .has-error").html(" ");a=1;
             }else{ $(".mobileBox .has-error").html(" Please enter a valid mobile number (+91 -98********) ");}
        } 
        else {  $(".mobileBox .has-error").html(" This field is required! ");  }        
        
        //council_reg_no 
        if(council_reg_no.length > 0)  {   $(".council_reg_noBox .has-error").html(" ");b=1;    } 
        else {  $(".council_reg_noBox .has-error").html(" This field is required! "); }
        
        
        //council_reg_no 
        if(experience.length > 0)  
        {  
            if( /^[0-9]*$/.test(experience) )
            {
                if(experience <= 60)  {  c=1;  $(".experienceBox .has-error").html(" ");  }
                else {  $(".experienceBox .has-error").html("please enter a valid experience");  }
            } else {  $(".experienceBox .has-error").html("please enter a valid experience");  }  
        } 
        else {  $(".experienceBox .has-error").html(" This field is required! ");  }  
        
        //services 
        if(services.length > 0){   $(".servicesBox .has-error").html(" ");d=1; } 
        else {  $(".servicesBox .has-error").html(" This field is required! "); }
        
        //state 
        if(state.length > 0){   $(".stateBox .has-error").html(" ");e=1; } 
        else {  $(".stateBox .has-error").html(" This field is required! "); }
        
        //address 
        if(address.length > 0){   $(".addressBox .has-error").html(" ");f=1; } 
        else {  $(".addressBox .has-error").html(" This field is required! "); } 
        if(a==1 && b==1 && c==1 && d==1 && e==1 && f==1)
        {
               $.ajax({
                        type: "POST",
                        url:$( '#proceed' ).attr( 'action' ),
                        dataType: "json",
                        async: false, 
                        data: new FormData($('#proceed')[0]),
                        processData: false,
                        contentType: false, 
                        success: function(response)
                        {     
                            
                            if(response.status=='0')
                            {
                                $("#msg").html(response.message);
                            }
                            else if (response.status=='1')
                            {
                                 
                                $("#sectionCtr").html(response.message);
                              

                            }
                            else if (response.status=='2')
                            {
                                window.location.href=response.url;
                            }
                        },
                        error: function (request, textStatus, errorThrown) 
                        {
 
                            var obj = request.responseJSON.errors ;

                            if(obj.hasOwnProperty("mobile") )
                            { 
                               $(".mobileBox .has-error").html(request.responseJSON.errors.mobile[0]);   
                            } 
                            
                            if(obj.hasOwnProperty("council_reg_no") )
                            { 
                               $(".council_reg_noBox .has-error").html(request.responseJSON.errors.council_reg_no[0]);   
                            }
                            
                            if(obj.hasOwnProperty("experience") )
                            { 
                               $(".experienceBox .has-error").html(request.responseJSON.errors.experience[0]);   
                            } 
                            
                             if(obj.hasOwnProperty("state") )
                            { 
                               $(".stateBox .has-error").html(request.responseJSON.errors.state[0]);   
                            }
                            
                            if(obj.hasOwnProperty("address") )
                            { 
                               $(".addressBox .has-error").html(request.responseJSON.errors.address[0]);   
                            }
                            
                            if(obj.hasOwnProperty("services") )
                            { 
                               $(".servicesBox .has-error").html(request.responseJSON.errors.services[0]);   
                            }
                        }
                    });
        }
        
    });
    
    /* ************************************************************************* */  
    /* ************************************************************************* */  
    /* ************************************************************************* */  

 });