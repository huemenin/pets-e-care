<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> @yield('title')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    <link rel="stylesheet" href="{{asset('public/web/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('public/web/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('public/web/fonts/gotham/stylesheet.css')}}">
    <link rel="stylesheet" href="{{asset('public/web/css/fontawesome-all.css')}}">
    <link rel="stylesheet" href="{{asset('public/web/css/slick.css')}}"> 
    @yield('css')
    <script> var base_url = "{{URL::to('/')}}";  </script>
</head>

<body> 
    <div class="container">
        <div class="h-add hedarea">
           <div class="contant">
               <span>Add area</span>
           </div>
       </div>
    </div>
    <header>
        <div class="container">
            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand logo" href="#"> <img src="{{asset('public/web/images/logo.png')}}" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
<!--                    <div class="menu-right">
                        <ul>
                            <li class="one"> <a href="javascript:void(0)">Search</a> </li>
                            
                            <li class="dropdown"> <a href="javascript:void(0)" class="signin-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-lock"></i> Sign In</a> 
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                      <a class="dropdown-item log" href="#" data-toggle="modal" data-target=".signin-popup" data-type='owner'>Individual</a>
                                      <a class="dropdown-item log" href="#"  data-toggle="modal" data-target=".signin-popup"  data-type='doctor'>Doctor</a>
                                    </div>
                            </li>
                            
                            <li class="dropdown"> <a href="javascript:void(0)" class="callus-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i> Register</a> 
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                      <a class="dropdown-item reg" href="#" data-toggle="modal" data-target=".signup-popup" data-type='owner'>Individual</a>
                                      <a class="dropdown-item reg" href="#" data-toggle="modal" data-target=".signup-popup" data-type='doctor'>Doctor</a>
                                    </div>
                            </li>
                            
                            <li class="bc-menu">
                                <div class="bc-main"><span class="one"></span> <span class="two"></span> <span class="three"></span></div>
                            </li>
                        </ul>
                    </div>-->
                
<div class="menu-right">
                        <ul>
                            <li class="one"> <a href="javascript:void(0)">Search</a> </li>
                            @if(Session::has('user_type'))
                                    @php 
                                       $user_type=Session::get('user_type');
                                       @endphp
                             
                            
                            <li class="dropdown menu-dp two">
                                <a href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <span class="menu-dp-icon">
                                 
                                    @if(Auth::guard($user_type)->user()->image)
                                    @if(file_exists( public_path()."/".$user_type."s/img/".Auth::guard($user_type)->user()->image ))
                                        <img src="{{asset('public/'.$user_type.'s/img/'.Auth::guard($user_type)->user()->image)}}" alt="">
                                    @else
                                        <img src="{{asset('public/'.$user_type.'s/default-profile.png')}}" alt="">
                                    @endif
                                    @else
                                        <img src="{{asset('public/'.$user_type.'s/default-profile.png')}}" alt="">
                                    @endif
                               
                                        
                                        
                                        
                                         
                                    </span> 
                                    {{Auth::guard($user_type)->user()->name}} 
                                    <i class="fas fa-sort-down"></i>
                                </a> 
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                      <a class="dropdown-item" href="#">Test 1</a>
                                      <a class="dropdown-item" href="#">Test 2</a>
                                    </div>
                            </li>
                            <li class="five">
                                <a href="{{URL('/').'/'.$user_type.'s/logout'}}"><i class="fas fa-sign-out-alt"></i> Logout</a> 
                            </li>
                             
                            @else
                            
                            <li class="dropdown three"> <a href="javascript:void(0)" class="signin-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-lock"></i> Sign In</a> 
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                      <a class="dropdown-item log" href="#" data-toggle="modal" data-target=".signin-popup" data-type='owner'>Individual</a>
                                      <a class="dropdown-item log" href="#"  data-toggle="modal" data-target=".signin-popup"  data-type='doctor'>Doctor</a>
                                    </div>
                            </li>
                            
                            <li class="dropdown three"> <a href="javascript:void(0)" class="callus-btn" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-user"></i> Register</a> 
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                      <a class="dropdown-item reg" href="#" data-toggle="modal" data-target=".signup-popup" data-type='owner'>Individual</a>
                                      <a class="dropdown-item reg" href="#" data-toggle="modal" data-target=".signup-popup" data-type='doctor'>Doctor</a>
                                    </div>
                            </li>
                            
                             @endif
                            <li class="bc-menu four">
                                <div class="bc-main"><span class="one"></span> <span class="two"></span> <span class="three"></span></div>
                            </li>
                            
                        </ul>
                    </div>


                </div>
            </nav>
        </div>
        
          <div class="menu-dropdown">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 col-sm-12 left-box">
                        <ul class="d-flex">
                            <li>
                                <p><a href="">Our <br>Services</a></p>
                                <a href="#">Pets Care</a>
                                <a href="#">Pets Identification</a>
                            </li>
                            <li>
                                <p><a href="">Who <br>We Are</a></p>
                                <a href="#">About Us</a>
                                <a href="#">Vision & Mission</a>
                            </li>
                            <li>
                                <p><a href="">Search <br>A Doctor</a></p>
                                <a href="#">Pets Care</a>
                                <a href="#">Pets Identification</a>
                            </li>
                            <li>
                                <p><a href="">Ask <br>An Expert</a></p>
                                <a href="#">Pets Care</a>
                                <a href="#">Pets Identification</a>
                            </li>
                            <li>
                                <p><a href="">Contact <br>Us</a></p>
                                <a href="#">Pets Care</a>
                                <a href="#">Pets Identification</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-sm-12 right-box">
                        <div class="contact-box">
                            <a href="mailto:info@petsecare.com">info@petsecare.com</a>
                            <div class="social">
                                <a href="#"><i class="fab fa-facebook-f"></i></a>
                                <a href="#"><i class="fab fa-twitter"></i></a>
                                <a href="#"><i class="fab fa-google"></i></a>
                                <a href="#"><i class="fab fa-linkedin-in"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    
    
    @yield('content')
    
    
     <footer>
        <div class="container">
           <div class="footerlogo row">
               <div class="col-lg-3 col-sm-12">
                   <img src="{{asset('public/web/images/footer.png')}}" alt="">
               </div>
           </div>
            <div class="footer-box row">
                <div class="footer-grid col-lg-3 col-sm-12">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quis odio quis mi 
                molestie iaculis at sit amet nisi.</p>
                </div>
                <div class="footer-grid col-lg-2 col-sm-12">
                    <a href="#">Our Services</a>
                    <a href="#">Who We Are</a>
                    <a href="#">Ask an Expert</a>
                </div>
                <div class="footer-grid col-lg-2 col-sm-12">
                    <a href="#">Search</a>
                    <a href="#">Forum</a>
                    <a href="#">Reach Us</a>
                </div>
                <div class="footer-grid col-lg-3 col-sm-12">
                    <ul>
                        <li><span><i class="fas fa-envelope"></i>    </span><a href="mailto:info@petsecare.com">info@petsecare.com</a></li>
                        <li><span><i class="fas fa-life-ring"></i></span><a href="">+91 5678 912 345</a></li>
                        <li class="social">
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                            <a href="#"><i class="fab fa-google"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="footer-grid col-lg-2 col-sm-12">
                    <a href="#">Made by <span>huemen inside.</span></a>
                </div>
            </div>
        </div>
    </footer>
    
    
    <div class="modal fade signin-popup" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
             <form method="post" autocomplete="off" name="login" id="login" action="{{URL('/owners/login')}}"  enctype="multipart/form-data"  >
             
            <h2 id="loguser">  </h2>
                <div id="logmsg"></div>
                <!--Email-->
                <div class="group input-wrap logEmailBox">      
                   <input type="text" name="logEmail">
                 <label>Email</label>
                  <span class="has-error"></span>
                </div>
                <!--/Email-->
                <!--password-->
                <div class="group input-wrap logPasswordBox">      
                  <input type="password" name="logPassword">
                  <label>Password</label> 
                  <span class="icon"><i class="far fa-eye-slash"></i></span>
                   <span class="has-error"></span>
                </div>
                <!--/password-->
            <a href="" class="password-lost">Lost Password?</a>
            <button class="btn signin-btn">Sign In</button>
            <p>Not a member? <a href="#">Register now!</a></p>
            
             </form>
        </div>
      </div>
    </div>
    
    <!--Sign Up-->
    <div id="signup"></div> 
    <div class="modal fade signup-popup" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
          <div class="modal-content" id="registration_model">
            <form method="post" autocomplete="off" name="registration" id="registration" action="{{URL('/owners/register')}}"  enctype="multipart/form-data"  >
                <h2>Sign Up!
                    <div id="emsg"></div>
                </h2>
                
                <!--Name-->
                <div class="group input-wrap nameBox">      
                  <input type="text" name='name'>
                  <label>Name</label>
                  <span class="has-error"></span>
                </div>
               
                <!--/Name-->
                
                <!--Email-->
                <div class="group input-wrap emailBox">      
                  <input type="text" name='email'>
                  <label>Email ID</label>
                  <span class="has-error"></span>
                </div>
                <!--/Email-->
                
                <!--Password-->
                <div class="group input-wrap passwordBox">      
                    <input type="password" name="password">
                  <label>Password</label>
                  <span class="icon" id="showPassword"><i class="far fa-eye-slash"></i></span>
                  <span class="has-error"></span>
                </div>
                <!--/Password-->
                
                <button class="btn signin-btn register" id="registerBtn" >Sign Up</button>
                <p>Already registered? <a href="#">Login!</a></p>
            </form>
        </div>
      </div>
    </div>
    <!--/*Sign Up-->
    
   <script src="{{asset('public/admin/plugins/jquery/dist/jquery.min.js')}}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <!-- Common  -->
    <script src="{{asset('public/web/js/bootstrap.min.js')}}"></script>
     
    <script src="{{asset('public/web/js/Common.js')}}"></script>
    <script src="{{asset('Modules/Website/Resources/assets/app/js/log_reg.js')}}"></script>
    <script src="{{asset('public/web/js/slick.min.js')}}"></script>
    <script src="{{asset('public/web/js/pets.js')}}"></script>
    @yield('js')
    @stack('scripts')
</body>

</html>