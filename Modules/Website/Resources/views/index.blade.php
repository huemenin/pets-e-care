@extends('website::layouts.master')
@section('title', "pet e care")
@section('content')
<section class="banner">
        <div class="banner-contant">
            <h2><span>Your Best Friends</span>
                <span>Need Best Care</span></h2>
            <div class="seach-box">
                <div class="search-location"><input type="text" placeholder="Kottayam"><span class="icon"><i class="far fa-dot-circle"></i></span></div>
                <div class="search-input"><input type="text" placeholder="Expertise"></div>
                <div class="search-btn"><button><i class="fas fa-search"></i></button></div>
            </div>
        </div>
        <div class="banner-image">
            <div class="banner-slide">
                <div><img src="{{asset('public/web/images/banner.jpg')}}" alt=""></div>
                <div><img src="{{asset('public/web/images/banner.jpg')}}" alt=""></div>
            </div>
        </div>
        <div class="banner-gradient"></div>
    </section>
    <section class="specialists">
        <div class="container">
            <h3>Specialists Ready To Help</h3>
            <div class="specialists-grid row">
                <div class="col-lg-3 col-sm-12 spec-grid-repeat">
                    <div class="spec-grid-main">
                        <div class="img-box">
                            <img src="{{asset('public/web/images/dr-3.jpg')}}" alt="">
                            <div class="bottom-curve"></div>
                        </div>
                        <div class="contant-box">
                            <div class="contant">
                                <h5>Michael J Collins</h5>
                                <p>Veterinarian</p>
                                <span><i class="fas fa-map-marker"></i> Kottayam</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 spec-grid-repeat">
                    <div class="spec-grid-main">
                        <div class="img-box">
                            <img src="{{asset('public/web/images/dr-2.jpg')}}" alt="">
                            <div class="bottom-curve"></div>
                        </div>
                        <div class="contant-box">
                            <div class="contant">
                                <h5>Michael J Collins</h5>
                                <p>Veterinarian</p>
                                <span><i class="fas fa-map-marker"></i> Kottayam</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 spec-grid-repeat">
                    <div class="spec-grid-main">
                        <div class="img-box">
                            <img src="{{asset('public/web/images/dr-1.jpg')}}" alt="">
                            <div class="bottom-curve"></div>
                        </div>
                        <div class="contant-box">
                            <div class="contant">
                                <h5>Michael J Collins</h5>
                                <p>Veterinarian</p>
                                <span><i class="fas fa-map-marker"></i> Kottayam</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 spec-grid-repeat">
                    <div class="spec-grid-main">
                        <div class="add-box"><span>Add area</span></div>
                        
                    </div>
                </div>
            </div>
            <h4>Do you have concerns? <a href="#">Ask our experts.</a></h4>
        </div>
    </section>
    <section class="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-sm-12 left-box">
                <h2>Quality <br>
                Pet Care Services</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce massa nunc, egestas vitae efficitur id, blandit id tortor. Sed efficitur nisi ut elit mattis, posuere consectetur ligula porttitor.</p>
                <a href="" class="btn-main gradient-btn">All Services</a>
            </div>
            <div class="col-lg-8 col-sm-12 right-box">
                   <div class="slide-box">
                       <div class="slide-box-slider">
                           <div class="item">
                               <div class="logo">
                                   <span><img src="{{asset('public/web/images/icon-1.png')}}" alt=""></span>
                               </div>
                               <div class="contant">
                                   <h5>1 Pet Care Services</h5>
                                   <p>1 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce massa nunc, egestas vitae efficitur id, blandit id tortor. Sed efficitur nisi ut elit mattis, posuere consectetur ligula porttitor.</p>
                               </div>
                           </div>
                           <div class="item">
                               <div class="logo">
                                   <span><img src="{{asset('public/web/images/icon-1.png')}}" alt=""></span>
                               </div>
                               <div class="contant">
                                   <h5>2 Pet Care Services</h5>
                                   <p>2 Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce massa nunc, egestas vitae efficitur id, blandit id tortor. Sed efficitur nisi ut elit mattis, posuere consectetur ligula porttitor.</p>
                               </div>
                           </div>
                       </div>
                   </div> 
            </div>
            </div>
            <div class="h-add area2">
               <div class="contant">
                   <span>Add area</span>
               </div>
           </div>
        </div>
    </section>
@stop
@section('js') 
<script src="{{asset('public/web/js/slick.min.js')}}"></script>
<script src="{{asset('public/web/js/pets.js')}}"></script>
@stop