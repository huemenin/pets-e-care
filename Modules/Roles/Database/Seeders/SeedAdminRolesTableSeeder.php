<?php

namespace Modules\Roles\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class SeedAdminRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");
         if(DB::table('admin_roles')->get()->count() == 0){
            $tasks  =   [
                            [
                                'name' => 'Developer',
                                'slug' => 'developer', 
                                'type' => '0'
                            ]
                        ];
             
            DB::table('admin_roles')->insert($tasks);
         }
    }
}
