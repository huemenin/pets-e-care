<?php

namespace Modules\Roles\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RolesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
       if( $this->call(SeedAdminRolesTableSeeder::class))
        $this->command->info('Table admin_roles seeded!');
          /* 2 */
        if( $this->call(SeedAdminUsersRolesTableSeeder::class))
        $this->command->info('Table Admin Users Roles seeded!');
          /* 3 */
        if( $this->call(SeedAdminRolePermissionsTableSeeder::class))
        $this->command->info('Table Admin Role Permissions seeded!');
        
    }
}
