<?php

namespace Modules\Roles\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class SeedAdminRolePermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        if(DB::table('admin_role_permissions')->get()->count() == 0){
            $tasks  =   [
                            [
                                'role_id' => '1',
                                'permission_id' => '1' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '2' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '3' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '4' 
                            ],
            
    /* modules End  */
                            [
                                'role_id' => '1',
                                'permission_id' => '5' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '6' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '7' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '8' 
                            ],
                
    /* permissions End  */
                            [
                                'role_id' => '1',
                                'permission_id' => '9' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '10' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '11' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '12' 
                            ],
    /* Roles End  */
                            [
                                'role_id' => '1',
                                'permission_id' => '13' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '14' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '15' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '16' 
                            ],
                
    /* Admin End  */    
                            [
                                'role_id' => '1',
                                'permission_id' => '17' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '18' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '19' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '20' 
                            ],
                
    /* Owners End  */ 
                            [
                                'role_id' => '1',
                                'permission_id' => '21' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '22' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '23' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '24' 
                            ],
                
    /* Doctors End  */ 
                [
                                'role_id' => '1',
                                'permission_id' => '25' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '26' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '27' 
                            ],
                            [
                                'role_id' => '1',
                                'permission_id' => '28' 
                            ],
                
    /* pets End  */ 
                
                
                
                        ];
             
            DB::table('admin_role_permissions')->insert($tasks);
         }
        // $this->call("OthersTableSeeder");
    }
}
