<?php

namespace Modules\Roles\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SeedAdminUsersRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        if(DB::table('admin_users_roles')->get()->count() == 0){
                   $tasks  =   [
                                   [
                                       'user_id' => '1',
                                       'role_id' => '1'
                                   ]
                               ];

                   DB::table('admin_users_roles')->insert($tasks);
                }
        // $this->call("OthersTableSeeder");
    }
}
