<?php

namespace Modules\Roles\Entities;

use Illuminate\Database\Eloquent\Model; 
use Illuminate\Database\Eloquent\SoftDeletes;

class Roles extends Model
{
    use SoftDeletes;
    protected $table = "admin_roles";
    //protected $primaryKey = "id";
    protected $fillable = ['name','slug']; 
    protected $dates = ['deleted_at'];

    public function permissions()
    {
        return $this->belongsToMany("Modules\Permissions\Entities\Permissions","admin_role_permissions","role_id","permission_id") ;
    }
}
