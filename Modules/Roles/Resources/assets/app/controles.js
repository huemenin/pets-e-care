     
$(function() {
   
   if($('#roles-table').length){
        
   $('#roles-table').DataTable({
        processing: true,
        serverSide: true,  
        ajax: base_url+"/admin/roles/AdminRolesList",
      columns: [ 
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            {
                     data: "null",
                    'searchable': false, 
                    'orderable': false,
                   
                    render: function (data, type, full) 
                    { 
                        
                        var u;
                             if(full.id==1)
                             {
                                  
                                u='<a href="'+base_url+'/admin/view/'+full.id+'" class="iconclass"><i class="fa  fa-eye"></i></a>'; 
                             }else
                             {
                                 
                            
                          u = '<a href="'+base_url+'/admin/roles/view/'+full.id+'" class="iconclass"><i class="fa  fa-eye"></i></a>' 
                       +''+'<a href="'+base_url+'/admin/roles/edit/'+full.id+'" class="iconclass"><i class="fa  fa-pencil"></i></a>'
                          +''+'<a href="'+base_url+'/admin/roles/delete/'+full.id+'" class="iconclass"  Onclick="return ConfirmDelete();"><i class="fa  fa-trash"></i></a>';
 }
                     
                        return u;
                    }
            }
        ]
       
   });
   
   }
 /* ************************************************************************* */  
/* *************************** data table listing end ********************** */  
/* ************************************************************************* */  
    /*
     * create form 
     * params : Module,Name,Status,Slug  
     */

    $("#roles_create").submit(function(e)
    {
        e.preventDefault();
        
        var Status       =   $("[name='Status']").val().trim();
        var Name         =   $("[name='role_name']").val().trim();
        var Slug         =   $("[name='role_slug']").val().trim();
        var description  =   $("[name='description']").val().trim();
        var Permissions =    $("[name='permissions[]']:checked").length;
        
 
        var a=0;
        var b=0;
        var c=0;
        var d=0;
        
         
        //Name
        if(Name.length > 0){a=1; $( "#Name_Err" ).removeClass( "has-error" );}
        else{a=0; $( "#Name_Err" ).addClass( "has-error" );}
        
        //Status
        if(Status=='0' || Status=='1'){b=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{b=0; $( "#Status_Err" ).addClass( "has-error" );}
        
        //Slug
        if(Slug.length > 0)
        {
            var gnsg= generate_slug($("#role_name").val());
            if(gnsg != Slug) { c=0; $( "#Slug_Err" ).addClass( "has-error" );  }
            else{c=1; $( "#Slug_Err" ).removeClass( "has-error" );}   
        }
        else{b=0; $( "#Slug_Err" ).addClass( "has-error" );}
 
        //Permissions
        if(Permissions > 0){d=1; $( "#errorAssign" ).html( " " );}
        else{  d=0; $( "#errorAssign" ).html( "Please select at least one Permission." );}
        
        /* ------------------------------------------------------------------ */
        /* ----------------- form submitting -------------------------------- */
        /* ------------------------------------------------------------------ */
        
        if(a==1 && b==1 && c==1 && d==1)
        {
            
            $("#NameCheck").html(" ");  
            $("#SlugCheck").html(" "); 
            $(".showinfo").html(" ");
            $( "#Name_Err" ).removeClass( "has-error" );
            $( "#Slug_Err" ).removeClass( "has-error" );
            
               $.ajax({
                type: "POST",
                url:base_url+"/admin/roles/create",
                dataType: "json",
                async: false, 
                data: new FormData($('#roles_create')[0]),
                processData: false,
                contentType: false, 
                success: function(response)
                {     
                     if(response.status==true){window.location.href = response.url; }
                     else{$(".showinfo").html(response.alert);}
                },
                error: function (request, textStatus, errorThrown) {
                    var obj = request.responseJSON.errors ;
                    
                    if(obj.hasOwnProperty("role_name") )
                    {
                       $( "#Name_Err" ).addClass( "has-error" );
                       $("#NameCheck").html("<div class='mad'>"+request.responseJSON.errors.role_name[0]+"</div>");   
                    }
                   
                    if(obj.hasOwnProperty("role_slug") )
                    {
                        $( "#Slug_Err" ).addClass( "has-error" );
                        $("#SlugCheck").html("<div class='mad'>"+request.responseJSON.errors.role_slug[0]+"</div>");   
                    }
              
                }
                });
        }
        
        return false;
        
    });
  
  
  
  
/* ************************************************************************* */  
/* *************************** Roles create end *********************** */  
/* ************************************************************************* */  
   
     $("#role_name").keyup(function() {  
        $("#role_slug").val(generate_slug($("#role_name").val()));
    });
    

/* ************************************************************************* */  
/* *************************** generate slug end *************************** */  
/* ************************************************************************* */  
 $("[name='permissions[]']").change(function(){
    
    var slug = $(this).attr('data-slug').split("-"); 
     
   if(this.checked)
   {    
        if(slug[0] != 'view'){
        $("[data-slug='view-"+slug[1]+"']").prop('checked',true);}
   }
   else
   {
            var create=$("[data-slug='create-"+slug[1]+"']:checked").length;
            var edit=$("[data-slug='edit-"+slug[1]+"']:checked").length;
            var deletes=$("[data-slug='delete-"+slug[1]+"']:checked").length;
           
       if(slug[0] == 'view'){
           
           
           if( (create != 0) || (edit != 0) || (deletes != 0) )
           {
               $("[data-slug='view-"+slug[1]+"']").prop('checked',true);
               $( "#errorAssign" ).html( "To unselect view-"+slug[1]+",<br/> please unselect all the remaining selected boxes under "+slug[1]+" module" ); 
 
           }else
           { 
                $( "#errorAssign" ).html( " " );
           }
                
       }else
       {
           if( (create == 0) && (edit == 0) && (deletes == 0) )
           {
              $( "#errorAssign" ).html( " " );
           } 
       }
   }
});

/* ************************************************************************* */  
/* *************************** checkbox check end *************************** */  
/* ************************************************************************* */  
    /*
     * edit form 
     * params : Module,Name,Status,Slug  
     */

    $("#roles_update").submit(function(e)
    {
        e.preventDefault();
        var action=   $("#roles_update").attr('action');
         
        var Status       =   $("[name='Status']").val().trim();
        var Name         =   $("[name='role_name']").val().trim();
        var Slug         =   $("[name='role_slug']").val().trim();
        var description  =   $("[name='description']").val().trim();
        var Permissions =    $("[name='permissions[]']:checked").length;
        
 
        var a=0;
        var b=0;
        var c=0;
        var d=0;
        
         
        //Name
        if(Name.length > 0){a=1; $( "#Name_Err" ).removeClass( "has-error" );}
        else{a=0; $( "#Name_Err" ).addClass( "has-error" );}
        
        //Status
        if(Status=='0' || Status=='1'){b=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{b=0; $( "#Status_Err" ).addClass( "has-error" );}
        
        //Slug
        if(Slug.length > 0)
        {
            var gnsg= generate_slug($("#role_name").val());
            if(gnsg != Slug) { c=0; $( "#Slug_Err" ).addClass( "has-error" );  }
            else{c=1; $( "#Slug_Err" ).removeClass( "has-error" );}   
        }
        else{b=0; $( "#Slug_Err" ).addClass( "has-error" );}
 
        //Permissions
        if(Permissions > 0){d=1; $( "#errorAssign" ).html( " " );}
        else{  d=0; $( "#errorAssign" ).html( "Please select at least one Permission." );}
        
        /* ------------------------------------------------------------------ */
        /* ----------------- form submitting -------------------------------- */
        /* ------------------------------------------------------------------ */
        
        if(a==1 && b==1 && c==1 && d==1)
        {
            
            $("#NameCheck").html(" ");  
            $("#SlugCheck").html(" "); 
            $(".showinfo").html(" ");
            $( "#Name_Err" ).removeClass( "has-error" );
            $( "#Slug_Err" ).removeClass( "has-error" );
            
               $.ajax({
                type: "POST",
                url:action,
                dataType: "json",
                async: false, 
                data: new FormData($('#roles_update')[0]),
                processData: false,
                contentType: false, 
                success: function(response)
                {     
                     if(response.status==true){window.location.href = response.url; }
                     else{$(".showinfo").html(response.alert);}
                },
                error: function (request, textStatus, errorThrown) {
                    var obj = request.responseJSON.errors ;
                    
                    if(obj.hasOwnProperty("role_name") )
                    {
                       $( "#Name_Err" ).addClass( "has-error" );
                       $("#NameCheck").html("<div class='mad'>"+request.responseJSON.errors.role_name[0]+"</div>");   
                    }
                   
                    if(obj.hasOwnProperty("role_slug") )
                    {
                        $( "#Slug_Err" ).addClass( "has-error" );
                        $("#SlugCheck").html("<div class='mad'>"+request.responseJSON.errors.role_slug[0]+"</div>");   
                    }
              
                }
                });
        }
        
        return false;
        
    });
  
  
  
  
/* ************************************************************************* */  
/* *************************** Roles update end *********************** */  
/* ************************************************************************* */  
  

});
