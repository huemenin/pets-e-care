@extends('admin::admin.master')
@section('title', "Admin View Roles")
 
@section('content')

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small style="font-weight: bold;">View Roles</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{URL('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="{{URL('/admin/roles/')}}">Roles</a></li>
          <li  class="active"><a href="javascript:void(0)">View Role</a></li>
          
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="showinfo"></div>
      <!-- Default box -->
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
                
                    <div class="row"> 
                <!-- full column -->
                        <div class="col-md-12">
                            <div class="col-md-6">
                            <!-- Name has-error-->
                             
                                <div class="form-group " >
                                    <label>Name</label>
                                    <input type="text" class="form-control" disabled="true"  value="{{$role->name}}">
                                </div>
                             
                            <!-- /.Name -->
                            
                            <!--Slug--> 
                                <div class="form-group"  >
                                    <label>Slug </label>
                                    <input disabled="true" type="text" class="form-control"  value="{{$role->slug}}"> 
                                 </div>
                            
                            <!-- /.Slug -->
                            
                            <!--Status -->
                            
                                <div class="form-group"    >
                                    <label>Status</label>
                                    <select class="form-control" style="width: 100%;" disabled="true">
                                        <option value="1" {{($role->status==1)?'selected':''}} >Active</option>
                                        <option value="0" {{($role->status==0)?'selected':''}}>Inactive</option>
                                    </select>
                                </div>
                             
                            <!-- /.Status -->
                            </div>
                            <!-- Description -->
                            <div class="form-group col-md-6">
                                <label>Description</label>
                                <textarea rows="8" cols="5" style="resize: none"  disabled="true"  class="form-control"  >{{$role->description}}</textarea>
                            </div>
                            <!-- /.Description -->
                            
                            
                            
                            
                        <div class="row">
                            <div class="form-group col-md-12"><br/>
                                    <label class="display-block text-semibold">Assign Permissions for Role</label>
                                    <div id="errorAssign" style="color: #dd4b39;font-weight: 600"></div>
                                    <br/>
                                    <?php $perms = $role->permissions->pluck('id')->toArray(); ?>
                                    @foreach($modules as $module)
                                            @if($module->permissions->count()>0)
                                            <div class="form-group col-md-12">
                                                <label class="display-block text-semibold">{{$module->module_name}}</label><br/>
                                                @foreach($module->permissions as $permission)
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" class="styled" value="{{$permission->id}}" disabled="true"   @if(in_array($permission->id,$perms)) checked @endif >
                                                        {{$permission->name}}
                                                    </label>
                                                @endforeach
                                            </div>
                                            @endif
                                    @endforeach
                            </div>
                        </div>
                            
                            
                        </div>
                <!--/.col (full) -->        
                          
                    </div> 
             
            </div>
            <!-- /.box-body -->
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@stop

 