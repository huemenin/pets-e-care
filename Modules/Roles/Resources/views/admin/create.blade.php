@extends('admin::admin.master')
@section('title', "Admin Create Roles")
 
@section('content')

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small style="font-weight: bold;">Create Roles</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{URL('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="{{URL('/admin/roles/')}}">Roles</a></li>
          <li  class="active"><a href="javascript:void(0)">Create Roles</a></li>
          
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="showinfo"></div>
      <!-- Default box -->
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
                    <form method="post" autocomplete="off" name="roles_create" id="roles_create" action="{{URL('/admin/roles/create')}}"  enctype="multipart/form-data">
                    <div class="row"> 
                <!-- full column -->
                        <div class="col-md-12">
                            <div class="col-md-6">
                            <!-- Name has-error-->
                             
                                <div class="form-group " id="Name_Err">
                                    <label>Name<span class="mad">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Role Name" name="role_name" id="role_name">
                                    <div id="NameCheck"></div>
                                </div>
                             
                            <!-- /.Name -->
                            
                            <!--Slug-->
                            
                                <div class="form-group" id="Slug_Err">
                                    <label>Slug<span class="mad">*</span></label>
                                    <input readonly="true" type="text" class="form-control" placeholder="Role Slug" name="role_slug" id="role_slug">
                                    <div id="SlugCheck"></div>
                                 </div>
                            
                            <!-- /.Slug -->
                            
                            <!--Status -->
                            
                                <div class="form-group" id="Status_Err"  >
                                    <label>Status</label>
                                    <select class="form-control" style="width: 100%;" name="Status" id="Status">
                                        <option value="1" selected="">Active</option>
                                        <option value="0">Not Activate</option>
                                    </select>
                                </div>
                             
                            <!-- /.Status -->
                            </div>
                            <!-- Description -->
                            <div class="form-group col-md-6">
                                <label>Description</label>
                                <textarea rows="8" cols="5" style="resize: none" class="form-control" placeholder="Role Description" name="description" id="description"></textarea>
                            </div>
                            <!-- /.Description -->
                            
                            
                            
                            
                        <div class="row">
                            <div class="form-group col-md-12"><br/>
                                    <label class="display-block text-semibold">Assign Permissions for Role</label>
                                    <div id="errorAssign" style="color: #dd4b39;font-weight: 600"></div>
                                    <br/>
                                    
                                    @foreach($modules as $module)
                                            @if($module->permissions->count()>0)
                                            <div class="form-group col-md-12">
                                                <label class="display-block text-semibold">{{$module->module_name}}</label><br/>
                                                @foreach($module->permissions as $permission)
                                                    <label class="checkbox-inline">
                                                        <input type="checkbox" class="styled" name="permissions[]" value="{{$permission->id}}" data-slug="{{$permission->slug}}">
                                                        {{$permission->name}}
                                                    </label>
                                                @endforeach
                                            </div>
                                            @endif
                                    @endforeach
                            </div>
                        </div>
                            
                            
                        </div>
                <!--/.col (full) -->
                     <button type="submit"  class="btn btn-info pull-right" style="margin-right: 25px;border:  none;">Submit</button>         
                          
                    </div> 
               </form>
            </div>
            <!-- /.box-body -->
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@stop

@section('js')
<!-- controls -->
 <script src="{{asset('Modules/Roles/Resources/assets/app/controles.js')}}"></script>
@stop
 