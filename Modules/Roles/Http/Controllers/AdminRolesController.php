<?php

namespace Modules\Roles\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Modules\Roles\Entities\Roles;
use Modules\Module\Entities\Modules;
use \Illuminate\Support\Facades\Session;

class AdminRolesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
         return view('roles::admin.index');
    }

     /**
     * Display a listing of the all roles.
     * @return Response Json array
     */
    public function allRoles()
    {
        return Datatables::of(Roles::select('id','name')->where('deleted_at',NULL))->make(true);
    }
    
     
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $modules = Modules::with('permissions')->where('status',1)->get();
        return view('roles::admin.create',compact('modules'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        
        $this->validate($request, ['role_name' => 'required|unique:admin_roles,name,NULL,id,deleted_at,NULL', 'role_slug' => 'required|unique:admin_roles,slug,NULL,id,deleted_at,NULL', 'permissions.*' => 'required', ]);
        $role = new Roles;
        $role->name = $request->role_name;
        $role->slug = $request->role_slug;
        $role->description = $request->description;
        $role->status = $request->Status;
        
        try
        {
            $role->save(); $id = $role->id;
            try{
                 $role->permissions()->attach($request->input('permissions'));
                 $request->session()->flash('val', 1);
                 $request->session()->flash('msg', "Role created successfully !");
                 return response()->json(['status'=>true,'url'=>URL('/admin/roles/'),'csrf' => csrf_token()]);
            } catch (Exception $ex) {
            $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
            return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$ex->getMessage(),'csrf' => csrf_token()]);
            }
             
            
        }
        catch (\Exception $e)
        {
            $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
            return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $modules = Modules::with('permissions')->where('status',1)->get();
        $role = Roles::with('permissions')->find($id);
        if($role==null){return redirect('/admin/404');}
        else{ return view('roles::admin.view',compact('modules','role'));}
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $modules = Modules::with('permissions')->where('status',1)->get();
        $role = Roles::with('permissions')->find($id);
        if($role==null){return redirect('/admin/404');}
        else{ return view('roles::admin.edit',compact('modules','role'));}
       
       
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id,Request $request)
    {
        $this->validate($request, ['role_name' => "required|unique:admin_roles,name,$id,id,deleted_at,NULL",'role_slug' => "required|unique:admin_roles,slug,$id,id,deleted_at,NULL", 'permissions.*' => 'required', ]);
        $role = Roles::find($id); 
        if($role==null){return response()->json(['status'=>true,'url'=>URL('/admin/404'),'csrf' => csrf_token()]);}
        else{
            $role->name = $request->role_name;
            $role->slug = $request->role_slug;
            $role->description = $request->description;
            $role->status = $request->Status;
            try
            {$role->save();
                try{
                     $role->permissions()->sync($request->input('permissions'));
                     $request->session()->flash('val', 1);
                     $request->session()->flash('msg', "Role updated successfully !");
                     return response()->json(['status'=>true,'url'=>URL('/admin/roles/'),'csrf' => csrf_token()]);
                } catch (Exception $ex) {
                $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$ex->getMessage().'</div>';
                return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$ex->getMessage(),'csrf' => csrf_token()]);
                }


            }catch (\Exception $e)
            {
                $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
                return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $role = Roles::find($id);
        if($role==null){return redirect('/admin/404');}
        else
        {
            try
            { 
                $role->delete();
                Session::flash('val', 1);
                Session::flash('msg', "Role deleted successfully !");

             } catch (Exception $ex) {
                Session::flash('val', 1);
                Session::flash('msg', $ex->getMessage());
             }
            return redirect('/admin/roles');
        }
    }
}
