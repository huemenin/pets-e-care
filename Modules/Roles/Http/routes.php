<?php

Route::group(['middleware' => 'web', 'prefix' => 'admin/roles', 'namespace' => 'Modules\Roles\Http\Controllers'], function()
{
     
    /* logged users opertaions */
    Route::group(['middleware' =>  'admin_auth:admin'], function()
    {
        Route::get('/', ['middleware' => 'check_perm:view-roles', 'uses' => 'AdminRolesController@index']);
        Route::get('/AdminRolesList',[ 'middleware' => 'check_perm:view-roles','uses' => 'AdminRolesController@allRoles', ]); 
        
        Route::get('/add', ['middleware' => 'check_perm:create-roles', 'uses' => 'AdminRolesController@create']);
        Route::get('/view/{id}', ['middleware' => 'check_perm:view-modules', 'uses' => 'AdminRolesController@show']);
        Route::post('/create', ['middleware' => 'check_perm:create-roles', 'uses' => 'AdminRolesController@store']);
        Route::get('/edit/{id}', ['middleware' => 'check_perm:edit-roles', 'uses' => 'AdminRolesController@edit']);
        Route::post('/update/{id}', ['middleware' => 'check_perm:edit-modules', 'uses' => 'AdminRolesController@update']);
        Route::get('/delete/{id}', ['middleware' => 'check_perm:delete-modules', 'uses' => 'AdminRolesController@destroy']); 
        
    });
    
});
