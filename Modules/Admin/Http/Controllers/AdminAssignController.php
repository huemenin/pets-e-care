<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Modules\Admin\Entities\Administrator;
use Modules\Roles\Entities\Roles;
use URL;
use Auth;
use \Illuminate\Support\Facades\Session;

class AdminAssignController extends Controller
{
    
    
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
       return view('admin::admin.index');
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function anyData()
    {
        return Datatables::of(Administrator::orderBy('id', 'DESC')->get())->make(true);
    }
    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $roles = Roles::where('id','!=',1)->get();
        return view('admin::admin.create',compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [ 'email' => 'required|unique:admin_users,email,NULL,id,deleted_at,NULL', ]);
        $admin = new Administrator();
        $admin->name = $request->name;
        $admin->email = $request->email;
  
        if ($request->hasFile('ProfileImage'))
        {
            $image = $request->file('ProfileImage');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/admin/users');
            $image->move($destinationPath, $name);   
            $admin->image =$name;
        }
        
        $admin->password = bcrypt($request->password);
        $admin->status = $request->Status;
        try
        {
            $admin->save();
            try
             {  
                $admin->roles()->attach($request->role); 
                $request->session()->flash('val', 1);
                $request->session()->flash('msg', "Admin user created successfully !");
                return response()->json(['status'=>true,'url'=>URL('/admin/'),'csrf' => csrf_token()]);
 
            } catch (Exception $ex) {
                $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
                return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$ex->getMessage(),'csrf' => csrf_token()]);
            }
       }
        catch (\Exception $e)
        {
            $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
            return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);
        }
  
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $roles = Roles::where('id','!=',1)->get();
        $admin = Administrator::with('GetRole')->where('id',$id)->get(); 
        if($admin->isEmpty()){return redirect('/admin/404');}
        else{
        return view('admin::admin.view',compact('admin','roles'));}
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $roles = Roles::where('id','!=',1)->get();
        $admin = Administrator::with('GetRole')->where('id',$id)->get(); 
        if($admin->isEmpty()){return redirect('/admin/404');}
        else{ return view('admin::admin.edit',compact('admin','roles'));}
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id,Request $request)
    {  
        $this->validate($request, ['email' => "required|unique:admin_users,email,$id,id"]);
        $admin = Administrator::find($id);
        $admin->name = $request->name;
        $admin->email = $request->email;
         
        if($request->password !="" || $request->password != null)
            $admin->password = bcrypt($request->password);
        
        $admin->status = $request->Status;
  
        if ($request->hasFile('ProfileImage'))
        {
            $image = $request->file('ProfileImage');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/admin/users');
            $image->move($destinationPath, $name);   
            $admin->image =$name;
        }
 
        try
        {
            $admin->save();
            try
             {  if($request->role !="" || $request->role !=null)
                $admin->roles()->sync($request->role);
                $request->session()->flash('val', 1);
                $request->session()->flash('msg', "Admin user updated successfully !");
                return response()->json(['status'=>true,'url'=>URL('/admin/'),'csrf' => csrf_token()]);
 
            } catch (Exception $ex) {
                $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
                return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$ex->getMessage(),'csrf' => csrf_token()]);
            }
       }
        catch (\Exception $e)
        {
            $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
            return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);
        }
        
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $admin = Administrator::find($id);
        if($admin==null){return redirect('/admin/404');}
        else
        {
            try
            { 
                $admin->delete();
                Session::flash('val', 1);
                Session::flash('msg', "Admin user deleted successfully !");

             } catch (Exception $ex) {
                Session::flash('val', 1);
                Session::flash('msg', $ex->getMessage());
             }
            return redirect('/admin');
        }
    }
    /**
     * Retrive the unique values.
     * @return array
     */
     function unique_multidim_array($array, $key)
    { 
        $temp_array = array(); 
        $i = 0; 
        $key_array = array(); 

        foreach($array as $val) 
        { 
            if (!in_array($val[$key], $key_array)) 
            { 
                $key_array[$i] = $val[$key]; 
                $temp_array[$i] = $val; 
            } 
            $i++; 
        } 
        return $temp_array; 
    } 
    
    
    /*
     * Retriving modules according to permission
     */
    public static function listingModule()
    {
        $return=array();
        $mod=   Auth::guard('admin')->user()->load('roles.permissions.modules');
        if($mod->roles->count() > 0)
        {
            $Admin    = new AdminAssignController();
            $modul  =  $mod->roles[0]->permissions->toArray();
            $return =   $Admin->unique_multidim_array($modul, 'modules');
        }
        
        return $return;
    }
}
