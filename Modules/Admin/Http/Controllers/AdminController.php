<?php

namespace Modules\Admin\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use URL;
use Cache;
 use Modules\Roles\Entities\Roles;
 use Modules\Permissions\Entities\Permissions;
 use Modules\Module\Entities\Modules;
 
class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        if(Auth::guard('admin')->user()) { return view('admin::admin.dashboard'); }
        else { return view('admin::admin.login'); }
        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('admin::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('admin::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('admin::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
    /**
     * checking admin login details.
     * @param email
     * @param password
     * @return Response json array
     */
    public function post_login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);
         
        $credentials = $request->only('email', 'password');
        $credentials['status']  =   1;
        if (Auth::guard('admin')->attempt($credentials, $request->has('remember')))
        {
            return response()->json(['status'=>true,'csrf' => csrf_token(),'url'=>URL::to('/admin/dashboard/')]);   
        }
        else
        {
            return response()->json(['status' => false, 'message' => trans('admin::auth.failed')]); 
        }
    }
    
    /**
     * dashboard.
     * @return Response
     */
    public function dashboard()
    {
        
         return view('admin::admin.dashboard');
    }
    
 
    /**
     * logout for admin users.
     * @return Response
     */
    public function logout()
    {
        Auth::guard('admin')->logout();
        Cache::flush();
        return redirect('/admin/login');
    }
    
    /**
     * Checking user Login.
     * @return true or false
     */
    public function CheckLogin()
    {
        if(Auth::guard('admin')->user()) 
        {
          return response()->json(['status'=>true,'csrf' => csrf_token()]);   
        }
        else 
        { 
         return response()->json(['status'=>false,'csrf' => csrf_token(),'url'=>URL::to('/admin/')]);   
        }
    }
    
    
    /**
     * Checking valid.
     * @return true or false
     */
    public function not_found()
    {
       return view('admin::admin.404'); 
    }
    
}
