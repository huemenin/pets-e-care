<?php

Route::group(['middleware' => 'web', 'prefix' => 'admin/admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{ 
    /* logged users opertaions */
    Route::group(['middleware' =>  'admin_auth:admin'], function(){
        Route::get('/', ['middleware' => 'check_perm:view-admin', 'uses' => 'AdminAssignController@index']);
 
    });
    
});



Route::group(['middleware' => 'web', 'prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
    Route::get('/', 'AdminController@index');
    Route::get('/login', 'AdminController@index');
    Route::post('/post_login', 'AdminController@post_login');
    Route::get('/logout', 'AdminController@logout');
    Route::get('/CheckLogin', 'AdminController@CheckLogin');
    Route::get('/404', 'AdminController@not_found');
     
    
    /* logged users opertaions */
    Route::group(['middleware' =>  'admin_auth:admin'], function(){
        
    	Route::get('/dashboard', 'AdminController@dashboard'); 
        Route::get('/', ['middleware' => 'check_perm:view-admin', 'uses' => 'AdminAssignController@index']);
        Route::get('/AdminUserList',[ 'middleware' => 'check_perm:view-admin', 'as'=>'AdminUserList.all',  'uses' => 'AdminAssignController@anyData', ]); 
        Route::get('/add', ['middleware' => 'check_perm:create-admin', 'uses' => 'AdminAssignController@create']);
        Route::post('/create', ['middleware' => 'check_perm:create-admin', 'uses' => 'AdminAssignController@store']);
        Route::get('/view/{id}', ['middleware' => 'check_perm:view-admin', 'uses' => 'AdminAssignController@show']);
        Route::get('/edit/{id}', ['middleware' => 'check_perm:edit-admin', 'uses' => 'AdminAssignController@edit']);
        Route::post('/update/{id}', ['middleware' => 'check_perm:edit-admin', 'uses' => 'AdminAssignController@update']);
        Route::get('/delete/{id}', ['middleware' => 'check_perm:delete-admin', 'uses' => 'AdminAssignController@destroy']); 
        
        
    });
    
});
