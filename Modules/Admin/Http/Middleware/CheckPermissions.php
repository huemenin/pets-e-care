<?php

namespace Modules\Admin\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Modules\Module\Entities\Modules;
use Modules\Roles\Entities\Roles;
class CheckPermissions
{
    /**
     * For checking user permission
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next ,$permission = null,$guard="admin")
    {
         if (!Auth::guard($guard)->check()) 
        {
            if ($request->ajax()) 
            { 
                return response([
                    'error' => 'unauthorized',
                    'error_description' => 'Failed authentication.',
                    'data' => [],
                ], 401);
            } 
            else 
            {
                return redirect('/admin/login');
            }

        }
        else{
            $user = Auth::guard($guard)->user(); 
            $roles = $user->load('roles')->roles->toArray();
            
            if(!empty($roles))
            {
                 $role_perms = Roles::with(['permissions' => function($query){
                $query->where('admin_permissions.status',1);
                $query->where('admin_permissions.deleted_at',null);
            }])->where('id',$roles[0]['id'])->get();
            
            $permissions = $role_perms[0]->permissions->pluck('slug')->toArray();
            
            if(!in_array($permission,$permissions)){ return redirect('/admin/acces-denied');}
        
            }else{
                return redirect('/admin/logout');
            }
          
        }
        return $next($request);
    }
}
