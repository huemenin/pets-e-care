<?php

namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class SeedAllCountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        if(DB::table('all_countries')->get()->count() == 0)
        {
            $tasks =[
                [
                'name' => 'Afghanistan',
                'flag' => 'af',
                'code' => 'afg',
                'currency' =>'Afghanis',
                'currency_code' =>'AF',
                'symbol' => '؋',
                ],
                [
                'name' => 'Albania',
                'flag' => 'al',
                'code' => 'alb',
                'currency' =>'Leke',
                'currency_code' =>'ALL',
                'symbol' => 'Lek',
                ],

                [
                'name' => 'Argentina',
                'flag' => 'ar',
                'code' => 'arg',
                'currency' =>'Pesos',
                'currency_code' =>'ARS',
                'symbol' =>'$' ,
                ],

                [
                'name' => 'Aruba',
                'flag' => 'aw' ,
                'code' =>'abw' ,
                'currency' =>'Guilders',
                'currency_code' =>'AWG',
                'symbol' =>  'ƒ',
                ],

                [
                'name' => 'Australia',
                'flag' => 'au' ,
                'code' => 'aus',
                'currency' => 'Dollars',
                'currency_code' =>'AUD',
                'symbol' => '$' ,
                ],

                [
                'name' => 'Azerbaijan',
                'flag' => 'az',
                'code' =>  'aze', 
                'currency' => 'New Manats',
                'currency_code' => 'AZ',
                'symbol' => 'ман',
                ],

                [
                'name' => 'Bahamas',
                'flag' => 'bs', 
                'code' => 'bhs',
                'currency' => 'Dollars',
                'currency_code' => 'BSD',
                'symbol' =>  '$',
                ],
                [
                'name' =>'Barbados',
                'flag' => 'bb',
                'code' => 'brb',
                'currency' =>  'Dollars',
                'currency_code' => 'BBD',
                'symbol' =>  '$',
                ],

                [
                'name' =>'Belarus',
                'flag' =>'by', 
                'code' =>  'blr',
                'currency' => 'Rubles',
                'currency_code' =>  'BYR',
                'symbol' =>  'p.',
                ],

                [
                'name' =>'Belgium',
                'flag' =>'be', 
                'code' =>  'bel',
                'currency' =>  'Euro',
                'currency_code' =>  'EUR',
                'symbol' =>  '€',
                ],

                [
                'name' =>'Bermuda',
                'flag' =>'bm', 
                'code' => 'bmu',
                'currency' => 'Dollars',
                'currency_code' => 'BMD',
                'symbol' => '$',
                ],

                [
                'name' =>'Bosnia and Herzegovina',
                'flag' =>'ba', 
                'code' => 'bih',
                'currency' => 'Convertible Marka',
                'currency_code' => 'BAM',
                'symbol' => 'KM',
                ],

                [
                'name' =>'Botswana',
                'flag' =>'bw', 
                'code' => 'bwa',
                'currency' => 'Pula\'s',
                'currency_code' => 'BWP',
                'symbol' => 'P',
                ],

                [
                'name' =>'Brazil',
                'flag' =>'br', 
                'code' => 'bra',
                'currency' =>'Reais',
                'currency_code' => 'BRL',
                'symbol' => 'R$',
                ],

                [
                'name' =>'Brunei Darussalam',
                'flag' =>'bn', 
                'code' => 'brn',
                'currency' =>'Dollars', 
                'currency_code' => 'BND',
                'symbol' => '$',
                ],

                [
                'name' =>'Bulgaria',
                'flag' =>'bg', 
                'code' => 'bgr',
                'currency' =>'Leva', 
                'currency_code' => 'BG',
                'symbol' =>'лв',
                ],

                [
                'name' =>'Cambodia',
                'flag' =>'kh', 
                'code' => 'khm',
                'currency' =>'Riels', 
                'currency_code' =>'KHR',
                'symbol' =>'៛',
                ],

                [
                'name' =>'Canada',
                'flag' =>'ca', 
                'code' => 'can',
                'currency' =>'Dollars', 
                'currency_code' =>'CAD',
                'symbol' =>'$',
                ],

                [
                'name' =>'Cayman Islands',
                'flag' =>'ky', 
                'code' => 'cym',
                'currency' =>'Dollars', 
                'currency_code' => 'KYD',
                'symbol' =>'$',
                ],

                [
                'name' =>'Chile',
                'flag' =>'cl', 
                'code' => 'chl',
                'currency' =>'Pesos',  
                'currency_code' => 'CLP',
                'symbol' =>'$',
                ],

                [
                'name' =>'China',
                'flag' =>'cn', 
                'code' => 'chn',
                'currency' =>'Yuan Renminbi', 
                'currency_code' => 'CNY',
                'symbol' =>'¥',
                ],

                [
                'name' =>'Colombia',
                'flag' =>'co', 
                'code' => 'col',
                'currency' =>'Pesos', 
                'currency_code' => 'COP',
                'symbol' =>'$',
                ],

                [
                'name' => 'Costa Rica',
                'flag' =>'cr', 
                'code' => 'cri',
                'currency' =>'Colón', 
                'currency_code' => 'CRC',
                'symbol' => '₡',
                ],

                [
                'name' =>'Croatia',
                'flag' =>'hr', 
                'code' => 'hrv',
                'currency' =>'Kuna', 
                'currency_code' => 'HRK',
                'symbol' =>'kn',
                ],

                [
                'name' =>'Cuba',
                'flag' =>'cu', 
                'code' => 'cub',
                'currency' =>'Pesos', 
                'currency_code' => 'CUP',
                'symbol' =>'₱',
                ],

                [
                'name' =>'Cyprus',
                'flag' =>'cy', 
                'code' => 'cyp',
                'currency' =>'Euro', 
                'currency_code' => 'EUR',
                'symbol' =>'€',
                ],

                [
                'name' =>'Denmark',
                'flag' =>'dk', 
                'code' => 'dnk',
                'currency' =>'Kroner',  
                'currency_code' => 'DKK',
                'symbol' =>'kr',
                ],

                [
                'name' =>'Dominican Republic',
                'flag' =>'do', 
                'code' => 'dom',
                'currency' =>'Pesos',  
                'currency_code' => 'DOP',
                'symbol' =>'RD$',
                ],

                [
                'name' =>'Egypt',
                'flag' =>'eg', 
                'code' => 'egy',
                'currency' =>'Pounds',   
                'currency_code' => 'EGP',
                'symbol' =>'£',
                ],

                [
                'name' =>'El Salvador',
                'flag' =>'sv', 
                'code' => 'slv',
                'currency' =>'Colones',   
                'currency_code' => 'SVC',
                'symbol' =>'$',
                ],

                [
                'name' =>'Fiji', 
                'flag' =>'fj', 
                'code' => 'fji',
                'currency' =>'Dollars',   
                'currency_code' => 'FJD',
                'symbol' =>'$',
                ],

                [
                'name' =>'France', 
                'flag' =>'fr', 
                'code' => 'fra',
                'currency' =>'Euro',   
                'currency_code' =>  'EUR',
                'symbol' =>'€'
                ],

                [
                'name' =>'Ghana', 
                'flag' =>'gh', 
                'code' => 'gha',
                'currency' =>'Cedis',   
                'currency_code' =>  'GHC',
                'symbol' =>'¢',
                ],

                [
                'name' =>'Gibraltar',  
                'flag' =>'gi', 
                'code' => 'gib',
                'currency' =>'Pounds',   
                'currency_code' =>  'GIP',
                'symbol' =>'£',
                ],
                [
                'name' =>'Greece', 
                'flag' =>'gr', 
                'code' => 'grc',
                'currency' =>'Euro',    
                'currency_code' => 'EUR',
                'symbol' =>'€',
                ],

                [
                'name' =>'Guatemala',
                'flag' =>'gt', 
                'code' => 'gtm',
                'currency' =>'Quetzales',     
                'currency_code' => 'GTQ',
                'symbol' =>'Q',
                ],

                [
                'name' =>'Guernsey',
                'flag' =>'gg', 
                'code' => 'ggy',
                'currency' =>'Pounds',     
                'currency_code' => 'GGP',
                'symbol' =>'£',
                ],

                [
                'name' =>'Guyana',
                'flag' =>'gy', 
                'code' => 'guy',
                'currency' => 'Dollars',    
                'currency_code' => 'GYD',
                'symbol' =>'$',
                ],

                [
                'name' =>'Honduras',
                'flag' =>'hn', 
                'code' => 'hnd',
                'currency' => 'Lempiras',    
                'currency_code' => 'HNL',
                'symbol' =>'L',
                ],

                [
                'name' => 'Hong Kong',
                'flag' =>'hk', 
                'code' => 'hkg',
                'currency' => 'Dollars',   
                'currency_code' => 'HKD',
                'symbol' => '$',
                ],

                [
                'name' => 'Hungary', 
                'flag' =>'hu', 
                'code' => 'hun',
                'currency' => 'Forint',   
                'currency_code' => 'HUF',
                'symbol' => 'Ft',
                ],

                [
                'name' => 'Iceland', 
                'flag' =>'is', 
                'code' => 'isl',
                'currency' => 'Kronur',   
                'currency_code' => 'ISK',
                'symbol' => 'kr',
                ],

                [
                'name' => 'India', 
                'flag' =>'in', 
                'code' => 'ind',
                'currency' =>  'Rupees',   
                'currency_code' => 'INR',
                'symbol' => '₹',
                ],

                [
                'name' => 'Indonesia', 
                'flag' =>'id', 
                'code' => 'idn',
                'currency' =>'Rupiahs',   
                'currency_code' => 'IDR',
                'symbol' => 'Rp',
                ],

                [
                'name' =>'Ireland', 
                'flag' =>'ie', 
                'code' => 'irl',
                'currency' =>'Euro',   
                'currency_code' => 'EUR',
                'symbol' =>'€',
                ],

                [
                'name' =>'Isle of Man', 
                'flag' =>'im', 
                'code' => 'imn',
                'currency' =>'Pounds',   
                'currency_code' => 'IMP',
                'symbol' => '£',
                ],

                [
                'name' =>'Israel', 
                'flag' =>'il', 
                'code' => 'isr', 
                'currency' => 'New Shekels',   
                'currency_code' =>  'ILS',
                'symbol' => '₪',
                ],        

                [
                'name' => 'Italy', 
                'flag' =>'it', 
                'code' => 'ita', 
                'currency' =>'Euro',   
                'currency_code' =>'EUR',
                'symbol' => '€',
                ],        

                [
                'name' => 'Jamaica', 
                'flag' =>'jm', 
                'code' => 'jam', 
                'currency' =>'Dollars',  
                'currency_code' =>'JMD', 
                'symbol' => 'J$',
                ],        

                [
                'name' => 'Japan',
                'flag' =>'jp', 
                'code' => 'jpn', 
                'currency' =>'Yen',  
                'currency_code' =>'JPY', 
                'symbol' => '¥',
                ],       

                [
                'name' => 'Jersey', 
                'flag' =>'je', 
                'code' => 'jey', 
                'currency' => 'Pounds',  
                'currency_code' =>'JEP',
                'symbol' => '£',
                ],       
                [
                'name' => 'Kazakhstan',
                'flag' =>'kz', 
                'code' => 'kaz', 
                'currency' =>'Tenge',   
                'currency_code' =>'KZT',
                'symbol' => 'лв',
                ],       

                [
                'name' => 'Kyrgyzstan',
                'flag' =>'kg', 
                'code' => 'kgz', 
                'currency' =>'Soms',   
                'currency_code' =>'KGS',
                'symbol' => 'лв',
                ],

                [
                'name' =>'Latvia',
                'flag' =>'lv', 
                'code' => 'lva', 
                'currency' => 'Lati',   
                'currency_code' =>'LVL',
                'symbol' =>'Ls',
                ], 

                [
                'name' =>'Lebanon',
                'flag' =>'lb', 
                'code' => 'lbn', 
                'currency' => 'Pounds',   
                'currency_code' =>'LBP',
                'symbol' => '£',
                ],       

                [
                'name' =>'Lebanon',
                'flag' =>'lb', 
                'code' => 'lbn', 
                'currency' => 'Pounds',   
                'currency_code' =>'LBP',
                'symbol' => '£',
                ],       

                [
                'name' => 'Liberia', 
                'flag' =>'lr', 
                'code' => 'lbr', 
                'currency' => 'Dollars',  
                'currency_code' =>'LRD',
                'symbol' => '$',
                ],       

                [
                'name' =>  'Liechtenstein',
                'flag' =>'li', 
                'code' => 'lie', 
                'currency' => 'Switzerland Francs',  
                'currency_code' =>'CHF',
                'symbol' => 'CHF',
                ],       

                [
                'name' =>  'Lithuania',
                'flag' =>'lt', 
                'code' => 'ltu', 
                'currency' =>'Litai',  
                'currency_code' =>'LTL',
                'symbol' => 'Lt',
                ], 

                [
                'name' =>'Luxembourg',
                'flag' =>'lu',
                'code' => 'lux', 
                'currency' =>'Euro',  
                'currency_code' =>'EUR',
                'symbol' => '€',
                ],  

                [
                'name' => 'Malaysia',
                'flag' =>'my',
                'code' => 'mys', 
                'currency' =>'Ringgits',  
                'currency_code' =>'MYR',
                'symbol' => 'RM',
                ],

                [
                'name' =>'Malta',
                'flag' =>'mt',
                'code' => 'mlt', 
                'currency' => 'Euro',  
                'currency_code' =>'EUR',
                'symbol' => '€',
                ],  
                [
                'name' => 'Mauritius',
                'flag' =>'mu',
                'code' => 'mus', 
                'currency' => 'Rupees', 
                'currency_code' =>'MUR',
                'symbol' => '₨',
                ],

                [
                'name' =>  'Mexico',
                'flag' =>'mx',
                'code' => 'mex', 
                'currency' => 'Pesos', 
                'currency_code' =>'MX',
                'symbol' => '$',
                ], 

                [
                'name' => ' Mongolia',
                'flag' =>'mn',
                'code' => 'mng', 
                'currency' =>  'Tugriks', 
                'currency_code' =>'MNT',
                'symbol' => '₮',
                ],

                [
                'name' =>  'Mozambique',
                'flag' =>'mz',
                'code' => 'moz', 
                'currency' => 'Meticais', 
                'currency_code' =>'MZ',
                'symbol' =>'MT',
                ], 
                [
                'name' =>  'Mozambique',
                'flag' =>'mz',
                'code' => 'moz', 
                'currency' => 'Meticais', 
                'currency_code' =>'MZ',
                'symbol' =>'MT',
                ], 
                [
                'name' =>'Namibia',
                'flag' =>'na',
                'code' => 'nam', 
                'currency' =>  'Dollars', 
                'currency_code' =>'NAD',
                'symbol' =>'$',
                ], 

                [
                'name' =>'Nepal',
                'flag' =>'np',
                'code' => 'npl', 
                'currency' => 'Rupees', 
                'currency_code' =>'NPR',
                'symbol' => '₨',
                ], 

                [
                'name' => 'Netherlands',
                'flag' =>'nl',
                'code' => 'nld', 
                'currency' => 'Euro',
                'currency_code' => 'EUR',
                'symbol' =>  '€',
                ], 

                [
                'name' => 'New Zealand',
                'flag' => 'nz',
                'code' => 'nzl', 
                'currency' => 'Dollars', 
                'currency_code' =>  'NZD',
                'symbol' =>  '$',
                ], 
                [
                'name' =>'Nicaragua',
                'flag' => 'ni',
                'code' => 'nic', 
                'currency' =>'Cordobas',
                'currency_code' =>  'NIO',
                'symbol' =>   'C$',
                ], 

                [
                'name' =>'Nigeria',
                'flag' => 'ng',
                'code' => 'nga', 
                'currency' =>'Nairas',
                'currency_code' =>  'NG',
                'symbol' => '₦',
                ], 

                [
                'name' =>'Norway',
                'flag' => 'no',
                'code' => 'nor', 
                'currency' =>'Krone',
                'currency_code' =>  'NOK', 
                'symbol' => 'kr',
                ], 

                [
                'name' =>'Oman',
                'flag' =>  'om',
                'code' =>  'omn', 
                'currency' =>'Rials',
                'currency_code' =>  'OMR',
                'symbol' => '﷼',
                ], 

                [
                'name' => 'Pakistan',
                'flag' => 'pk',
                'code' =>  'pak', 
                'currency' =>'Rupees', 
                'currency_code' =>  'PKR',
                'symbol' => '₨',
                ], 

                [
                'name' =>'Panama',
                'flag' =>'pa',
                'code' => 'pan',
                'currency' =>'Balboa', 
                'currency_code' =>   'PAB',
                'symbol' => 'B/.',
                ], 
                [
                'name' => 'Paraguay',
                'flag' =>  'py',
                'code' => 'pry',
                'currency' =>'Guarani', 
                'currency_code' =>  'PYG', 
                'symbol' => 'Gs',
                ], 
                [
                'name' => 'Peru', 
                'flag' => 'pe',
                'code' =>'per',
                'currency' =>'Nuevos Soles', 
                'currency_code' => 'PE', 
                'symbol' => 'S/.',
                ], 
                [
                'name' => 'Philippines',
                'flag' =>  'ph', 
                'code' =>  'phl', 
                'currency' =>'Pesos',
                'currency_code' =>'PHP',
                'symbol' => 'Php',
                ], 
                [
                'name' => 'Poland',
                'flag' => 'pl',
                'code' => 'pol', 
                'currency' => 'Zlotych', 
                'currency_code' =>'PL',
                'symbol' => 'zł',
                ], 
                [
                'name' =>'Qatar', 
                'flag' => 'qa',
                'code' => 'qat',  
                'currency' =>  'Rials',  
                'currency_code' =>'QAR',
                'symbol' => '﷼',
                ], 
                [
                'name' =>'Romania',
                'flag' => 'ro',
                'code' => 'rou',   
                'currency' => 'New Lei',   
                'currency_code' =>'RO',
                'symbol' => 'lei',
                ], 
                [
                'name' =>'Saudi Arabia', 
                'flag' =>  'sa',
                'code' =>'sau',   
                'currency' => 'Riyals',   
                'currency_code' =>'SAR',
                'symbol' =>'﷼',
                ],
                [
                'name' =>'Serbia', 
                'flag' => 'rs',
                'code' =>'srb',   
                'currency' => 'Dinars', 
                'currency_code' =>'RSD',
                'symbol' => 'Дин.',
                ],
                [
                'name' => 'Seychelles', 
                'flag' => 'sc', 
                'code' =>'syc',  
                'currency' => 'Rupees',   
                'currency_code' =>'SCR',
                'symbol' => '₨',
                ],
                [
                'name' =>  'Singapore', 
                'flag' =>  'sg', 
                'code' =>'sgp',   
                'currency' => 'Dollars',    
                'currency_code' =>'SGD',
                'symbol' =>  '$',
                ],
                [
                'name' =>  'Slovenia', 
                'flag' => 'si', 
                'code' => 'svn',    
                'currency' => 'Euro',   
                'currency_code' =>'EUR',
                'symbol' =>'€',
                ],
                [
                'name' =>  'Solomon Islands',  
                'flag' => 'sb',
                'code' => 'slb',    
                'currency' =>'Dollars',   
                'currency_code' =>'SBD', 
                'symbol' =>'$',
                ],
                [
                'name' =>  'Somalia',
                'flag' =>  'so',
                'code' => 'som' ,
                'currency' =>'Shillings',  
                'currency_code' => 'SOS',
                'symbol' =>'S',
                ],
                [
                'name' =>  'South Africa',
                'flag' =>  'za',
                'code' =>'zaf',  
                'currency' => 'Rand', 
                'currency_code' =>  'ZAR',
                'symbol' => 'R',
                ],
                [
                'name' => 'Spain',
                'flag' =>  'es',
                'code' => 'esp',  
                'currency' => 'Euro',
                'currency_code' =>  'EUR',
                'symbol' => '€',
                ],
                [
                'name' =>  'Sri Lanka',
                'flag' => 'lk',
                'code' =>'lka',  
                'currency' =>  'Rupees',
                'currency_code' => 'LKR',
                'symbol' => '₨',
                ],
                [
                'name' =>   'Suriname',
                'flag' => 'sr', 
                'code' =>'sur',  
                'currency' =>'Dollars',
                'currency_code' =>  'SRD', 
                'symbol' =>'$',
                ],
                [
                'name' =>   'Sweden',
                'flag' =>  'se', 
                'code' =>'swe', 
                'currency' =>'Kronor', 
                'currency_code' =>  'SEK',
                'symbol' =>'kr',
                ],
                [
                'name' =>  'Switzerland',
                'flag' =>  'ch', 
                'code' =>'che', 
                'currency' =>'Francs',
                'currency_code' =>  'CHF',
                'symbol' => 'CHF',
                ],
                [
                'name' =>   'Thailand', 
                'flag' =>   'th',  
                'code' => 'tha', 
                'currency' =>'Baht',
                'currency_code' =>  'THB',
                'symbol' => '฿',
                ],
                [
                'name' =>   'Trinidad and Tobago',
                'flag' =>  'tt', 
                'code' =>'tto',
                'currency' =>'Dollars', 
                'currency_code' =>  'TTD',
                'symbol' =>'TT$',
                ],
                [
                'name' => 'Turkey', 
                'flag' => 'tr',
                'code' =>'tur',
                'currency' =>'Lira',
                'currency_code' =>  'TRY',
                'symbol' =>'TL',
                ],
                [
                'name' => 'Turkey', 
                'flag' => 'tr',
                'code' =>'tur',
                'currency' =>'Liras',
                'currency_code' =>  'TRY',
                'symbol' => '£',
                ],
                [
                'name' => 'Tuvalu', 
                'flag' => 'tv',
                'code' =>'tuv',
                'currency' =>'Dollars',
                'currency_code' => 'TVD',
                'symbol' =>  '$',
                ],
                [
                'name' => 'Ukraine',  
                'flag' =>  'ua',
                'code' =>'ukr',
                'currency' =>'Hryvnia',
                'currency_code' =>  'UAH',
                'symbol' =>  '₴',
                ],
                [
                'name' => 'United States of America',  
                'flag' =>  'us',
                'code' => 'usa',
                'currency' =>'Dollars',
                'currency_code' =>  'USD',
                'symbol' =>   '$',
                ],
                [
                'name' => 'Uruguay',  
                'flag' =>  'uy',
                'code' => 'ury',
                'currency' => 'Pesos',
                'currency_code' =>  'UYU',
                'symbol' =>   '$U',
                ],
                [
                'name' =>'Uzbekistan', 
                'flag' =>   'uz',
                'code' => 'uzb',
                'currency' =>'Sums',
                'currency_code' =>  'UZS',
                'symbol' =>  'лв',
                ],
                [
                'name' =>'Yemen',
                'flag' =>   'ye',
                'code' => 'yem',
                'currency' => 'Rials',
                'currency_code' =>'YER',
                'symbol' => '﷼',
                ],
                [
                'name' =>'Zimbabwe', 
                'flag' =>  'zw',
                'code' =>  'zwe',
                'currency' => 'Zimbabwe Dollars',
                'currency_code' =>'ZWD',
                'symbol' => 'Z$',
                ]
            ];
            DB::table('all_countries')->insert($tasks);
        }
        // $this->call("OthersTableSeeder");
    }
}
