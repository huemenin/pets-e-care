<?php

namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class SeedCountriesStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void 43
     */
    public function run()
    {
        Model::unguard();
        if(DB::table('countries_states')->get()->count() == 0)
        {
            $tasks =[
                [
                'name' => 'Andra Pradesh',
                'country_id' => '43'
                ],
                [
                'name' => 'Arunachal Pradesh',
                'country_id' => '43'
                ],
                [
                'name' => 'Assam',
                'country_id' => '43'
                ],
                [
                'name' => 'Bihar',
                'country_id' => '43'
                ],
                [
                'name' => 'Chhattisgarh',
                'country_id' => '43'
                ],
                [
                'name' => 'Goa',
                'country_id' => '43'
                ],
                [
                'name' => 'Gujarat',
                'country_id' => '43'
                ],
                [
                'name' => 'Haryana',
                'country_id' => '43'
                ],
                [
                'name' => 'Himachal Pradesh',
                'country_id' => '43'
                ],
                [
                'name' => 'Jammu and Kashmir',
                'country_id' => '43'
                ],
                [
                'name' => 'Jharkhand',
                'country_id' => '43'
                ],
                [
                'name' => 'Karnataka',
                'country_id' => '43'
                ],
                [
                'name' => 'Kerala',
                'country_id' => '43'
                ],
                [
                'name' => 'Madya Pradesh',
                'country_id' => '43'
                ],
                [
                'name' => 'Maharashtra',
                'country_id' => '43'
                ],
                [
                'name' => 'Manipur',
                'country_id' => '43'
                ],
                [
                'name' => 'Meghalaya',
                'country_id' => '43'
                ],
                [
                'name' => 'Mizoram',
                'country_id' => '43'
                ],
                [
                'name' => 'Nagaland',
                'country_id' => '43'
                ],
                [
                'name' => 'Orissa',
                'country_id' => '43'
                ],
                [
                'name' => 'Punjab',
                'country_id' => '43'
                ],
                [
                'name' => 'Rajasthan',
                'country_id' => '43'
                ],
                [
                'name' => 'Sikkim',
                'country_id' => '43'
                ],
                [
                'name' => 'Tamil Nadu',
                'country_id' => '43'
                ],
                [
                'name' => 'Tripura',
                'country_id' => '43'
                ],
                [
                'name' => 'Uttaranchal',
                'country_id' => '43'
                ],
                [
                'name' => 'Uttar Pradesh',
                'country_id' => '43'
                ],
                [
                'name' => 'West Bengal',
                'country_id' => '43'
                ],
                [
                'name' => 'Andaman and Nicobar Islands',
                'country_id' => '43'
                ],
                [
                'name' => 'Chandigarh',
                'country_id' => '43'
                ],
                [
                'name' => 'Dadar and Nagar Haveli',
                'country_id' => '43'
                ],
                [
                'name' => 'Daman and Diu',
                'country_id' => '43'
                ],
                [
                'name' => 'Delhi',
                'country_id' => '43'
                ],
                [
                'name' => 'Lakshadeep',
                'country_id' => '43'
                ],
                [
                'name' => 'Pondicherry',
                'country_id' => '43'
                ]  
               
            ];
            DB::table('countries_states')->insert($tasks);
        }
        // $this->call("OthersTableSeeder");
    }
}
