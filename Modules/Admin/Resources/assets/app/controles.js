     
$(function() {
    if($('#users-table').length)
    {
        $('#users-table').DataTable({
             processing: true,
             serverSide: true,  
             ajax: base_url+"/admin/AdminUserList",
             columns: [
                 { data: 'id', name: 'id' },
                 { data: 'name', name: 'name' },
                 { data: 'email', name: 'email' },
                 { data: 'created_at', name: 'created_at' },
                 {
                         data: "null",
                         'searchable': false, 
                         'orderable': false,
                         render: function (data, type, full) 
                         { 
                             
                           var u ; 
                             if(full.id==1)
                             {
                                  
                                u='<a href="'+base_url+'/admin/view/'+full.id+'" class="iconclass"><i class="fa  fa-eye"></i></a>'; 
                             }else
                             {
                                           u = '<a href="'+base_url+'/admin/view/'+full.id+'" class="iconclass"><i class="fa  fa-eye"></i></a>' 
                                        +''+'<a href="'+base_url+'/admin/edit/'+full.id+'" class="iconclass"><i class="fa  fa-pencil"></i></a>'
                                           +''+'<a href="'+base_url+'/admin/delete/'+full.id+'" class="iconclass"  Onclick="return ConfirmDelete();"><i class="fa  fa-trash"></i></a>';

                             }
                      return u;
                             
                         }
                 }
             ]

        });
    }
/* ************************************************************************* */  
/* *************************** data table listing end ********************** */  
/* ************************************************************************* */  
  
         /*
     * create form 
     * params : Name,Status,email,role,password  
     */
      
    $("#admin_create").submit(function(e)
    {
        e.preventDefault(); 
         
          
        var name       =   $("[name='name']").val().trim();
        var email      =   $("[name='email']").val().trim();
        var password   =   $("[name='password']").val().trim();
        var Status     =   $("[name='Status']").val().trim();
        var role       =   $("[name='role']").val().trim();
      
        var a=0;
        var b=0;
        var c=0; 
        var d=0; 
        var e=0; 
  
        //Name
        if(name.length > 0){a=1; $( "#Name_Err" ).removeClass( "has-error" );}
        else{a=0; $( "#Name_Err" ).addClass( "has-error" );}
 
        //email
        if(email.length > 0)
        {    
            if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) ){ b=1; $("#Email_Err").removeClass( "has-error" ); }
            else{  b=0;  $( "#Email_Err" ).addClass( "has-error" );}
        }
        else{b=0;  $( "#Email_Err" ).addClass( "has-error" ); }
        
        //password
        if(password.length > 0){c=1; $( "#Password_Err" ).removeClass( "has-error" );}
        else{c=0; $( "#Password_Err" ).addClass( "has-error" );}
        
        //Status
        if(role != 'select'){d=1; $( "#Role_Err" ).removeClass( "has-error" );}
        else{d=0; $( "#Role_Err" ).addClass( "has-error" );}
        
        //Status
        if(Status=='0' || Status=='1'){e=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{e=0; $( "#Status_Err" ).addClass( "has-error" );}
 
   
//        /* ------------------------------------------------------------------ */
//        /* ----------------- form submitting -------------------------------- */
//        /* ------------------------------------------------------------------ */
//        
        if(a==1 && b==1 && c==1 && d==1 && e==1)
        {
//            
            $("#EmailCheck").html(" ");  
            $(".showinfo").html(" ");
            $( "#EmailCheck" ).removeClass( "has-error" ); 
            
               $.ajax({
                    type: "POST",
                    url:base_url+"/admin/create",
                    dataType: "json",
                    async: false, 
                    data: new FormData($('#admin_create')[0]),
                    processData: false,
                    contentType: false, 
                    success: function(response)
                    {     
                         if(response.status==true){window.location.href = response.url; }
                         else{$(".showinfo").html(response.alert);}
                    },
                    error: function (request, textStatus, errorThrown) {

                        var obj = request.responseJSON.errors ;

                        if(obj.hasOwnProperty("email") )
                        {
                           $( "#Email_Err" ).addClass( "has-error" );
                           $("#EmailCheck").html("<div class='mad'>"+request.responseJSON.errors.email[0]+"</div>");   
                        }
//
 
                    }
                });
        }
        
        return false;
        
    });
  
/* ************************************************************************* */  
/* *************************** Admin user create end *********************** */  
/* ************************************************************************* */  
     
    /*
     * update form 
     * params : Name,Status,email,role,password
     */
      
    $("#admin_update").submit(function(e)
    {
        e.preventDefault(); 
         var action=   $("#admin_update").attr('action');
         
        
         
        var name       =   $("[name='name']").val().trim();
        var email      =   $("[name='email']").val().trim(); 
        var Status     =   $("[name='Status']").val().trim();
        var role       =   $("[name='role']").val().trim();
      
        var a=0;
        var b=0;
        var c=0; 
        var d=0; 
        var e=0; 
  
        //Name
        if(name.length > 0){a=1; $( "#Name_Err" ).removeClass( "has-error" );}
        else{a=0; $( "#Name_Err" ).addClass( "has-error" );}
 
        //email
        if(email.length > 0)
        {    
            if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) ){ b=1; $("#Email_Err").removeClass( "has-error" ); }
            else{  b=0;  $( "#Email_Err" ).addClass( "has-error" );}
        }
        else{b=0;  $( "#Email_Err" ).addClass( "has-error" ); }
        
 
        //Status
        if(role != 'select'){c=1; $( "#Role_Err" ).removeClass( "has-error" );}
        else{c=0; $( "#Role_Err" ).addClass( "has-error" );}
        
        //Status
        if(Status=='0' || Status=='1'){d=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{d=0; $( "#Status_Err" ).addClass( "has-error" );}
 
   
//        /* ------------------------------------------------------------------ */
//        /* ----------------- form submitting -------------------------------- */
//        /* ------------------------------------------------------------------ */
//        
        if(a==1 && b==1 && c==1 && d==1  )
        {
//            
            $("#EmailCheck").html(" ");  
            $(".showinfo").html(" ");
            $( "#EmailCheck" ).removeClass( "has-error" ); 
            
               $.ajax({
                    type: "POST",
                    url:action,
                    dataType: "json",
                    async: false, 
                    data: new FormData($('#admin_update')[0]),
                    processData: false,
                    contentType: false, 
                    success: function(response)
                    {     
                         if(response.status==true){window.location.href = response.url; }
                         else{$(".showinfo").html(response.alert);}
                    },
                    error: function (request, textStatus, errorThrown) {

                        var obj = request.responseJSON.errors ;

                        if(obj.hasOwnProperty("email") )
                        {
                           $( "#Email_Err" ).addClass( "has-error" );
                           $("#EmailCheck").html("<div class='mad'>"+request.responseJSON.errors.email[0]+"</div>");   
                        }
                    }
                });
        }
        
        return false;
        
    });
  
/* ************************************************************************* */  
/* *************************** Admin user update end *********************** */  
/* ************************************************************************* */  
    
    
    
    
/* ************************************************************************* */  
/* ****************************** function end ***************************** */  
/* ************************************************************************* */  
    
});
