$(function() 
{
 
/* ************************************************************************** */  
/* ************************************************************************** */ 
/* ************************************************************************** */

    /*
     * login form 
     * params : email,password  
     */
    
    $("#submit").click(function(e) 
    {
        // validate and process form here
        e.preventDefault; 
        var email       =   $("[name='email']").val().trim();
        var password    =   $("[name='password']").val().trim();
        
        var a=0;
        var b=0;
        /* ------------------------------------------------------------------ */
        /* --------------------- email validation --------------------------- */
        /* ------------------------------------------------------------------ */
        if(email.length > 0)
        {  
            if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) ) {
                a=1;  
                $("#Email").removeClass("has-error");
                $("#Email .help-block").html(' ');
            }
            else{
                a=0; 
                $("#Email").addClass("has-error");
                $("#Email .help-block").html('Please enter a valid enail id ');
            }
        }
        else 
        { 
            a=0 
            $("#Email").addClass("has-error");
            $("#Email .help-block").html('This field is required ');
        }
        
        /* ------------------------------------------------------------------ */
        /* --------------------- Password validation ------------------------ */
        /* ------------------------------------------------------------------ */
        
        if(password.length > 0)
        {  
            b=1;
            $("#Password").removeClass("has-error");
            $("#Password .help-block").html(' ');
        }
        else 
        { 
            b=0; 
            $("#Password").addClass("has-error");
            $("#Password .help-block").html('This field is required '); 
        }
        
        /* ------------------------------------------------------------------ */
        /* ----------------- form submitting -------------------------------- */
        /* ------------------------------------------------------------------ */
        
        if(a===1 && b===1)
        {
            $.ajax({
                headers: 
                {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "POST",
                url:base_url+"/admin/post_login",
                dataType: "json",
                async: false, 
                data: new FormData($('#LoginForm')[0]),
                processData: false,
                contentType: false, 
                success: function(response)
                {
                    
                    if(response.status==true)
                    {
                        $("#Email").removeClass("has-error");
                        $("#Email .help-block").html(' ');

                        $("#Password").removeClass("has-error");
                        $("#Password .help-block").html(' ');
                        
                        window.location.href = response.url;
                    
                    }
                    else
                    {
                      $("#login_error").show().html(response.message);  
                    }
                    
          
  
                },
                error: function (request, textStatus, errorThrown) {
                    
                    if(request.responseJSON.errors.email.length)
                    {
                        $("#Email").addClass("has-error");
                        $("#Email .help-block").html(request.responseJSON.errors.email[0]);
                    }
                    
                    if(request.responseJSON.errors.password.length)
                    {
                        $("#Password").addClass("has-error");
                        $("#Password .help-block").html(request.responseJSON.errors.password[0]);
                    }
                    
//                console.log(request.responseText);
//                console.log(textStatus);
//                console.log(errorThrown);
                }
                });
                
                
   
                
        }
        
        /* ------------------------------------------------------------------ */
        /* ------------------------------------------------------------------ */
        /* ------------------------------------------------------------------ */
        
        return false;
        
    });
    
/* ************************************************************************** */  
/* ************************************************************************** */ 
/* ************************************************************************** */ 

    
    
  });
