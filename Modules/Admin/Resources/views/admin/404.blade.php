@extends('admin::admin.master')
@section('title', "404 error")

@section('css')
<link rel="stylesheet" href="{{asset('public/admin/plugins/datatables.net-bs/css/dataTables.bootstrap.min.css ')}}">
@stop

@section('content')

   <!-- Main content -->
   <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        404 Error Page
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{URL('/admin')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">404 error</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="error-page">
        <h2 class="headline text-yellow"> 404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

          <p>
            We could not find the page you were looking for.
            Meanwhile, you may <a href="{{URL('/admin')}}">return to dashboard.</a>
          </p>

           
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->
  </div>
    <!-- /.content -->

@stop
@section('js')
<!-- DataTables -->
<script src="{{asset('public/admin/plugins/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('public/admin/plugins/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('Modules/Admin/Resources/assets/app/controles.js')}}"></script>
@stop
 @push('scripts')
 <style>
     .iconclass{margin-left: 2em;font-size: 20px}
 </style>
 
@endpush