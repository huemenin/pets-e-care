<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Pts | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{asset('public/admin/plugins/bootstrap/dist/css/bootstrap.min.css')}}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('public/admin/plugins/font-awesome/css/font-awesome.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{asset('public/admin/plugins/Ionicons/css/ionicons.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('public/admin/css/adminMain.min.css')}} ">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('public/admin/master_plugins/iCheck/square/blue.css')}} ">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <script>
    var base_url = "{{URL::to('/')}}";
  </script>
  
</head>
<body class="hold-transition login-page">
    <div class="Page_Center">
        <div class="login-box">
            <div class="login-logo">
              <a href="{{URL('/admin')}}"><b>Administrator</b></a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
              <p class="login-box-msg">Sign in to start your session</p>
              <form action="" method="post" id="LoginForm" >
                
                <div class="form-group has-feedback " id="Email">
                  <label>Email</label>
                  <input type="email" class="form-control" placeholder="Email" name="email">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  <span class="help-block"></span>
                   
                </div>
                  
                <div class="form-group has-feedback" id="Password">
                    <label>Password</label>
                    <input type="password" class="form-control" placeholder="Password" name="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    <span class="help-block"></span>
                </div>
                 
                <div class="row">
                    
                    <div class="form-group has-error">
                        <span class="help-block col-md-12" id="login_error" style="margin-top: 0px !important"></span>
                    </div>
                  <!-- /.col -->
                  
                  <div class="col-xs-12">
                      <button type="submit" id="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                  </div>
                  <!-- /.col -->
                </div>
                  
              </form>



          <!--    <a href="#">I forgot my password</a><br> -->
            </div>
            <!-- /.login-box-body -->
        </div>
    </div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="{{asset('public/admin/plugins/jquery/dist/jquery.min.js')}}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{asset('public/admin/plugins/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('public/admin/master_plugins/iCheck/icheck.min.js')}}"></script>
<script src="{{asset('Modules/Admin/Resources/assets/app/login.js')}}"></script>
<script src="{{asset('Modules/Admin/Resources/assets/app/CheckLogin.js')}}"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
