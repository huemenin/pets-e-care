@extends('admin::admin.master')
@section('title', "Edit Admin user")
 
@section('content')

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small style="font-weight: bold;">Edit Admin user's</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{URL('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li  class="active"><a href="{{URL('admin/')}}">Admin</a></li>
          <li  class="active"><a href="javascript:void(0)">Edit Admin user</a></li>
          
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="showinfo"></div>
      <!-- Default box -->
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
                    <form method="post" autocomplete="off" name="admin_update" id="admin_update" action="{{URL('/admin/update')."/".$admin[0]->id}}"  enctype="multipart/form-data">
                    <div class="row"> 
                <!-- full column -->
                        <div class="col-md-12">
                            <!-- Name has-error-->
                            <div class="col-md-6">
                                <div class="form-group " id="Name_Err">
                                    <label>Name<span class="mad">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Name" name="name" id="name" value="{{$admin[0]->name}}" >
                                     
                                </div>
                            </div>
                            <!-- /.Name -->
                            
                            <!--Email-->
                            <div class="col-md-6">
                                <div class="form-group" id="Email_Err">
                                    <label>Email<span class="mad">*</span></label>
                                    <input   type="email" class="form-control" placeholder="Enter Email" name="email" id="email" value="{{$admin[0]->email}}" >
                                    <div id="EmailCheck"></div>
                                 </div>
                            </div>
                            <!-- /.Email -->
                            
                            <!--Password-->
                            <div class="col-md-6">
                                <div class="form-group" id="Password_Err">
                                    <label>Password   </label> 
                                    <input   type="text" class="form-control" placeholder="Please leave as empty for not change password" name="password" id="password">
                                    
                                 </div>
                            </div>
                            <!-- /.Password -->
                            
                            <!--Status -->
                            <div class="col-md-6">
                                <div class="form-group" id="Status_Err"  >
                                    <label>Status</label>
                                    <select class="form-control" style="width: 100%;" name="Status" id="Status">
                                        <option value="1" {{($admin[0]->status==1)?'selected':''}} >Active</option>
                                        <option value="0" {{($admin[0]->status==0)?'selected':''}}>Inactive</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.Status -->
                            
                            <!--Role   --> 
                            <div class="form-group col-md-6 "  id="Role_Err" >
                                <label>Role<span class="mad">*</span></label>
                                <select class="form-control" style="width: 100%;" id="role"  name="role" data-width="100%">
                                    <option value="select">Select </option>
                                    @foreach($roles as $role)
                                            <option value="{{$role->id}}"   @if($role->id == $admin[0]['GetRole']['0']->id ) selected @endif >{{$role->name}}</option>
                                    @endforeach
                                </select>
                                
                            </div>
                            
                            <!--Profile Pic -->
                            <div class="col-md-6">
                                <div class="form-group" id="Status_Err"  >
                                    <label>Profile Pic</label>
                                     <input name="ProfileImage" type="file"  >
                                </div>
  
                            </div>
                            <!-- /.Profile Pic -->
                            
                            
                             
                            
                            <button type="submit"  class="btn btn-info pull-right" style="margin-top: 20px;border:  none;">Submit</button>         
                            
                        </div>
                <!--/.col (full) -->
                   
                    </div> 
               </form>
            </div>
            <!-- /.box-body -->
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@stop

@section('js')
<!-- controls -->
 <script src="{{asset('Modules/Admin/Resources/assets/app/controles.js')}}"></script>
@stop
 