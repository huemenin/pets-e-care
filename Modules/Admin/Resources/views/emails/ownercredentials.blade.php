<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body>
<table width="100%" bgcolor="#eef3f7" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
    <td height="30px">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td width="550px">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" style="background-color: #ffffff; background-image:url({{URL('Modules/Owners/Resources/assets/emails/images/edm-bg.png')}}); background-position:top right; background-repeat:no-repeat; background-size:100%; padding-top: 74px; padding-right: 136px; padding-bottom: 0; padding-left: 74px; border-radius: 10px 10px 0 0;">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                    <span><img src="{{asset('Modules/Owners/Resources/assets/emails/images/logo.png')}}" alt="" width="173px" style="display:block; margin-bottom:51px;"/></span>
                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; display:block; color:#000000; line-height:18px; padding:0; margin-top:0; margin-right:0; margin-bottom: 25px; margin-left:0;">Hi {{$name}}</span>
                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:24px; color:#000000; line-height:38px; padding:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;">Pets E Care Inside welcomes you.</p>
                    <a href="javascript:void(0)" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; display:inline-block; color:#324fa2; line-height:18px; padding-top: 21px; padding-right: 55px; padding-bottom: 21px; padding-left: 55px; margin-top:25px; margin-right:0; margin-bottom: 100px; margin-left:0; text-decoration:none; background:#01dbba; border-radius: 50px;">your password : <b>{{$password}}</b></a>
                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; display:block; color:#a0a7b0; line-height:18px; padding:0; margin-top:0; margin-right:0; margin-bottom: 55px; margin-left:0;">Thanks for joining the team</span>
                    </td>
                  </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td width="100%" bgcolor="#2d465e" style="padding-top:28px; padding-bottom:28px; border-radius: 0 0 10px 10px">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="246px">&nbsp;</td>
                    <td width="58px" style="">
                    	<table width="53px" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="11px">
                            	<a href="#" style="display:block; text-decoration:none; width:11px; height:11px;"><img src="{{asset('Modules/Owners/Resources/assets/emails/images/edm-facebook.png')}}" width="11px" height="11px" /></a>
                            </td>
                            <td width="11px">
                            	<a href="#" style="display:block; text-decoration:none; width:11px; height:11px;"><img src="{{asset('Modules/Owners/Resources/assets/emails/images/edm-twitter.png')}}" width="11px" height="11px" /></a>
                            </td>
                            <td width="11px">
                            	<a href="#" style="display:block; text-decoration:none; width:11px; height:11px;"><img src="{{asset('Modules/Owners/Resources/assets/emails/images/edm-google.png')}}" width="11px" height="11px" /></a>
                            </td>
                          </tr>
                        </table>

                    </td>
                    <td width="246px">&nbsp;</td>
                  </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td align="center" valign="middle" style="padding-top: 23px;">
            	<p style="font-family:Arial, Helvetica, sans-serif; font-size:8px; display:block; color:#a0a7b0; line-height:12px; padding:0; margin-top:0; margin-right:0; margin-bottom: 0; margin-left:0;">You've received this email as confirmation of your pentsecare account.</p>
				<p style="font-family:Arial, Helvetica, sans-serif; font-size:8px; display:block; color:#a0a7b0; line-height:12px; padding:0; margin-top:0; margin-right:0; margin-bottom: 0; margin-left:0;">1550 Bryant Street #800, San Francisco, CA 94103</p>
            </td>
          </tr>
        </table>

    </td>
    <td height="30px">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td height="30px">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>


</body>
</html>
