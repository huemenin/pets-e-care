<?php

namespace Modules\Admin\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Administrator extends Authenticatable
{
    use SoftDeletes;
    protected $table    = "admin_users";
    protected $fillable = ['name','email','password'];
    protected $hidden   = ['password']; 
    protected $dates    = ['deleted_at'];

    public function roles() {
        return $this->belongsToMany("Modules\Roles\Entities\Roles","admin_users_roles","user_id","role_id")->where('admin_roles.status',1)->where('admin_roles.deleted_at',null);
    }
 
   
    
    public function GetRole()
    {
        return $this->belongsToMany('Modules\Roles\Entities\Roles','admin_users_roles','user_id','role_id');
    }
}
