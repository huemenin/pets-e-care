@extends('admin::admin.master')
@section('title', "Admin View Permissions")
 
@section('content')

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small style="font-weight: bold;">View Permissions</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{URL('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li><a href="{{URL('/admin/permissions/')}}">Permissions</a></li>
          <li  class="active"><a href="javascript:void(0)">View Permissions</a></li>
          
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="showinfo"></div>
      <!-- Default box -->
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
                     <div class="row">
                <!-- left column -->
                        <div class="col-md-6">
                            <!--Module -->
                            <div class="form-group" id="Module_Err">
                                <label>Module <span class="mad">*</span></label>
                                 
                                <input type="text" disabled="true" class="form-control" value="{{$permission['0']['modules']->module_name}}">
                               
                            </div>
                            <!-- /.Module -->

                            <!--Status -->
                            <div class="form-group" id="Status_Err"  >
                                <label>Status</label>
                                @if($permission[0]->status==1)
                                    <input type="text" disabled="true" class="form-control" value="Active">
                                @endif
                                  
                                @if($permission[0]->status==0)
                                    <input type="text" disabled="true" class="form-control" value="Inactive">
                                @endif
                            </div>
                            <!-- /.Status -->

                        </div>
                <!--/.col (left) -->
                        
                <!-- Right column -->
                        <div class="col-md-6">
                            <!-- Name has-error-->
                            <div class="form-group">
                                <label>Name</label>
                                <input type="text" disabled="true" class="form-control" value="{{$permission[0]->name}}">
                            </div>
                            <!-- /.Name -->
                            
                            <!--Slug-->
                            <div class="form-group" >
                                <label>Slug</label>
                                <input  disabled="true" type="text" class="form-control"  value={{$permission[0]->slug}}>
                             </div>
                            <!-- /.Slug -->

                        </div>
                <!--/.col (Right) -->
                    </div> 
                    
            </div>
            <!-- /.box-body -->
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@stop

 
 