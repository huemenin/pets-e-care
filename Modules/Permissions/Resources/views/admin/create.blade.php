@extends('admin::admin.master')
@section('title', "Admin Create Permissions")
 
@section('content')

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small style="font-weight: bold;">Create Permissions</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{URL('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li  class="active"><a href="{{URL('/admin/permissions/')}}">Permissions</a></li>
          <li  class="active"><a href="javascript:void(0)">Create Permissions</a></li>
          
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="showinfo"></div>
      <!-- Default box -->
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
                    <form method="post" autocomplete="off" name="permission_create" id="permission_create" action="{{URL('/admin/permissions/create')}}"  enctype="multipart/form-data">
                    <div class="row">
                <!-- left column -->
                        <div class="col-md-6">
                            <!--Module -->
                            <div class="form-group" id="Module_Err">
                                <label>Module <span class="mad">*</span></label>
                                <select class="form-control" style="width: 100%;" name="Module" id="Module">
                                    <option value="select">select</option> 
                                    @if($modules->isEmpty())
                                    @else
                                    @foreach($modules as $module)
                                    <option value="{{$module->id}}" >{{$module->module_name}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <!-- /.Module -->

                            <!--Status -->
                            <div class="form-group" id="Status_Err"  >
                                <label>Status</label>
                                <select class="form-control" style="width: 100%;" name="Status" id="Status">
                                    <option value="1" selected="">Active</option>
                                    <option value="0">Not Activate</option>
                                </select>
                            </div>
                            <!-- /.Status -->

                        </div>
                <!--/.col (left) -->
                        
                <!-- Right column -->
                        <div class="col-md-6">
                            <!-- Name has-error-->
                            <div class="form-group " id="Name_Err">
                                <label>Name<span class="mad">*</span></label>
                                <input type="text" class="form-control" placeholder="Enter Permission Name" name="permission_name" id="permission_name">
                                <div id="NameCheck"></div>
                            </div>
                            <!-- /.Name -->
                            
                            <!--Slug-->
                            <div class="form-group" id="Slug_Err">
                                <label>Slug<span class="mad">*</span></label>
                                <input readonly="true" type="text" class="form-control" placeholder="Permission Slug" name="permission_slug" id="permission_slug">
                                <div id="SlugCheck"></div>
                             </div>
                            <!-- /.Slug -->

                        </div>
                <!--/.col (Right) -->
                    </div> 
                   <button type="submit" id="add_permission" class="btn btn-info pull-right" style="margin-right: 10px;border:  none;">Submit</button>
                </form>
            </div>
            <!-- /.box-body -->
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@stop

@section('js')
<!-- controls -->
 <script src="{{asset('Modules/Permissions/Resources/assets/app/controles.js')}}"></script>
@stop
 