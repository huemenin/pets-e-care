     
$(function() {
   
  
    
   if($('#permissions-table').length){
        $('#permissions-table').DataTable({
        processing: true,
        serverSide: true,  
        ajax: base_url+"/admin/permissions/AdminPermissionsList",
        columns: [ 
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'modules.module_name', name: 'modules.module_name','orderable': false, },
            {
                    data: "null",
                    'searchable': false, 
                    'orderable': false,
                   
                    render: function (data, type, full) 
                    { 
                        
                       var u = '<a href="'+base_url+'/admin/permissions/view/'+full.id+'" class="iconclass"><i class="fa  fa-eye"></i></a>' 
                       +''+'<a href="'+base_url+'/admin/permissions/edit/'+full.id+'" class="iconclass"><i class="fa  fa-pencil"></i></a>'
                          +''+'<a href="'+base_url+'/admin/permissions/delete/'+full.id+'" class="iconclass"  Onclick="return ConfirmDelete();"><i class="fa  fa-trash"></i></a>';

                        return u;
                    }
            }
        ]
       
   });
    }
/* ************************************************************************* */  
/* *************************** data table listing end ********************** */  
/* ************************************************************************* */  
   
    /*
     * create form 
     * params : Module,Name,Status,Slug  
     */
      
    $("#permission_create").submit(function(e)
    {
        e.preventDefault();
        var Module       =   $("[name='Module']").val().trim();
        var Status       =   $("[name='Status']").val().trim();
        var Name         =   $("[name='permission_name']").val().trim();
        var Slug         =   $("[name='permission_slug']").val().trim();
        
        
        var a=0;
        var b=0;
        var c=0;
        var d=0;
        
        //Module
        if(Module=="select"){a=0; $("#Module_Err").addClass( "has-error" );}
        else{a=1; $( "#Module_Err" ).removeClass( "has-error" );}

        //Name
        if(Name.length > 0){b=1; $( "#Name_Err" ).removeClass( "has-error" );}
        else{b=0; $( "#Name_Err" ).addClass( "has-error" );}
        
        //Status
        if(Status=='0' || Status=='1'){c=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{c=0; $( "#Status_Err" ).addClass( "has-error" );}
        
        //Slug
        if(Slug.length > 0)
        {
            var gnsg= generate_slug($("#permission_name").val());
            if(gnsg != Slug) { d=0; $( "#Slug_Err" ).addClass( "has-error" );  }
            else{d=1; $( "#Slug_Err" ).removeClass( "has-error" );}   
        }
        else{b=0; $( "#Slug_Err" ).addClass( "has-error" );}
        
        /* ------------------------------------------------------------------ */
        /* ----------------- form submitting -------------------------------- */
        /* ------------------------------------------------------------------ */
        
        if(a==1 && b==1 && c==1 && d==1)
        {
            
            $("#NameCheck").html(" ");  
            $("#SlugCheck").html(" "); 
            $(".showinfo").html(" ");
            $( "#Name_Err" ).removeClass( "has-error" );
            $( "#Slug_Err" ).removeClass( "has-error" );
            
               $.ajax({
                type: "POST",
                url:base_url+"/admin/permissions/create",
                dataType: "json",
                async: false, 
                data: new FormData($('#permission_create')[0]),
                processData: false,
                contentType: false, 
                success: function(response)
                {     
                     if(response.status==true){window.location.href = response.url; }
                     else{$(".showinfo").html(response.alert);}
                },
                error: function (request, textStatus, errorThrown) {
                    var obj = request.responseJSON.errors ;
                    
                    if(obj.hasOwnProperty("permission_name") )
                    {
                       $( "#Name_Err" ).addClass( "has-error" );
                       $("#NameCheck").html("<div class='mad'>"+request.responseJSON.errors.permission_name[0]+"</div>");   
                    }
                   
                    if(obj.hasOwnProperty("permission_slug") )
                    {
                        $( "#Slug_Err" ).addClass( "has-error" );
                        $("#SlugCheck").html("<div class='mad'>"+request.responseJSON.errors.permission_slug[0]+"</div>");   
                    }
             
//                console.log(request.responseText);
//                console.log(textStatus);
//                console.log(errorThrown);
                }
                });
        }
        
        return false;
        
    });
  
/* ************************************************************************* */  
/* *************************** permission create end *********************** */  
/* ************************************************************************* */  
     
     $("#permission_name").keyup(function() {
        $("#permission_slug").val(generate_slug($("#permission_name").val()));
    });
    

/* ************************************************************************* */  
/* *************************** generate slug end *************************** */  
/* ************************************************************************* */  
 
 
 /*
     * edit form 
     * params : Module,Name,Status,Slug  
     */
      
    $("#permission_update").submit(function(e)
    {   e.preventDefault();
        
        
        var action=   $("#permission_update").attr('action');

        var Module       =   $("[name='Module']").val().trim();
        var Status       =   $("[name='Status']").val().trim();
        var Name         =   $("[name='permission_name']").val().trim();
        var Slug         =   $("[name='permission_slug']").val().trim();
        
        
        var a=0;
        var b=0;
        var c=0;
        var d=0;
        
        //Module
        if(Module=="select"){a=0; $("#Module_Err").addClass( "has-error" );}
        else{a=1; $( "#Module_Err" ).removeClass( "has-error" );}

        //Name
        if(Name.length > 0){b=1; $( "#Name_Err" ).removeClass( "has-error" );}
        else{b=0; $( "#Name_Err" ).addClass( "has-error" );}
        
        //Status
        if(Status=='0' || Status=='1'){c=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{c=0; $( "#Status_Err" ).addClass( "has-error" );}
        
 
        
        /* ------------------------------------------------------------------ */
        /* ----------------- form submitting -------------------------------- */
        /* ------------------------------------------------------------------ */
        
        if(a==1 && b==1 && c==1)
        {
            
            $("#NameCheck").html(" ");  
            $("#SlugCheck").html(" "); 
            $(".showinfo").html(" ");
            
               $.ajax({
                type: "POST",
                url:action,
                dataType: "json",
                async: false, 
                data: new FormData($('#permission_update')[0]),
                processData: false,
                contentType: false, 
                success: function(response)
                {     
                     if(response.status==true){window.location.href = response.url; }
                     else{$(".showinfo").html(response.alert);}
                },
                error: function (request, textStatus, errorThrown) {
                    
                    var obj = request.responseJSON.errors ;
                    
                    if(obj.hasOwnProperty("permission_name") )
                    {
                       $("#NameCheck").html("<div class='mad'>"+request.responseJSON.errors.permission_name[0]+"</div>");   
                    }
                   
                    if(obj.hasOwnProperty("permission_slug") )
                    {
                        $("#SlugCheck").html("<div class='mad'>"+request.responseJSON.errors.permission_slug[0]+"</div>");   
                    }
             
//                console.log(request.responseText);
//                console.log(textStatus);
//                console.log(errorThrown);
                }
                });
        }
        
        return false;
        
        
        
        
    });
/* ************************************************************************* */  
/* ****************************** function end ***************************** */  
/* ************************************************************************* */    
   
   
});
