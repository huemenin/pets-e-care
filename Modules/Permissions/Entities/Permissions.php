<?php

namespace Modules\Permissions\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Permissions extends Model
{
     use SoftDeletes;
    protected $table = "admin_permissions";
    //protected $primaryKey = "id";
    protected $fillable = ['name','slug','module_id','status']; 

    public function modules() {
        return $this->belongsTo("Modules\Module\Entities\Modules","module_id","id");
    }
    
    
    
}
