<?php

namespace Modules\Permissions\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Modules\Permissions\Entities\Permissions;
use Modules\Module\Entities\Modules;
use URL;
use \Illuminate\Support\Facades\Session;

class AdminPermissionsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('permissions::admin.index');
    }

    /**
     * Display a listing of the all permissions and their module.
     * @return Response json array
     */
    public function allPermissions()
    {
        return Datatables::of(
        Permissions::with(array('modules'=>function($query){$query->select('modules.id','modules.module_name');})
        )->where('admin_permissions.deleted_at',NULL)
        ->whereHas('modules', function ($query){$query->where('modules.status', '1');$query->where('modules.deleted_at',null);}))
        ->make(true);
    }
    
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $modules = Modules::where('status',1)->get();
        return view('permissions::admin.create',compact('modules'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['permission_name' => 'required|unique:admin_permissions,name,NULL,id','permission_slug' => 'required|unique:admin_permissions,slug,NULL,id']);

        $permission             =   new Permissions;
        $permission->name       =   $request->permission_name;
        $permission->module_id  =   $request->Module;
        $permission->slug       =   $request->permission_slug; 
        $permission->status     =   $request->Status;
        try
        {
            $permission->save();
            $request->session()->flash('val', 1);
            $request->session()->flash('msg', "Permission created successfully !");
            return response()->json(['status'=>true,'url'=>URL('/admin/permissions/'),'csrf' => csrf_token()]);
        }
        catch (\Exception $e)
        {
            $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
            return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);
   
        }
    }

    /**
     * Show the specified resource.
     * @param  id
     * @return Response
     */
    public function show($id)
    {   
        $permission = Permissions::with('modules')->where('id',$id)->get(); 
        if($permission->isEmpty()){return redirect('/admin/404');}
        else{return view('permissions::admin.view',compact('permission'));   }
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $modules = Modules::where('status',1)->get();
        $permission = Permissions::find($id);
        if($permission==null){return redirect('/admin/404');}
        else{return view('permissions::admin.edit',compact('modules','permission'));  }
        
    }

    /**
     * Update the specified resource in storage.
     * @param  id
     * @param  Request $request
     * @return Response
     */
    public function update($id,Request $request)
    {
        $this->validate($request, ['permission_name' => "required|unique:admin_permissions,name,$id,id", 'permission_slug' => "required|unique:admin_permissions,slug,$id,id"]);
        
        $permission             =   Permissions::find($id);
        if($permission==null){return response()->json(['status'=>true,'url'=>URL('/admin/404'),'csrf' => csrf_token()]);}
        else {
            $permission->name       =   $request->permission_name;
            $permission->module_id  =   $request->Module;
            $permission->slug       =   $request->permission_slug; 
            $permission->status     =   $request->Status;
            try {
                $permission->save();
                $request->session()->flash('val', 1);
                $request->session()->flash('msg', "Permission updated successfully !");
                return response()->json(['status'=>true,'url'=>URL('/admin/permissions/'),'csrf' => csrf_token()]);
            }
            catch (\Exception $e) {
                $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4><i class="icon fa fa-ban"> Alert!</i></h4> Sorry something went wrong!</div>';    
                return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);
            }
        }
        
    }

    /**
     * delete the specified resource from storage.
     * @param id
     * @return Response
     */
    public function destroy($id)
    {
        $permission = Permissions::find($id);
        if($permission==null){return redirect('/admin/404');}
        else
        {
            try
            { 
                $permission->delete();
                Session::flash('val', 1);
                Session::flash('msg', "Permission deleted successfully !");

             } catch (Exception $ex) {
                Session::flash('val', 1);
                Session::flash('msg', $ex->getMessage());
             }
            return redirect('/admin/permissions');
        }
    }
    
    
    
    
}
