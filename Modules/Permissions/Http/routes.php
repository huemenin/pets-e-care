<?php

Route::group(['middleware' => 'web', 'prefix' => 'admin/permissions', 'namespace' => 'Modules\Permissions\Http\Controllers'], function()
{
 
    
    
      /* logged users opertaions */
    Route::group(['middleware' =>  'admin_auth:admin'], function()
    {
        Route::get('/', ['middleware' => 'check_perm:view-permissions', 'uses' => 'AdminPermissionsController@index']);
        Route::get('/AdminPermissionsList',[ 'middleware' => 'check_perm:view-permissions', 'as'=>'AdminPermissionsList.all',  'uses' => 'AdminPermissionsController@allPermissions', ]); 
        Route::get('/add', ['middleware' => 'check_perm:create-permissions', 'uses' => 'AdminPermissionsController@create']);
        Route::get('/view/{id}', ['middleware' => 'check_perm:view-permissions', 'uses' => 'AdminPermissionsController@show']);
        Route::post('/create', ['middleware' => 'check_perm:create-permissions', 'uses' => 'AdminPermissionsController@store']);
        Route::get('/edit/{id}', ['middleware' => 'check_perm:edit-permissions', 'uses' => 'AdminPermissionsController@edit']);
        Route::post('/update/{id}', ['middleware' => 'check_perm:edit-permissions', 'uses' => 'AdminPermissionsController@update']);
        Route::get('/delete/{id}', ['middleware' => 'check_perm:delete-permissions', 'uses' => 'AdminPermissionsController@destroy']); 
        
    });
    
    
    
});
