<?php

namespace Modules\Permissions\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SeedAdminModulesListTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        if(DB::table('modules')->get()->count() == 0){
                    $tasks  =   [
                                    [
                                        'id'=>'1',
                                        'module_name' => 'Modules',
                                        'slug' => 'module',
                                        'icon' => 'fa fa-cog',
                                        're_order'=>'1'
                                    ],
                                    [
                                        'id'=>'2',
                                        'module_name' => 'Permissions',
                                        'slug' => 'permissions',
                                        'icon' => 'fa fa-cog',
                                        're_order'=>'2'
                                    ],
                                    [
                                        'id'=>'3',
                                        'module_name' => 'Roles',
                                        'slug' => 'roles',
                                        'icon' => 'fa fa-lock',
                                        're_order'=>'3'
                                    ],
                                    [
                                        'id'=>'4',
                                        'module_name' => 'Admin',
                                        'slug' => 'admin',
                                        'icon' => 'fa fa-user-secret',
                                        're_order'=>'4'
                                    ],
                                    [
                                        'id'=>'5',
                                        'module_name' => 'Owners',
                                        'slug' => 'owners',
                                        'icon' => 'fa fa-smile-o',
                                        're_order'=>'5'
                                    ],
                                    [
                                        'id'=>'6',
                                        'module_name' => 'Doctors',
                                        'slug' => 'doctors',
                                        'icon' => 'fa  fa-stethoscope',
                                        're_order'=>'6'
                                    ],
                                    [
                                        'id'=>'7',
                                        'module_name' => 'Pets',
                                        'slug' => 'pets',
                                        'icon' => 'fa  fa-smile-o',
                                        're_order'=>'7'
                                    ],
                                    
                                ];
             
            DB::table('modules')->insert($tasks);
         }
        // $this->call("OthersTableSeeder");
    }
}
