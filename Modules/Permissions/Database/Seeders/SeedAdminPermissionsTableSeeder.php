<?php

namespace Modules\Permissions\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SeedAdminPermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        if(DB::table('admin_permissions')->get()->count() == 0){
                   $tasks  =   [
                                    [
                                       'id' =>'1',
                                       'name' => 'View Modules',
                                       'slug' => 'view-modules',
                                       'module_id' => '1',
                                    ],
                                    [
                                       'id' =>'2',
                                       'name' => 'Create Modules',
                                       'slug' => 'create-modules',
                                       'module_id' => '1',
                                    ],
                                    [
                                       'id' =>'3',
                                       'name' => 'Edit Modules',
                                       'slug' => 'edit-modules',
                                       'module_id' => '1',
                                    ],
                                    [
                                       'id' =>'4',
                                       'name' => 'Delete Modules',
                                       'slug' => 'delete-modules',
                                       'module_id' => '1',
                                    ],
                /*modules' End*/       
                                    [
                                       'id' =>'5',
                                       'name' => 'View Permissions',
                                       'slug' => 'view-permissions',
                                       'module_id' => '2',
                                    ],
                                    [
                                       'id' =>'6',
                                       'name' => 'Create Permissions',
                                       'slug' => 'create-permissions',
                                       'module_id' => '2',
                                    ],
                                    [
                                       'id' =>'7',
                                       'name' => 'Edit Permissions',
                                       'slug' => 'edit-permissions',
                                       'module_id' => '2',
                                    ],
                                    [
                                       'id' =>'8',
                                       'name' => 'Delete Permissions',
                                       'slug' => 'delete-permissions',
                                       'module_id' => '2',
                                    ],
                /*permissions' End*/
                                    [
                                       'id' =>'9',
                                       'name' => 'View Roles',
                                       'slug' => 'view-roles',
                                       'module_id' => '3',
                                    ],
                                    [
                                       'id' =>'10',
                                       'name' => 'Create Roles',
                                       'slug' => 'create-roles',
                                       'module_id' => '3',
                                    ],
                                    [
                                       'id' =>'11',
                                       'name' => 'Edit Roles',
                                       'slug' => 'edit-roles',
                                       'module_id' => '3',
                                    ],
                                    [
                                       'id' =>'12',
                                       'name' => 'Delete Roles',
                                       'slug' => 'delete-roles',
                                       'module_id' => '3',
                                    ],
                    /*Roles End*/
                                    [
                                       'id' =>'13',
                                       'name' => 'View Admin',
                                       'slug' => 'view-admin',
                                       'module_id' => '4',
                                    ],
                                    [
                                       'id' =>'14',
                                       'name' => 'Create Admin',
                                       'slug' => 'create-admin',
                                       'module_id' => '4',
                                    ],
                                    [
                                       'id' =>'15',
                                       'name' => 'Edit Admin',
                                       'slug' => 'edit-admin',
                                       'module_id' => '4',
                                    ],
                                    [
                                       'id' =>'16',
                                       'name' => 'Delete Admin',
                                       'slug' => 'delete-admin',
                                       'module_id' => '4',
                                    ],                      
                    /*Admin End*/  
                                    [
                                       'id' =>'17',
                                       'name' => 'View Owners',
                                       'slug' => 'view-owner',
                                       'module_id' => '5',
                                    ],
                                    [
                                       'id' =>'18',
                                       'name' => 'Create Owners',
                                       'slug' => 'create-owner',
                                       'module_id' => '5',
                                    ],
                                    [
                                       'id' =>'19',
                                       'name' => 'Edit Owners',
                                       'slug' => 'edit-owner',
                                       'module_id' => '5',
                                    ],
                                    [
                                       'id' =>'20',
                                       'name' => 'Delete Owners',
                                       'slug' => 'delete-owner',
                                       'module_id' => '5',
                                    ],                      
                    /*Owners End*/  
                                    [
                                       'id' =>'21',
                                       'name' => 'View Doctors',
                                       'slug' => 'view-doctors',
                                       'module_id' => '6',
                                    ],
                                    [
                                       'id' =>'22',
                                       'name' => 'Create Doctors',
                                       'slug' => 'create-doctors',
                                       'module_id' => '6',
                                    ],
                                    [
                                       'id' =>'23',
                                       'name' => 'Edit Doctors',
                                       'slug' => 'edit-doctors',
                                       'module_id' => '6',
                                    ],
                                    [
                                       'id' =>'24',
                                       'name' => 'Delete Doctors',
                                       'slug' => 'delete-doctors',
                                       'module_id' => '6',
                                    ],                      
                    /*Doctors End*/  
                                    [
                                       'id' =>'25',
                                       'name' => 'View Pets',
                                       'slug' => 'view-pets',
                                       'module_id' => '7',
                                    ],
                                    [
                                       'id' =>'26',
                                       'name' => 'Create Pets',
                                       'slug' => 'create-pets',
                                       'module_id' => '7',
                                    ],
                                    [
                                       'id' =>'27',
                                       'name' => 'Edit Pets',
                                       'slug' => 'edit-pets',
                                       'module_id' => '7',
                                    ],
                                    [
                                       'id' =>'28',
                                       'name' => 'Delete Pets',
                                       'slug' => 'delete-pets',
                                       'module_id' => '7',
                                    ],                      
                    /*Pets End*/  
                       
                               ];

                   DB::table('admin_permissions')->insert($tasks);
        }
        // $this->call("OthersTableSeeder");
    }
}
