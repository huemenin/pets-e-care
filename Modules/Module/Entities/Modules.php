<?php

namespace Modules\Module\Entities;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Modules extends Model
{
    use SoftDeletes;
    protected $table = "modules";
    protected $fillable = ['module_name','slug']; 
    protected $dates = ['deleted_at'];
    
    public function permissions()
    {
    	return  $this->hasMany("Modules\Permissions\Entities\Permissions","module_id","id");
    }
    
}
