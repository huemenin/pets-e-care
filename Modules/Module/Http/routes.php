<?php

Route::group(['middleware' => 'web', 'prefix' => 'admin/module', 'namespace' => 'Modules\Module\Http\Controllers'], function()
{
 
    /* logged users opertaions */
    Route::group(['middleware' =>  'admin_auth:admin'], function()
    {
        Route::get('/', ['middleware' => 'check_perm:view-modules', 'uses' => 'AdminModuleController@index']);
        Route::get('/AdminModulesList',[ 'middleware' => 'check_perm:view-modules', 'as'=>'AdminModuleController.allModules',  'uses' => 'AdminModuleController@allModules', ]); 
        Route::get('/add', ['middleware' => 'check_perm:create-modules', 'uses' => 'AdminModuleController@create']);
        Route::get('/view/{id}', ['middleware' => 'check_perm:view-modules', 'uses' => 'AdminModuleController@show']);
        Route::post('/create', ['middleware' => 'check_perm:create-modules', 'uses' => 'AdminModuleController@store']);
        Route::get('/edit/{id}', ['middleware' => 'check_perm:edit-modules', 'uses' => 'AdminModuleController@edit']);
        Route::post('/update/{id}', ['middleware' => 'check_perm:edit-modules', 'uses' => 'AdminModuleController@update']);
        Route::get('/delete/{id}', ['middleware' => 'check_perm:delete-modules', 'uses' => 'AdminModuleController@destroy']); 
        
    });
    
    
    
});
