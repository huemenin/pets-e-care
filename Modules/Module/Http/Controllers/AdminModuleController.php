<?php

namespace Modules\Module\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Module\Entities\Modules;
use Yajra\DataTables\DataTables;
use \Illuminate\Support\Facades\Session;

class AdminModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('module::admin.index');
    }
    
    
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function allModules()
    {
        return Datatables::of(Modules::orderBy('id', 'DESC')->get())->make(true);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('module::admin.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['module_name' => 'required|unique:modules,module_name,NULL,id','module_slug' => 'required|unique:modules,slug,NULL,id']);
        $modules                =   new Modules;
        $modules->module_name   =   $request->module_name; 
        $modules->slug          =   $request->module_slug; 
        $modules->status        =   $request->Status;
        $modules->icon          =   $request->module_icon; 
        
        try
        {
            $modules->save();
            $request->session()->flash('val', 1);
            $request->session()->flash('msg', "Module created successfully !");
            return response()->json(['status'=>true,'url'=>URL('/admin/module/'),'csrf' => csrf_token()]);
        }
        catch (\Exception $e)
        {
            $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
            return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);
   
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id)
    {
        $modules = Modules::find($id); 
       
        if($modules==null){return redirect('/admin/404');}
        else{return view('module::admin.view',compact('modules'));   }
         
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit($id)
    {
        $modules = Modules::find($id);
        if($modules==null){return redirect('/admin/404');}
        else{return view('module::admin.edit',compact('modules'));  }
      
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id,Request $request)
    {
        $this->validate($request, ['module_name' => "required|unique:modules,module_name,$id,id", 'module_slug' => "required|unique:modules,slug,$id,id"]);
        $modules                =   Modules::find($id);
        if($modules==null){return response()->json(['status'=>true,'url'=>URL('/admin/404'),'csrf' => csrf_token()]);}
        else{
            $modules->module_name   =   $request->module_name; 
            $modules->slug          =   $request->module_slug; 
            $modules->status        =   $request->Status;
            $modules->icon          =   $request->module_icon; 
            try
            {
                $modules->save();
                $request->session()->flash('val', 1);
                $request->session()->flash('msg', "Module updated successfully !");
                return response()->json(['status'=>true,'url'=>URL('/admin/module/'),'csrf' => csrf_token()]);
            }
            catch (\Exception $e) 
            {
                $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> <h4><i class="icon fa fa-ban"> Alert!</i></h4> Sorry something went wrong!</div>';    
                return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {   
        $modules = Modules::find($id);
        if($modules==null){return redirect('/admin/404');}
        else
        { 
            try
            { 
                $modules->delete();
                Session::flash('val', 1);
                Session::flash('msg', "Module deleted successfully !");

            } catch (Exception $ex) {
                Session::flash('val', 1);
                Session::flash('msg', $ex->getMessage());
            } 
            return redirect('/admin/module');
        }
        
        
    }
}
