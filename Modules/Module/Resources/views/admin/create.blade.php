@extends('admin::admin.master')
@section('title', "Admin Create Permissions")
 
@section('content')

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small style="font-weight: bold;">Create Modules</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{URL('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li  class="active"><a href="{{URL('/admin/module/')}}">Modules</a></li>
          <li  class="active"><a href="javascript:void(0)">Create Modules</a></li>
          
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="showinfo"></div>
      <!-- Default box -->
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
                    <form method="post" autocomplete="off" name="module_create" id="module_create" action="{{URL('/module/create')}}"  enctype="multipart/form-data">
                    <div class="row"> 
                <!-- full column -->
                        <div class="col-md-12">
                            <!-- Name has-error-->
                            <div class="col-md-6">
                                <div class="form-group " id="Name_Err">
                                    <label>Name<span class="mad">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Module Name" name="module_name" id="module_name">
                                    <div id="NameCheck"></div>
                                </div>
                            </div>
                            <!-- /.Name -->
                            
                            <!--Slug-->
                            <div class="col-md-6">
                                <div class="form-group" id="Slug_Err">
                                    <label>Slug<span class="mad">*</span></label>
                                    <input readonly="true" type="text" class="form-control" placeholder="Module Slug" name="module_slug" id="module_slug">
                                    <div id="SlugCheck"></div>
                                 </div>
                            </div>
                            <!-- /.Slug -->
                            
                            <!--Status -->
                            <div class="col-md-6">
                                <div class="form-group" id="Status_Err"  >
                                    <label>Status</label>
                                    <select class="form-control" style="width: 100%;" name="Status" id="Status">
                                        <option value="1" selected="">Active</option>
                                        <option value="0">Not Activate</option>
                                    </select>
                                </div>
                            </div>
                            <!-- /.Status -->
                            
                             <!--Icon-->
                            <div class="col-md-6">
                                <div class="form-group" id="icon_Err">
                                    <label>Icon<span class="mad">*</span></label>
                                    <input   type="text" class="form-control" placeholder="Module Icon" name="module_icon" id="module_icon">
                                 </div>
                            </div>
                            <!-- /.Icon -->
                                     <button type="submit"  class="btn btn-info pull-right" style="margin-top: 20px;border:  none;">Submit</button>         
                            
                        </div>
                <!--/.col (full) -->
                   
                    </div> 
               </form>
            </div>
            <!-- /.box-body -->
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@stop

@section('js')
<!-- controls -->
 <script src="{{asset('Modules/Module/Resources/assets/app/controles.js')}}"></script>
@stop
 