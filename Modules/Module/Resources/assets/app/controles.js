     
$(function() {
   
  
    
   if($('#modules-table').length){
       
        $('#modules-table').DataTable({
        processing: true,
        serverSide: true,  
        ajax: base_url+"/admin/module/AdminModulesList",
        columns: [ 
            { data: 'id', name: 'id' },
            { data: 'module_name', name: 'module_name' }, 
            {
                    data: "null",
                    'searchable': false, 
                    'orderable': false,
                   
                    render: function (data, type, full) 
                    { 
                        
                       var u = '<a href="'+base_url+'/admin/module/view/'+full.id+'" class="iconclass"><i class="fa  fa-eye"></i></a>' 
                       +''+'<a href="'+base_url+'/admin/module/edit/'+full.id+'" class="iconclass"><i class="fa  fa-pencil"></i></a>'
                          +''+'<a href="'+base_url+'/admin/module/delete/'+full.id+'" class="iconclass"  Onclick="return ConfirmDelete();"><i class="fa  fa-trash"></i></a>';

                        return u;
                    }
            }
        ]
       
   });
    }
/* ************************************************************************* */  
/* *************************** data table listing end ********************** */  
/* ************************************************************************* */  
   
    /*
     * create form 
     * params : Name,Status,Slug  
     */
      
    $("#module_create").submit(function(e)
    {
        e.preventDefault(); 
         
        var Status       =   $("[name='Status']").val().trim();
        var Name         =   $("[name='module_name']").val().trim();
        var Slug         =   $("[name='module_slug']").val().trim();
        var icon         =   $("[name='module_icon']").val().trim(); 
        var a=0;
        var b=0;
        var c=0; 
        var d=0; 
        
        
        //Name
        if(Name.length > 0){a=1; $( "#Name_Err" ).removeClass( "has-error" );}
        else{a=0; $( "#Name_Err" ).addClass( "has-error" );}
        
        //Status
        if(Status=='0' || Status=='1'){b=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{b=0; $( "#Status_Err" ).addClass( "has-error" );}
        
        //Slug
        if(Slug.length > 0)
        {
            var gnsg= generate_slug($("#module_name").val());
            if(gnsg != Slug) { c=0; $( "#Slug_Err" ).addClass( "has-error" );  }
            else{c=1; $( "#Slug_Err" ).removeClass( "has-error" );}   
        }
        else{c=0; $( "#Slug_Err" ).addClass( "has-error" );}
        
        
        //Icon
        if(icon.length > 0){d=1; $( "#icon_Err" ).removeClass( "has-error" );}
        else{d=0; $( "#icon_Err" ).addClass( "has-error" );}
        
        /* ------------------------------------------------------------------ */
        /* ----------------- form submitting -------------------------------- */
        /* ------------------------------------------------------------------ */
        
        if(a==1 && b==1 && c==1 && d==1)
        {
            
            $("#NameCheck").html(" ");  
            $("#SlugCheck").html(" "); 
            $(".showinfo").html(" ");
            $( "#Name_Err" ).removeClass( "has-error" );
            $( "#Slug_Err" ).removeClass( "has-error" );
            
               $.ajax({
                    type: "POST",
                    url:base_url+"/admin/module/create",
                    dataType: "json",
                    async: false, 
                    data: new FormData($('#module_create')[0]),
                    processData: false,
                    contentType: false, 
                    success: function(response)
                    {     
                         if(response.status==true){window.location.href = response.url; }
                         else{$(".showinfo").html(response.alert);}
                    },
                    error: function (request, textStatus, errorThrown) {

                        var obj = request.responseJSON.errors ;

                        if(obj.hasOwnProperty("module_name") )
                        {
                           $( "#Name_Err" ).addClass( "has-error" );
                           $("#NameCheck").html("<div class='mad'>"+request.responseJSON.errors.module_name[0]+"</div>");   
                        }

                        if(obj.hasOwnProperty("module_slug") )
                        {
                            $( "#Slug_Err" ).addClass( "has-error" );
                            $("#SlugCheck").html("<div class='mad'>"+request.responseJSON.errors.module_slug[0]+"</div>");   
                        } 
                    }
                });
        }
        
        return false;
        
    });
  
/* ************************************************************************* */  
/* *************************** module create end *********************** */  
/* ************************************************************************* */  
     
     $("#module_name").keyup(function() {
        $("#module_slug").val(generate_slug($("#module_name").val()));
    });
    

/* ************************************************************************* */  
/* *************************** generate slug end *************************** */  
/* ************************************************************************* */  
 
 
 /*
     * edit form 
     * params : Name,Status,Slug  
     */
      
    $("#module_update").submit(function(e)
    {   e.preventDefault();
         
        var action=   $("#module_update").attr('action');

        var Status       =   $("[name='Status']").val().trim();
        var Name         =   $("[name='module_name']").val().trim();
        var Slug         =   $("[name='module_slug']").val().trim(); 
        var icon         =   $("[name='module_icon']").val().trim(); 
        
        var a=0;
        var b=0;
        var c=0; 
        var d=0;
  
        //Name
        if(Name.length > 0){a=1; $( "#Name_Err" ).removeClass( "has-error" );}
        else{a=0; $( "#Name_Err" ).addClass( "has-error" );}
        
        //Status
        if(Status=='0' || Status=='1'){b=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{b=0; $( "#Status_Err" ).addClass( "has-error" );}
        
        //Name
        if(Slug.length > 0){c=1; $( "#Slug_Err" ).removeClass( "has-error" );}
        else{c=0; $( "#Slug_Err" ).addClass( "has-error" );}
        
        //Icon
        if(icon.length > 0){d=1; $( "#icon_Err" ).removeClass( "has-error" );}
        else{d=0; $( "#icon_Err" ).addClass( "has-error" );}
        
        
        /* ------------------------------------------------------------------ */
        /* ----------------- form submitting -------------------------------- */
        /* ------------------------------------------------------------------ */
        
        if(a==1 && b==1 && c==1  && d==1)
        {
            
            $("#NameCheck").html(" ");  
            $("#SlugCheck").html(" "); 
            $(".showinfo").html(" ");
            $( "#Name_Err" ).removeClass( "has-error" );
            $( "#Slug_Err" ).removeClass( "has-error" );
            
               $.ajax({
                type: "POST",
                url:action,
                dataType: "json",
                async: false, 
                data: new FormData($('#module_update')[0]),
                processData: false,
                contentType: false, 
                success: function(response)
                {     
                     if(response.status==true){window.location.href = response.url; }
                     else{$(".showinfo").html(response.alert);}
                },
                error: function (request, textStatus, errorThrown) {
                    
                        var obj = request.responseJSON.errors ;

                        if(obj.hasOwnProperty("module_name") )
                        {
                           $( "#Name_Err" ).addClass( "has-error" );
                           $("#NameCheck").html("<div class='mad'>"+request.responseJSON.errors.module_name[0]+"</div>");   
                        }

                        if(obj.hasOwnProperty("module_slug") )
                        {
                            $( "#Slug_Err" ).addClass( "has-error" );
                            $("#SlugCheck").html("<div class='mad'>"+request.responseJSON.errors.module_slug[0]+"</div>");   
                        } 
 
                }
                });
        }
        
        return false;
        
        
        
        
    });
/* ************************************************************************* */  
/* ****************************** function end ***************************** */  
/* ************************************************************************* */    
   
   
});
