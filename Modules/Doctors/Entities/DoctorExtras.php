<?php

namespace Modules\Doctors\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorExtras extends Model
{
    protected $table = "doctor_extras";

    public $timestamps = false;
}
