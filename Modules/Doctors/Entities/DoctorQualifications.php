<?php

namespace Modules\Doctors\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DoctorQualifications extends Model
{
    protected $table = "doctor_qualifications";

    public $timestamps = false;
}
