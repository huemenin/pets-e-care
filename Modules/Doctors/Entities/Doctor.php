<?php

namespace Modules\Doctors\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes; 
use Illuminate\Foundation\Auth\User as Authenticatable;

class Doctor extends Authenticatable
{
    use SoftDeletes;
    protected $table = "doctors";
    protected $fillable = ['name','email','password']; 
    protected $hidden = ['password']; 
    protected $dates = ['deleted_at'];

    public function experiences()
    {
    	return $this->hasMany("Modules\Doctors\Entities\DoctorExperience","doctor_id");
    }

    public function ug_qualifications()
    {
    	return $this->hasMany("Modules\Doctors\Entities\DoctorQualifications","doctor_id")->where('type','ug');
    }

    public function pg_qualifications()
    {
    	return $this->hasMany("Modules\Doctors\Entities\DoctorQualifications","doctor_id")->where('type','pg');
    }

    public function expertises()
    {
    	return $this->hasMany("Modules\Doctors\Entities\DoctorExtras","doctor_id")->where('type','expertise');
    }

    public function achievements()
    {
    	return $this->hasMany("Modules\Doctors\Entities\DoctorExtras","doctor_id")->where('type','achievement');
    }

    public function services()
    {
    	return $this->hasMany("Modules\Doctors\Entities\DoctorExtras","doctor_id")->where('type','service');
    }
}
