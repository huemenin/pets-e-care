     
$(function() { 
  
  if($('#reg_validity').length){
    $('#reg_validity').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
    })

}
   if($('#doctors-table').length){
       
        $('#doctors-table').DataTable({
        processing: true,
        serverSide: true,  
        ajax: base_url+"/admin/doctors/AdminDoctorsList",
        columns: [ 
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' }, 
             { data: 'mobile', name: 'mobile' }, 
             { data: 'created_at', name: 'created_at' }, 
            {
                    data: "null",
                    'searchable': false, 
                    'orderable': false,
                   
                    render: function (data, type, full) 
                    { 
                        
                       var u = '<a href="'+base_url+'/doctors/view/'+full.id+'" class="iconclass"><i class="fa  fa-eye"></i></a>' 
                       +''+'<a href="'+base_url+'/admin/doctors/edit/'+full.id+'" class="iconclass"><i class="fa  fa-pencil"></i></a>'
                          +''+'<a href="'+base_url+'/doctors/delete/'+full.id+'" class="iconclass"  Onclick="return ConfirmDelete();"><i class="fa  fa-trash"></i></a>';

                        return u;
                    }
            }
        ]
       
   });
    }


    
/* ************************************************************************* */  
/* *************************** data table listing end ********************** */  
/* ************************************************************************* */  
   
    /*
     * create form   
     */
      
    $("#doctor_create").submit(function(e)
    {
        
        e.preventDefault(); 
        
          
        var Name         =   $("[name='name']").val().trim();
        var email        =   $("[name='email']").val().trim();
        var mobile       =   $("[name='mobile']").val().trim();  
        var Status       =   $("[name='Status']").val().trim();
        
        var a=0;
        var b=0;
        var c=0; 
        var d=0; 
        
        
        //Name
        if(Name.length > 0)
        {
            a=1; 
            $( "#Name_Err" ).removeClass( "has-error" );
            $("#Name_Err .help-block").html(' ');
        }
        else{
            a=0; 
            $( "#Name_Err" ).addClass( "has-error" );
            $("#Name_Err .help-block").html('This field is required');
        }
        
        //email
        if(email.length > 0)
        {  
            if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) )
            {
                b=1;  
                $("#Email_Err").removeClass("has-error");
                $("#Email_Err .help-block").html(' ');
            }
            else{
                b=0; 
                $("#Email_Err").addClass("has-error");
                $("#Email_Err .help-block").html('Please enter a valid enail id ');
            }
        }
        else 
        { 
            b=0 
            $("#Email_Err").addClass("has-error");
            $("#Email_Err .help-block").html('This field is required');
        }
        
        
        
         //Name
        if(mobile.length > 0)
        {
            
            if( /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(mobile) )
            {
                c=1; 
                $("#Mobile_Err").removeClass( "has-error" );
                $("#Mobile_Err .help-block").html(' ');  
            }else
            {
                c=0 
                $("#Mobile_Err").addClass("has-error");
                $("#Mobile_Err .help-block").html('Please enter a valid mobile no.');
            } 
        }
        else
        {
            c=1; 
            $( "#Mobile_Err" ).removeClass( "has-error" );
            $("#Mobile_Err .help-block").html(' ');  
        }
        
        
        
        //Status
        if(Status=='0' || Status=='1'){d=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{d=0; $( "#Status_Err" ).addClass( "has-error" );}
        
        
        /* ------------------------------------------------------------------ */
        /* ----------------- form submitting -------------------------------- */
        /* ------------------------------------------------------------------ */
        
        if(a==1 && b==1 && c==1 && d==1)
        {    
               $.ajax({
                    type: "POST",
                    url:base_url+"/doctors/create",
                    dataType: "json",
                    async: false, 
                    data: new FormData($('#doctor_create')[0]),
                    processData: false,
                    contentType: false, 
                    success: function(response)
                    {     
                         if(response.status==true){window.location.href = response.url; }
                         else{$(".showinfo").html(response.alert);}
                    },
                    error: function (request, textStatus, errorThrown) {

                        var obj = request.responseJSON.errors ;

                        if(obj.hasOwnProperty("email") )
                        {
                            $("#Email_Err").addClass("has-error"); 
                            $("#Email_Err .help-block").html("<div class='mad'>"+request.responseJSON.errors.email[0]+"</div>");   
                        }
                        
                        if(obj.hasOwnProperty("mobile") )
                        {
                            $("#Mobile_Err").addClass("has-error"); 
                            $("#Mobile_Err .help-block").html("<div class='mad'>"+request.responseJSON.errors.mobile[0]+"</div>");   
                        }

                         
                    }
                });
        }
        
        return false;
        
    });
  
/* ************************************************************************* */  
/* *************************** owner create end *********************** */  
/* ************************************************************************* */  
 
 
 /*
     * edit form   
     */
       $("#doctor_update").submit(function(e)
    {
        
        e.preventDefault(); 
        
        var action        =   $("#doctor_update").attr('action'); 
        var Name         =   $("[name='name']").val().trim();
        var email        =   $("[name='email']").val().trim();
        var mobile       =   $("[name='mobile']").val().trim();  
        var Status       =   $("[name='Status']").val().trim();
        
        var a=0;
        var b=0;
        var c=0; 
        var d=0; 
        
        
        //Name
        if(Name.length > 0)
        {
            a=1; 
            $( "#Name_Err" ).removeClass( "has-error" );
            $("#Name_Err .help-block").html(' ');
        }
        else{
            a=0; 
            $( "#Name_Err" ).addClass( "has-error" );
            $("#Name_Err .help-block").html('This field is required');
        }
        
        //email
        if(email.length > 0)
        {  
            if( /(.+)@(.+){2,}\.(.+){2,}/.test(email) )
            {
                b=1;  
                $("#Email_Err").removeClass("has-error");
                $("#Email_Err .help-block").html(' ');
            }
            else{
                b=0; 
                $("#Email_Err").addClass("has-error");
                $("#Email_Err .help-block").html('Please enter a valid enail id ');
            }
        }
        else 
        { 
            b=0 
            $("#Email_Err").addClass("has-error");
            $("#Email_Err .help-block").html('This field is required');
        }
        
        
        
         //Name
        if(mobile.length > 0)
        {
            
            if( /^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/.test(mobile) )
            {
                c=1; 
                $("#Mobile_Err").removeClass( "has-error" );
                $("#Mobile_Err .help-block").html(' ');  
            }else
            {
                c=0 
                $("#Mobile_Err").addClass("has-error");
                $("#Mobile_Err .help-block").html('Please enter a valid mobile no.');
            } 
        }
        else
        {
            c=1; 
            $( "#Mobile_Err" ).removeClass( "has-error" );
            $("#Mobile_Err .help-block").html(' ');  
        }
        
        
        
        //Status
        if(Status=='0' || Status=='1'){d=1; $( "#Status_Err" ).removeClass( "has-error" );}
        else{d=0; $( "#Status_Err" ).addClass( "has-error" );}
        
        
        /* ------------------------------------------------------------------ */
        /* ----------------- form submitting -------------------------------- */
        /* ------------------------------------------------------------------ */
        
        if(a==1 && b==1 && c==1 && d==1)
        {    
               $.ajax({
                    type: "POST",
                    url:action,
                    dataType: "json",
                    async: false, 
                    data: new FormData($('#doctor_update')[0]),
                    processData: false,
                    contentType: false, 
                    success: function(response)
                    {     
                         if(response.status==true){window.location.href = response.url; }
                         else{$(".showinfo").html(response.alert);}
                    },
                    error: function (request, textStatus, errorThrown) {

                        var obj = request.responseJSON.errors ;

                        if(obj.hasOwnProperty("email") )
                        {
                            $("#Email_Err").addClass("has-error"); 
                            $("#Email_Err .help-block").html("<div class='mad'>"+request.responseJSON.errors.email[0]+"</div>");   
                        }
                        
                        if(obj.hasOwnProperty("mobile") )
                        {
                            $("#Mobile_Err").addClass("has-error"); 
                            $("#Mobile_Err .help-block").html("<div class='mad'>"+request.responseJSON.errors.mobile[0]+"</div>");   
                        }

                         
                    }
                });
        }
        
        return false;
        
    });
     
/* ************************************************************************* */  
/* ****************************** function end ***************************** */  
/* ************************************************************************* */    
   
   
});


$( document ).ready(function() {
        $("#add_qualification").click(function(e){
            e.preventDefault();
            $(".qualification_row:last").clone().appendTo('#qualification_container');
            $(".qualification_row:last #add_qualification").remove();
            $(".qualification_row:last .trash-qualification").show();
        });
        $('body').on('click','.trash-qualification',function(e){
            e.preventDefault();
            $(this).parents('.qualification_row').first().remove();
        });
        $("#add_pg_qualification").click(function(e){
            e.preventDefault();
            $(".pg_qualification_row:last").clone().appendTo('#pg_qualification_container');
            $(".pg_qualification_row:last #add_pg_qualification").remove();
            $(".pg_qualification_row:last .trash-pg_qualification").show();
        });
        $('body').on('click','.trash-pg_qualification',function(e){
            e.preventDefault();
            $(this).parents('.pg_qualification_row').first().remove();
        });
        $("#add_experience").click(function(e){
            e.preventDefault();
            $(".experience_row:last").clone().appendTo('#experience_container');
            $(".experience_row:last #add_experience").remove();
            $(".experience_row:last .exp_to option[value='present']").remove();
            $(".experience_row:last .trash-experience").show();
        });
        $('body').on('click','.trash-experience',function(e){
            e.preventDefault();
            $(this).parents('.experience_row').first().remove();
        });
        $("#add_expertise").click(function(e){
            e.preventDefault();
            $(".expertise_row:last").clone().appendTo('#expertise_container');
            $(".expertise_row:last #add_expertise").remove();
            $(".expertise_row:last .trash-expertise").show();
        });
        $('body').on('click','.trash-expertise',function(e){
            e.preventDefault();
            $(this).parents('.expertise_row').first().remove();
        });
        $("#add_publish").click(function(e){
            e.preventDefault();
            $(".publish_row:last").clone().appendTo('#publish_container');
            $(".publish_row:last #add_publish").remove();
            $(".publish_row:last .trash-publish").show();
        });
        $('body').on('click','.trash-publish',function(e){
            e.preventDefault();
            $(this).parents('.publish_row').first().remove();
        });
        $("#add_achievement").click(function(e){
            e.preventDefault();
            $(".achievement_row:last").clone().appendTo('#achievement_container');
            $(".achievement_row:last #add_achievement").remove();
            $(".achievement_row:last .trash-achievement").show();
        });
        $('body').on('click','.trash-achievement',function(e){
            e.preventDefault();
            $(this).parents('.achievement_row').first().remove();
        });
        $("#add_service").click(function(e){
            e.preventDefault();
            $(".service_row:last").clone().appendTo('#service_container');
            $(".service_row:last #add_service").remove();
            $(".service_row:last .trash-service").show();
        });
        $('body').on('click','.trash-service',function(e){
            e.preventDefault();
            $(this).parents('.service_row').first().remove();
        });
    });
