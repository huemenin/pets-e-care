 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Doctors</title>
</head>

<body>
<table width="100%" bgcolor="#eef3f7" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>&nbsp;</td>
    <td height="30px">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td width="550px">
    	<table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="100%" style="background-color: #ffffff; background-image:url(https://lh3.googleusercontent.com/-pYbcL4KiNryg5_QNu4M_EJG51YwKb70XULalJO6-Ahjq2X_DlzVeK9RxcD4BHcnf9mVyO0Pa-FFlE4V88HJ5hcvg8qdgKQksza80-EJhiNQttG-KuC9_rNabWi1JqR-22qtDxqSrKDGrVcrMJlgBiHtU5KYOQ5CHRCxKBS77uDf-nwhodSqy25eJSP6q_Jdg9fcXMvR5sm9qqfQCkE5c31U8sABWu40J-y164badYdt2t_j_rKN65kitgPrsDrOgFwKAS33rPSM7lL8OyprjieXABTwR7gDaiRJwF5LBNrasHtW1oBCJ7penX7Wyb0PiV8-0G1WbRuwX2bTRUpPjFFLlTaXILnv2dca6pPAnk8LjgvTumo9Gh6Qnllh3GtYSrMrQxPMWPNwBswkugpVBwPMvOEIXctXAjIeMNj17DL5C9WODUyrpFmJyOfFGqJPz6hvcWEZRyKmVA3Mec0QF7cV2lxniE3AXsnVhrSX3v3v7-dMgqhCb4lJISrYGvjNxS537nltqPteejd0GcY5NijrDYhapqadjCv-mkQv57tHKG8l_VB9daHbagFp-_Fq7cA_rkNbxJMK-Ol6Gcn6Qa7DD-wzrx9f9TuuLg=w550-h222-no); background-position:top right; background-repeat:no-repeat; background-size:100%; padding-top: 74px; padding-right: 136px; padding-bottom: 0; padding-left: 74px; border-radius: 10px 10px 0 0;">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td>
                    <span><img src="https://lh3.googleusercontent.com/TZMjlJrZ_OlQupGrNxdZBJ-V2Yra3FZX2dQrUnweaRVI43Z_4nRXXrpSPd5f7FDwjyMa7ZLojfpwG80tEHGqIMt9gXoNZMTiuAhV9D-Qw08oiOsgtD0qWAZt098Wmg1rhfF1dOdUweg6fEJv-N6l2dftDLhw7CiyCwmxaJq0E9FVEXxaPReBLgj8dJ8TtQhaTi3d9CWwSJZ7XZYd77uwMljzl1LRILeolnzGKiLOBtEAFaiLt_PItHuwii1--t9eay49PP_oAw51Ivdw1LSXLOJblHXVtuy592jo5MrpnehOUFVFLDfina8akraj9f4gjWD960JgOoeMXuy9jHsBrWBpYdfH6E_5Svja8Q9nQRXkaJVO4kMycQHDo6lHsylQRbFVx9YPd46ooU4r4HkDVJpVUdjeN3b0rGeK7Z3Ok_Nc47y0fHUX5u2zt41N5rybgDsJBiPfwlAQsdbV2M4BC-jKFzpqjAStVtygYpgDO0chCjFGk3raUDEGd_jT83TTywbQsVn90crBEHehFLLGLAsd3KdlvdN_BNrgH7rr4VniHiVkzFLLtZkSBZaoFLYSoROfgasKTKD0cAa22kAQZYr_PmnZNvhUwRkHJg=w301-h79-no" alt="" width="173px" style="display:block; margin-bottom:51px;"/></span>
                    <p style="font-family:Arial, Helvetica, sans-serif; font-size:24px; color:#000000; line-height:38px; padding:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;"> Hi {{$name}},<br/>Click on the below link to verify your account.</p>
                    <a href="{{URL('doctors/verification').'/'.$key}}" style="font-family:Arial, Helvetica, sans-serif; font-size:15px; display:inline-block; color:#324fa2; line-height:18px; padding-top: 16px; padding-right: 35px; padding-bottom: 16px; padding-left: 35px; margin-top:25px; margin-right:0; margin-bottom: 40px; margin-left:0; text-decoration:none; background:#01dbba; border-radius: 50px;">Click to Verify</a>
 <br/>
                    <span style="font-family:Arial, Helvetica, sans-serif; font-size:14px; display:block; color:#a0a7b0; line-height:18px; padding:0; margin-top:0; margin-right:0; margin-bottom: 55px; margin-left:0;">Thanks for joining the team</span>
                    </td>
                  </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td width="100%" bgcolor="#2d465e" style="padding-top:28px; padding-bottom:28px; border-radius: 0 0 10px 10px">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td width="246px">&nbsp;</td>
                    <td width="58px" style="">
                    	<table width="53px" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="11px">
                            	<a href="#" style="display:block; text-decoration:none; width:11px; height:11px;"><img src="https://lh3.googleusercontent.com/KLFu3iRhU2vcJJiXO2-Wbd6wgFJPoAiS8Cavloz5f5cZFusDlFsBHrUcIYWtZMop4y3X6TW2ZRP0QkP58f5Y9-_Wo2Y-AIlukz3O0JQa9af_6-GP1b0g-eQNwR-FUlHhgXBGTZqxO-cfHh5oND6gsfVVRvxXAzkGxh0-JjXF50aPuiuMD4uChFXYspS4pvvkfebOZ5w9BaRyKnkq7jEJZtBoiN8MnSoMGcWo03IDg8vINo_Mbcsu3XdMpT6MkK6xDugtt0PHnX8v43lhXdtSta2AThFwmya1UkS4bKL9fZElBX4r5O6F_OpExoCChPvnqD1ed8o3fZB2G5IKN63KDO61SVSW9tSjpk7HsD05dcS7Z_eki6BoFhgsU0RtfjiQu1oedsduef7-Qpc58e3U98-MzxgtxI5l0VkHsP9UZPB05du1Yqnans7xDEY_hScRLlwB-Flus9rtwk-GYd0CX8ftZLRu68BKI5KqRSIBG7AexiMDC45EQBXiB3Rs24p0Dz7l2a_r7SmUmgaqQrFnevy3JMMLCXfW8z6WtMFMTH6XTdmquwLBoroZS1t1LK1B06N_bo0cXBfC-AKYYhw-53SG1V66shYsu34LOw=s11-no" width="11px" height="11px" /></a>
                            </td>
                            <td width="11px">
                            	<a href="#" style="display:block; text-decoration:none; width:11px; height:11px;"><img src="https://lh3.googleusercontent.com/Y53ue_2-uervxEaOYSxtrxmbrPsQkIgXgu8mTpjicm4hoPqIRW8A26dnYEy7IenuPQi18in565zIC5QwPZqnyYDaqBQwe6XWb5uinx6c0ysqsweLRvex2hVuO-vSN3uHk0Ges1l2kPH65jin8ws8cK-zwYy5YJbRzond5UIMrivJ6vLt0_ZhoVxgxQ2x5T3CvXup5quhaCwcs0vzAocOSHYp0wryrXkoQNC3jGDZPPzS8wthU-5ghNir7-MFY-JJnLR7m2Yg-f04lXkWgTxcBxLNgkEQWgXZcXeybnIvj1s_qFYjIgrfTFzGp9Q44pK63lO54HkQw3ahuueC_zDcro7HDQmAVBOYVHtu4Gq1vLuZ5xvhdJ6dE9Bi_ANv5uqNOnEYk-i6Omj-2qzVfRRYyZg8PdSWFBRkkpnKMMr6_G1Jaer0yhIDXhPWqSzuny2TE8HfRuaYVdGBMO5Ef8W_ske0LA1C6CI1l0uxjCSdkQMmqet53HEYx389wKWJGNMHsyEKc1vGEXV450tXeBsHNdtVSiy0MJJIGGmNSixNpk0RdeM0q5dLS7EYTGRVyj49s1mpASBItddgaT7PTrl_Gg43-Ci5BWmuDCftUQ=s11-no" width="11px" height="11px" /></a>
                            </td>
                            <td width="11px">
                            	<a href="#" style="display:block; text-decoration:none; width:11px; height:11px;"><img src="https://lh3.googleusercontent.com/k-Y1cKVSDUW1jaRs_HAcPkkaR3Ep74nTF3xo8Qv6Z6aVSXDXZoDJHyRn0xIdgwnC6aPQyaj7BE-29gFuHBOzWCYdCH4JCregQ6rEKRufWaPMP43SuI3d2mYZQJ_k36CIL5V8AcGAQMzKS_Dc8ZrbotzZ8mhrE_cEsWA3Wp1PzTe8oOB6tIpoq5cKyx8PPTd5sBp3TGQ5-158OZvaxNjOBuURTrNzz3nY3QGj_OCYsO6q_oWktSsL3WAnX4N5MexH455tpSaq64ZbglqWP31Nsb5Q3wC-vy_bbcKefwdfxdxhu-FkX8hKMKQ5JZjupv7RULIMURfozYeCkQj7vSJ3SfsjfVPVpn4pbJsdURiZalOdYd-pKQ6f2ZKj4A-0vUzm3K9IoWGZyBaEa304XWtFlAEmGsaFwcLE5MdJgltwRK2bcC0ewFgVrLAB20Rq2dI2ofO3i82AJIHEsBCW7Ke1bXOyv8e_BHPt9XDoc5G2EsPeupQeVDhmL_OfhhdPPkQNP1CcVRJxJXRJFtNTSSPnG850JMpHSgNy2Q9l37Ms1Ku8xXZvkIONNiEoqxnofB3KFaiGnSQhgxFo9_5N6AYturrEg-rl9k1OaMT_IQ=s11-no" width="11px" height="11px" /></a>
                            </td>
                          </tr>
                        </table>

                    </td>
                    <td width="246px">&nbsp;</td>
                  </tr>
                </table>
            </td>
          </tr>
          <tr>
            <td align="center" valign="middle" style="padding-top: 23px;">
            	<p style="font-family:Arial, Helvetica, sans-serif; font-size:8px; display:block; color:#a0a7b0; line-height:12px; padding:0; margin-top:0; margin-right:0; margin-bottom: 0; margin-left:0;">You've received this email as confirmation of your pentsecare account.</p>
				<p style="font-family:Arial, Helvetica, sans-serif; font-size:8px; display:block; color:#a0a7b0; line-height:12px; padding:0; margin-top:0; margin-right:0; margin-bottom: 0; margin-left:0;">1550 Bryant Street #800, San Francisco, CA 94103</p>
            </td>
          </tr>
        </table>

    </td>
    <td height="30px">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td height="30px">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
</table>


</body>
</html>

