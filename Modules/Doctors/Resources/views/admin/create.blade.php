@extends('admin::admin.master')
@section('title', "Create Doctor")
 
@section('content')

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small style="font-weight: bold;">Create Doctor</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{URL('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li  class="active"><a href="{{URL('/owners/')}}">Doctors</a></li>
          <li  class="active"><a href="javascript:void(0)">Create Doctor</a></li>
          
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="showinfo"></div>
      <!-- Default box -->
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
                    <form method="post" autocomplete="off" name="doctor_create" id="doctor_create" action="{{URL('/admin/create')}}"  enctype="multipart/form-data">
                    <div class="row"> 
                <!-- full column -->
                        <div class="col-md-12">
                            <!-- Name has-error-->
                            <div class="col-md-6">
                                <div class="form-group " id="Name_Err">
                                    <label>Name<span class="mad">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Name" name="name" id="name">
                                     <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Name -->

                            <!-- Category-->
                            <div class="col-md-6">
                                <div class="form-group " id="Category_Err">
                                    <label>Category<span class="mad">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Category" name="category" id="category">
                                     <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Category -->

                            <!-- Experience-->
                            <div class="col-md-6">
                                <div class="form-group " id="Name_Err">
                                    <label>Experience<span class="mad">*</span></label>
                                    <input type="number"  min="1" class="form-control" placeholder="Enter Experience" name="experience" id="experience">
                                     <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Experience -->
                            
                            <!--Email-->
                            <div class="col-md-6">
                                <div class="form-group" id="Email_Err">
                                    <label>Email<span class="mad">*</span></label>
                                    <input   type="email" class="form-control" placeholder="Enter Email" name="email" id="email">
                                    <span class="help-block"></span>
                                 </div>
                            </div>
                            <!-- /.Email -->
                            
                            <!--Mobile-->
                            <div class="col-md-6">
                                <div class="form-group" id="Mobile_Err">
                                    <label>Mobile</label>
                                    <input   type="text" class="form-control" placeholder="Enter Mobile" name="mobile" id="mobile">   
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Mobile -->

                            <!--Location-->
                            <div class="col-md-6">
                                <div class="form-group" id="Location_Err">
                                    <label>Location</label>
                                    <input   type="text" class="form-control" placeholder="Enter Location" name="location" id="location">   
                                    <input   type="hidden" name="latitude" id="latitude">   
                                    <input   type="hidden" name="longitude" id="longitude">   
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Location --> 

                            <!--Council Registration No-->
                            <div class="col-md-6">
                                <div class="form-group" id="Regno_Err">
                                    <label>Council Registration No</label>
                                    <input   type="text" class="form-control" placeholder="Enter Council Registration No" name="council_reg_no" id="council_reg_no">   
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Council Registration No --> 

                             <!--Registration Validity-->
                            <div class="col-md-6">
                                <div class="form-group" id="Regval_Err">
                                    <label>Registration Validity</label>
                                    <input   type="text" class="form-control" placeholder="Enter Registration Validity" name="reg_validity" id="reg_validity">   
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Registration Validity --> 

                            <!--Status -->
                            <div class="col-md-6">
                                <div class="form-group" id="Regstate_Err"  >
                                    <label>Registered State </label>
                                    <input   type="text" class="form-control" placeholder="Enter Registered State" name="reg_state" id="reg_state">   
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Status -->
                            
                            <!--Status -->
                            <div class="col-md-6">
                                <div class="form-group" id="Status_Err"  >
                                    <label>Status</label>
                                    <select class="form-control" style="width: 500%;" name="Status" id="Status">
                                        <option value="1" selected="">Active</option>
                                        <option value="0">Not Activate</option>
                                    </select>
                                    <span class="help-block"><br/></span>
                                </div>
                            </div>
                            <!-- /.Status --> 
                           
                            <!--About me-->
                            <div class="col-md-6">
                                <div class="form-group"  >
                                    <label>Residential Address</label>
                                    <textarea cols="3" name="residential_address" rows="5"  class="form-control" placeholder="Enter Residential Address" style="resize: none"></textarea>
                                 </div>
                            </div>
                            <!-- /.About me -->
                            
                            <!--Address-->
                            <div class="col-md-6">
                                <div class="form-group"  >
                                    <label>Clinical Address</label>
                                    <textarea cols="3" rows="5" name="clinical_address" class="form-control" placeholder="Enter Clinical Address" style="resize: none"></textarea>
                                 </div>
                            </div>
                            <!-- /.Address -->
                             
                            <!--Profile Pic -->
                            <div class="col-md-6">
                                <div class="form-group" id="Status_Err"  >
                                    <label>Profile Pic</label>
                                     <input name="ProfileImage" type="file"  >
                                </div>
                            </div>
                            <!-- /.Profile Pic -->

                                     
                            
                        </div>
                <!--/.col (full) -->
                    
                    </div>
                    <h4 class="box-title">Service Hours</h4>
                    <div class="row">
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Monday </label>
                            <input type="checkbox" value="mon" name="day[]" >
                            <label style="padding-left:50px;"> From</label>
                            <input type="time" name="from_time[mon]" />
                            <label style="padding-left:50px;">To</label>
                            <input type="time" name="to_time[mon]" />
                        </div>
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Tuesday </label>
                            <input type="checkbox" value="tue" name="day[]">
                            <label style="padding-left:50px;"> From</label>
                            <input type="time" name="from_time[tue]"  />
                            <label style="padding-left:50px;">To</label>
                            <input type="time" name="to_time[tue]"  />
                        </div>
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Wednesday </label>
                            <input type="checkbox" value="wed" name="day[]">
                            <label style="    padding-left: 28px;"> From</label>
                            <input type="time" name="from_time[wed]"  />
                            <label style="    padding-left: 50px;">To</label>
                            <input type="time" name="to_time[wed]"  />
                        </div>
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Thursday </label>
                            <input type="checkbox" value="thu" name="day[]">
                            <label style="padding-left:50px;"> From</label>
                            <input type="time" name="from_time[thu]"  />
                            <label style="padding-left:50px;">To</label>
                            <input type="time" name="to_time[thu]"  />
                        </div>
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Friday </label>
                            <input type="checkbox" value="fri" name="day[]">
                            <label style="padding-left:50px;"> From</label>
                            <input type="time" name="from_time[fri]"  />
                            <label style="padding-left:50px;">To</label>
                            <input type="time" name="to_time[fri]"  />
                        </div>
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Saturday </label>
                            <input type="checkbox" value="sat" name="day[]">
                            <label style="padding-left:50px;"> From</label>
                            <input type="time" name="from_time[sat]"  />
                            <label style="padding-left:50px;">To</label>
                            <input type="time" name="to_time[sat]"  />
                        </div>
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Sunday </label>
                            <input type="checkbox" value="sun" name="day[]">
                            <label style="padding-left:50px;"> From</label>
                            <input type="time" name="from_time[sun]"  />
                            <label style="padding-left:50px;">To</label>
                            <input type="time" name="to_time[sun]"  />
                        </div>
                    </div>
                    <h4 class="box-title">Educational Qualification</h4>
                    <div id="qualification_container">
                        <div class = "row qualification_row" >
                            <div class="col-md-3">
                                <label>Qualification </label>
                                    <input type="text" class="form-control" placeholder="Enter Qualification" name="qualification[]" >
                            </div>
                            <div class="col-md-4">
                                <label>Hosptital / College</label>
                                    <input type="text" class="form-control" placeholder="Enter Hosptital / College" name="qualified_hospital[]" >
                            </div>
                            <div class="col-md-2">
                                <label>Passed Year</label>
                                <select class="form-control" name="passed_year[]">
                                @for($i=date('Y');$i>1970;$i--)
                                    <option value="{{$i}}" >{{$i}}</option>
                                @endfor
                            </select>   
                            </div>
                            <div class="col-md-2" style="padding-top: 10px;">
                                <a href="" id="add_qualification" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                <a href="" class="btn bg-purple btn-flat margin trash-qualification" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a> 
                            </div>

                        </div>
                    </div>
                    <h4 class="box-title">Post Graduation (Any)</h4>
                    <div id="pg_qualification_container">
                        <div class = "row pg_qualification_row">
                            <div class="col-md-3">
                                <label>Qualification </label>
                                    <input type="text" class="form-control" placeholder="Enter Qualification" name="pg_qualification[]" >
                            </div>
                            <div class="col-md-4">
                                <label>Hosptital / College</label>
                                    <input type="text" class="form-control" placeholder="Enter Hospital" name="pg_hospital[]" >
                            </div>
                            
                            <div class="col-md-2">
                                <label>Passed Year</label>
                                <select class="form-control" name="pg_passed_year[]">
                                    @for($i=date('Y');$i>1970;$i--)
                                        <option value="{{$i}}" >{{$i}}</option>
                                    @endfor
                                </select>   
                            </div>
                            <div class="col-md-2" style="padding-top: 10px;">
                                <a href="" id="add_pg_qualification" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                <a href="" class="btn bg-purple btn-flat margin trash-pg_qualification" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a> 
                            </div>
                        </div>
                    </div>
                    <h4 class="box-title">Experience</h4>
                    <div id="experience_container">
                        <div class = "row experience_row">
                            <div class="col-md-1">
                                <label>From</label>
                                <select class="form-control" name="exp_from[]">
                                    @for($i=date('Y');$i>1970;$i--)
                                        <option value="{{$i}}" >{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-1">
                                <label>To</label>
                                <select class="form-control exp_to" name="exp_to[]">
                                    <option value="present">Present</option>
                                    @for($i=date('Y');$i>1970;$i--)
                                        <option value="{{$i}}" >{{$i}}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="col-md-3">
                                <label>Designation </label>
                                    <input type="text" class="form-control" placeholder="Enter Designation" name="experience_designation[]" >
                            </div>
                            <div class="col-md-3">
                                <label>Hosptital </label>
                                    <input type="text" class="form-control" placeholder="Enter Hospital" name="experience_hospital[]" >
                            </div>
                            <div class="col-md-2" style="padding-top: 10px;">
                                <a href="" type="button" id="add_experience" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                <a href="" type="button" class="btn bg-purple btn-flat margin trash-experience" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a> 
                            </div>
                        </div>
                    </div>
                    <h4 class="box-title">Area of Expertise / Specialisation</h4>
                    <div id="expertise_container">
                        <div class = "row expertise_row">
                            <div class="col-md-5">
                                <label>Expertise </label>
                                    <input type="text" class="form-control" placeholder="Enter Area of Expertise" name="expertise[]" >
                            </div>
                            <div class="col-md-2" style="padding-top: 10px;">
                                <a href="" id="add_expertise" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                <a href="" class="btn bg-purple btn-flat margin trash-expertise" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a> 
                            </div>
                        </div>
                    </div>
                    <h4 class="box-title">Papers Published</h4>
                    <div id="publish_container">
                        <div class = "row publish_row">
                            <div class="col-md-5">
                                <label>Paper Title </label>
                                    <input type="text" class="form-control" placeholder="Enter Paper Titile" name="publish_title[]" >
                            </div>
                            <div class="col-md-3">
                                <label>File </label>
                                    <input type="file" class="form-control" name="publish_file[]" >
                            </div>
                            <div class="col-md-2" style="padding-top: 10px;">
                                <a href="" id="add_publish"  class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                <a href="" class="btn bg-purple btn-flat margin trash-publish" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a> 
                            </div>
                        </div>
                    </div>
                    <h4 class="box-title">Achievements</h4>
                    <div id="achievement_container">
                        <div class = "row achievement_row">
                            <div class="col-md-5">
                                <label>Achievements </label>
                                    <input type="text" class="form-control" placeholder="Enter Achievement" name="achievement[]" >
                            </div>
                            <div class="col-md-2" style="padding-top: 10px;">
                                <a href="" id="add_achievement"  class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                <a href="" class="btn bg-purple btn-flat margin trash-achievement" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a>
                            </div>
                        </div>
                    </div>
                    <h4 class="box-title">Services</h4>
                    <div id="service_container">
                        <div class = "row service_row">
                            <div class="col-md-5">
                                <label>Service </label>
                                    <input type="text" class="form-control" placeholder="Enter Service" name="service[]" >
                            </div>
                            <div class="col-md-2" style="padding-top: 10px;">
                                <a href="" id="add_service" type="button" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                <a href="" class="btn bg-purple btn-flat margin trash-service" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a>
                            </div>
                        </div>
                    </div>
                    <button type="submit"  class="btn btn-info pull-right" style="margin-top: 20px;border:  none;">Submit</button>
               </form>
            </div>
            <!-- /.box-body -->
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@stop

@section('js')
<!-- controls -->
<script src="{{asset('public/admin/plugins/moment/min/moment.min.js')}}"></script>
<script src="{{asset('public/admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
 <script src="{{asset('Modules/Doctors/Resources/assets/app/controles.js')}}"></script>
 <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyDxSzbAmSaj6z6kgBSa134rcBIprKccHGo"></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('location'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                document.getElementById("latitude").value = latitude;
                document.getElementById("longitude").value = longitude;
            });
        });
    </script>
@stop
 