@extends('admin::admin.master')
@section('title', "Update doctor")
 
@section('content')

  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small style="font-weight: bold;">Update doctor</small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{URL('/admin/dashboard')}}"><i class="fa fa-dashboard"></i> Home</a></li>
          <li  class="active"><a href="{{URL('/doctors/')}}">doctors</a></li>
          <li  class="active"><a href="javascript:void(0)">Update doctor</a></li>
          
        </ol>
    </section>
    <!-- Main content -->
    <section class="content">

        <div class="showinfo"></div>
      <!-- Default box -->
        <div class="box box-success">
            <!-- /.box-header -->
            <div class="box-body">
                <form method="post" autocomplete="off" name="doctor_edit" id="doctor_update" action="{{URL('/').'/admin/doctors/update/'.$doctors->id}}"   enctype="multipart/form-data">
                    <div class="row"> 
                <!-- full column -->
                        <div class="col-md-12">
                            <!-- Name has-error-->
                            <div class="col-md-6">
                                <div class="form-group " id="Name_Err">
                                    <label>Name<span class="mad">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Name" name="name" id="name" value="{{$doctors->name}}">
                                     <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Name -->

                            <!-- Category-->
                            <div class="col-md-6">
                                <div class="form-group " id="Category_Err">
                                    <label>Category<span class="mad">*</span></label>
                                    <input type="text" class="form-control" placeholder="Enter Category" name="category" id="category" value="{{$doctors->category }}">
                                     <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Category -->

                            <!-- Experience-->
                            <div class="col-md-6">
                                <div class="form-group " id="Name_Err">
                                    <label>Experience<span class="mad">*</span></label>
                                    <input type="number"  min="1" class="form-control" placeholder="Enter Experience" name="experience" id="experience" value="{{$doctors->experience }}">
                                     <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Experience -->
                            
                            <!--Email-->
                            <div class="col-md-6">
                                <div class="form-group" id="Email_Err">
                                    <label>Email<span class="mad">*</span></label>
                                    <input   type="email" class="form-control" placeholder="Enter Email" name="email" id="email" value="{{$doctors->email }}">
                                    <span class="help-block"></span>
                                 </div>
                            </div>
                            <!-- /.Email -->
                            
                            <!--Mobile-->
                            <div class="col-md-6">
                                <div class="form-group" id="Mobile_Err">
                                    <label>Mobile</label>
                                    <input   type="text" class="form-control" placeholder="Enter Mobile" name="mobile" id="mobile" value="{{$doctors->mobile }}">   
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Mobile -->

                            <!--Location-->
                            <div class="col-md-6">
                                <div class="form-group" id="Location_Err">
                                    <label>Location</label>
                                    <input   type="text" class="form-control" placeholder="Enter Location" name="location" id="location" value="{{$doctors->location }}">   
                                    <input   type="hidden" name="latitude" id="latitude" value="{{$doctors->latitude }}">   
                                    <input   type="hidden" name="longitude" id="longitude" value="{{$doctors->longitude }}">   
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Location --> 

                            <!--Council Registration No-->
                            <div class="col-md-6">
                                <div class="form-group" id="Regno_Err">
                                    <label>Council Registration No</label>
                                    <input   type="text" class="form-control" placeholder="Enter Council Registration No" name="council_reg_no" id="council_reg_no" value="{{$doctors->council_reg_no }}">   
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Council Registration No --> 

                             <!--Registration Validity-->
                            <div class="col-md-6">
                                <div class="form-group" id="Regval_Err">
                                    <label>Registration Validity</label>
                                    <input   type="text" class="form-control" placeholder="Enter Registration Validity" name="reg_validity" id="reg_validity" value="{{ $doctors->reg_validity!='' ? date('d-m-Y',strtotime($doctors->reg_validity)) : ''}}">   
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Registration Validity --> 

                            <!--Status -->
                            <div class="col-md-6">
                                <div class="form-group" id="Regstate_Err"  >
                                    <label>Registered State </label>
                                    <input   type="text" class="form-control" placeholder="Enter Registered State" name="reg_state" id="reg_state" value="{{$doctors->reg_state }}">   
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <!-- /.Status -->
                            
                            <!--Status -->
                            <div class="col-md-6">
                                <div class="form-group" id="Status_Err"  >
                                    <label>Status</label>
                                    <select class="form-control" style="width: 500%;" name="Status" id="Status">
                                        <option @if($doctors->status==1) selected @endif value="1" selected="">Active</option>
                                        <option @if($doctors->status==0) selected @endif value="0">Not Activate</option>
                                        <option @if($doctors->status==2) selected @endif value="2">Not Complete</option>
                                        <option @if($doctors->status==3) selected @endif value="3">Complete</option>
                                    </select>
                                    <span class="help-block"><br/></span>
                                </div>
                            </div>
                            <!-- /.Status --> 
                           
                            <!--About me-->
                            <div class="col-md-6">
                                <div class="form-group"  >
                                    <label>Residential Address</label>
                                    <textarea cols="3" name="residential_address" rows="5"  class="form-control" placeholder="Enter Residential Address" style="resize: none">{{$doctors->resident_address }}</textarea>
                                 </div>
                            </div>
                            <!-- /.About me -->
                            
                            <!--Address-->
                            <div class="col-md-6">
                                <div class="form-group"  >
                                    <label>Clinical Address</label>
                                    <textarea cols="3" rows="5" name="clinical_address" class="form-control" placeholder="Enter Clinical Address" style="resize: none">{{$doctors->clinical_address }}</textarea>
                                 </div>
                            </div>
                            <!-- /.Address -->
                             
                            <!--Profile Pic -->
                            <div class="col-md-6">
                                <div class="form-group" id="Status_Err"  >
                                    <label>Profile Pic</label>
                                     <input name="ProfileImage" type="file"  >
                                </div>
                            </div>
                            <!-- /.Profile Pic -->

                                     
                            
                        </div>
                <!--/.col (full) -->
                    
                    </div>
                    @php
                        $days = array();
                        $froms = array();
                        $tos = array();
                        if($doctors->service_time!=""){ 
                            $service_times = json_decode($doctors->service_time);
                            foreach($service_times as $service_time) {
                                $days[] = $service_time->day;
                                $froms[$service_time->day] = $service_time->from;
                                $tos[$service_time->day] = $service_time->to;
                            }
                        }
                    @endphp
                    <h4 class="box-title">Service Hours</h4>
                    <div class="row">
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Monday </label>
                            <input type="checkbox" value="mon" name="day[]" @if(in_array('mon',$days)) checked @endif >
                            <label style="padding-left:50px;"> From</label>
                            <input type="time" name="from_time[mon]" @if(in_array('mon',$days)) value="{{$froms['mon']}}" @endif />
                                                                                    <label style="padding-left:50px;">To</label>
                            <input type="time" name="to_time[mon]" @if(in_array('mon',$days)) value="{{$tos['mon']}}" @endif />
                        </div>
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Tuesday </label>
                            <input type="checkbox" value="tue" name="day[]" @if(in_array('tue',$days)) checked @endif >
                            <label style="padding-left:50px;"> From</label>
                            <input type="time" name="from_time[tue]" @if(in_array('tue',$days)) value="{{$froms['tue']}}" @endif  />
                            <label style="padding-left:50px;">To</label>
                            <input type="time" name="to_time[tue]" @if(in_array('tue',$days)) value="{{$tos['tue']}}" @endif />
                        </div>
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Wednesday </label>
                            <input type="checkbox" value="wed" name="day[]" @if(in_array('wed',$days)) checked @endif >
                            <label style="    padding-left: 28px;"> From</label>
                            <input type="time" name="from_time[wed]"  @if(in_array('wed',$days)) value="{{$froms['wed']}}" @endif  />
                            <label style="    padding-left: 50px;">To</label>
                            <input type="time" name="to_time[wed]"  @if(in_array('wed',$days)) value="{{$tos['wed']}}" @endif  />
                        </div>
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Thursday </label>
                            <input type="checkbox" value="thu" name="day[]" @if(in_array('thu',$days)) checked @endif >
                            <label style="padding-left:50px;"> From</label>
                            <input type="time" name="from_time[thu]"  @if(in_array('thu',$days)) value="{{$froms['thu']}}" @endif />
                            <label style="padding-left:50px;">To</label>
                            <input type="time" name="to_time[thu]"  @if(in_array('thu',$days)) value="{{$tos['thu']}}" @endif />
                        </div>
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Friday </label>
                            <input type="checkbox" value="fri" name="day[]" @if(in_array('fri',$days)) checked @endif >
                            <label style="padding-left:50px;"> From</label>
                            <input type="time" name="from_time[fri]" @if(in_array('fri',$days)) value="{{$froms['fri']}}" @endif />
                            <label style="padding-left:50px;">To</label>
                            <input type="time" name="to_time[fri]" @if(in_array('fri',$days)) value="{{$tos['fri']}}" @endif />
                        </div>
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Saturday </label>
                            <input type="checkbox" value="sat" name="day[]" @if(in_array('sat',$days)) checked @endif >
                            <label style="padding-left:50px;"> From</label>
                            <input type="time" name="from_time[sat]" @if(in_array('sat',$days)) value="{{$froms['sat']}}" @endif />
                            <label style="padding-left:50px;">To</label>
                            <input type="time" name="to_time[sat]" @if(in_array('sat',$days)) value="{{$tos['sat']}}" @endif />
                        </div>
                        <div class="col-md-6" style="margin-top: 10px;">
                            <label>Sunday </label>
                            <input type="checkbox" value="sun" name="day[]" @if(in_array('sun',$days)) checked @endif >
                            <label style="padding-left:50px;"> From</label>
                            <input type="time" name="from_time[sun]" @if(in_array('sun',$days)) value="{{$froms['sun']}}" @endif />
                            <label style="padding-left:50px;">To</label>
                            <input type="time" name="to_time[sun]" @if(in_array('sun',$days)) value="{{$tos['sun']}}" @endif />
                        </div>
                    </div>
                    <h4 class="box-title">Educational Qualification</h4>
                    <div id="qualification_container">
                        @forelse ($doctors->ug_qualifications as $qualification)
                            <div class = "row qualification_row" >
                                <div class="col-md-3">
                                    <label>Qualification </label>
                                        <input type="text" class="form-control" placeholder="Enter Qualification" name="qualification[]" value="{{$qualification->qualification}}"  >
                                </div>
                                <div class="col-md-4">
                                    <label>Hosptital / College</label>
                                        <input type="text" class="form-control" placeholder="Enter Hosptital / College" name="qualified_hospital[]" value="{{$qualification->college}}" >
                                </div>
                                <div class="col-md-2">
                                    <label>Passed Year</label>
                                    <select class="form-control" name="passed_year[]">
                                    @for($i=date('Y');$i>1970;$i--)
                                        <option  @if($i==$qualification->year) selected @endif value="{{$i}}" >{{$i}}</option>
                                    @endfor
                                </select>   
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                     @if ($loop->first)
                                        <a href="" id="add_qualification" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                    @else
                                        <a href="" class="btn bg-purple btn-flat margin trash-qualification"><span class="glyphicon glyphicon-trash"></span></a> 
                                    @endif
                                </div>

                            </div>
                        @empty
                            <div class = "row qualification_row" >
                                <div class="col-md-3">
                                    <label>Qualification </label>
                                        <input type="text" class="form-control" placeholder="Enter Qualification" name="qualification[]" >
                                </div>
                                <div class="col-md-4">
                                    <label>Hosptital / College</label>
                                        <input type="text" class="form-control" placeholder="Enter Hosptital / College" name="qualified_hospital[]" >
                                </div>
                                <div class="col-md-2">
                                    <label>Passed Year</label>
                                    <select class="form-control" name="passed_year[]">
                                    @for($i=date('Y');$i>1970;$i--)
                                        <option  @if($i==$qualification->year) selected @endif value="{{$i}}" >{{$i}}</option>
                                    @endfor
                                </select>   
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <a href="" id="add_qualification" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                    <a href="" class="btn bg-purple btn-flat margin trash-qualification" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a> 
                                </div>

                            </div>
                        @endforelse
                        
                    </div>
                    <h4 class="box-title">Post Graduation (Any)</h4>
                    <div id="pg_qualification_container">
                        @forelse ($doctors->pg_qualifications as $qualification)
                            <div class = "row pg_qualification_row">
                                <div class="col-md-3">
                                    <label>Qualification </label>
                                        <input type="text" class="form-control" placeholder="Enter Qualification" name="pg_qualification[]" value="{{$qualification->qualification}}" >
                                </div>
                                <div class="col-md-4">
                                    <label>Hosptital / College</label>
                                        <input type="text" class="form-control" placeholder="Enter Hospital" name="pg_hospital[]" value="{{$qualification->college}}" >
                                </div>
                                
                                <div class="col-md-2">
                                    <label>Passed Year</label>
                                    <select class="form-control" name="pg_passed_year[]">
                                        @for($i=date('Y');$i>1970;$i--)
                                            <option value="{{$i}}" >{{$i}}</option>
                                        @endfor
                                    </select>   
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    @if ($loop->first)
                                        <a href="" id="add_pg_qualification" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                    @else
                                        <a href="" class="btn bg-purple btn-flat margin trash-pg_qualification" ><span class="glyphicon glyphicon-trash"></span></a>
                                    @endif 
                                </div>
                            </div>
                        @empty
                            <div class = "row pg_qualification_row">
                                <div class="col-md-3">
                                    <label>Qualification </label>
                                        <input type="text" class="form-control" placeholder="Enter Qualification" name="pg_qualification[]" >
                                </div>
                                <div class="col-md-4">
                                    <label>Hosptital / College</label>
                                        <input type="text" class="form-control" placeholder="Enter Hospital" name="pg_hospital[]" >
                                </div>
                                
                                <div class="col-md-2">
                                    <label>Passed Year</label>
                                    <select class="form-control" name="pg_passed_year[]">
                                        @for($i=date('Y');$i>1970;$i--)
                                            <option value="{{$i}}" >{{$i}}</option>
                                        @endfor
                                    </select>   
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <a href="" id="add_pg_qualification" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                    <a href="" class="btn bg-purple btn-flat margin trash-pg_qualification" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a> 
                                </div>
                            </div>
                        @endforelse
                    </div>
                    <h4 class="box-title">Experience</h4>
                    <div id="experience_container">
                        @forelse ($doctors->experiences as $experience)
                            <div class = "row experience_row">
                                <div class="col-md-1">
                                    <label>From</label>
                                    <select class="form-control" name="exp_from[]">
                                        @for($i=date('Y');$i>1970;$i--)
                                            <option @if($i == $experience->from_year) selected @endif value="{{$i}}" >{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <label>To</label>
                                    <select class="form-control exp_to" name="exp_to[]">
                                        <option @if('present' == $experience->from_year) selected @endif value="present">Present</option>
                                        @for($i=date('Y');$i>1970;$i--)
                                            <option @if($i == $experience->from_year) selected @endif value="{{$i}}" >{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Designation </label>
                                        <input type="text" class="form-control" placeholder="Enter Designation" name="experience_designation[]" value="{{$experience->designation}}" >
                                </div>
                                <div class="col-md-3">
                                    <label>Hosptital </label>
                                        <input type="text" class="form-control" placeholder="Enter Hospital" name="experience_hospital[]" value="{{$experience->hospital}}" >
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    @if ($loop->first)
                                        <a href="" type="button" id="add_experience" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                    @else
                                        <a href="" type="button" class="btn bg-purple btn-flat margin trash-experience" ><span class="glyphicon glyphicon-trash"></span></a>
                                    @endif
                                </div>
                            </div>
                        @empty
                            <div class = "row experience_row">
                                <div class="col-md-1">
                                    <label>From</label>
                                    <select class="form-control" name="exp_from[]">
                                        @for($i=date('Y');$i>1970;$i--)
                                            <option value="{{$i}}" >{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <label>To</label>
                                    <select class="form-control exp_to" name="exp_to[]">
                                        <option value="present">Present</option>
                                        @for($i=date('Y');$i>1970;$i--)
                                            <option value="{{$i}}" >{{$i}}</option>
                                        @endfor
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Designation </label>
                                        <input type="text" class="form-control" placeholder="Enter Designation" name="experience_designation[]" >
                                </div>
                                <div class="col-md-3">
                                    <label>Hosptital </label>
                                        <input type="text" class="form-control" placeholder="Enter Hospital" name="experience_hospital[]" >
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <a href="" type="button" id="add_experience" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                    <a href="" type="button" class="btn bg-purple btn-flat margin trash-experience" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a> 
                                </div>
                            </div>
                        @endforelse
                    </div>
                    <h4 class="box-title">Area of Expertise / Specialisation</h4>
                    <div id="expertise_container">
                        @forelse ($doctors->expertises as $expertise)
                            <div class = "row expertise_row">
                                <div class="col-md-5">
                                    <label>Expertise </label>
                                        <input type="text" class="form-control" placeholder="Enter Area of Expertise" name="expertise[]" value="{{$expertise->value}}">
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    @if ($loop->first)
                                        <a href="" id="add_expertise" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                    @else
                                        <a href="" class="btn bg-purple btn-flat margin trash-expertise" ><span class="glyphicon glyphicon-trash"></span></a> 
                                    @endif
                                </div>
                            </div>
                        @empty
                            <div class = "row expertise_row">
                                <div class="col-md-5">
                                    <label>Expertise </label>
                                        <input type="text" class="form-control" placeholder="Enter Area of Expertise" name="expertise[]" >
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <a href="" id="add_expertise" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                    <a href="" class="btn bg-purple btn-flat margin trash-expertise" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a> 
                                </div>
                            </div>
                        @endforelse
                    </div>
                    <h4 class="box-title">Papers Published</h4>
                    <div id="publish_container">
                        <div class = "row publish_row">
                            <div class="col-md-5">
                                <label>Paper Title </label>
                                    <input type="text" class="form-control" placeholder="Enter Paper Titile" name="publish_title[]" >
                            </div>
                            <div class="col-md-3">
                                <label>File </label>
                                    <input type="file" class="form-control" name="publish_file[]" >
                            </div>
                            <div class="col-md-2" style="padding-top: 10px;">
                                <a href="" id="add_publish"  class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                <a href="" class="btn bg-purple btn-flat margin trash-publish" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a> 
                            </div>
                        </div>
                    </div>
                    <h4 class="box-title">Achievements</h4>
                    <div id="achievement_container">
                        @forelse ($doctors->achievements as $achievement)
                            <div class = "row achievement_row">
                                <div class="col-md-5">
                                    <label>Achievements </label>
                                        <input type="text" class="form-control" placeholder="Enter Achievement" name="achievement[]"  value="{{$achievement->value}}">
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    @if ($loop->first)
                                        <a href="" id="add_achievement"  class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                    @else
                                        <a href="" class="btn bg-purple btn-flat margin trash-achievement" ><span class="glyphicon glyphicon-trash"></span></a>
                                    @endif
                                </div>
                            </div>
                        @empty
                            <div class = "row achievement_row">
                                <div class="col-md-5">
                                    <label>Achievements </label>
                                        <input type="text" class="form-control" placeholder="Enter Achievement" name="achievement[]" >
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <a href="" id="add_achievement"  class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                    <a href="" class="btn bg-purple btn-flat margin trash-achievement" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a>
                                </div>
                            </div>
                        @endforelse
                    </div>
                    <h4 class="box-title">Services</h4>
                    <div id="service_container">
                        @forelse ($doctors->services as $service)
                            <div class = "row service_row">
                                <div class="col-md-5">
                                    <label>Service </label>
                                        <input type="text" class="form-control" placeholder="Enter Service" name="service[]" value="{{$service->value}}" >
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    @if ($loop->first)
                                        <a href="" id="add_service" type="button" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                    @else
                                        <a href="" class="btn bg-purple btn-flat margin trash-service" ><span class="glyphicon glyphicon-trash"></span></a>
                                    @endif
                                </div>
                            </div>
                        @empty
                            <div class = "row service_row">
                                <div class="col-md-5">
                                    <label>Service </label>
                                        <input type="text" class="form-control" placeholder="Enter Service" name="service[]" >
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <a href="" id="add_service" type="button" class="btn bg-purple btn-flat margin"><span class="glyphicon glyphicon-plus"></span></a> 
                                    <a href="" class="btn bg-purple btn-flat margin trash-service" style="display: none;"><span class="glyphicon glyphicon-trash"></span></a>
                                </div>
                            </div>
                        @endforelse
                    </div>
                    <button type="submit"  class="btn btn-info pull-right" style="margin-top: 20px;border:  none;">Submit</button>
               </form>
            </div>
            <!-- /.box-body -->
        </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
  <!-- /.content-wrapper -->
@stop

@section('js')
<!-- controls -->
<script src="{{asset('public/admin/plugins/moment/min/moment.min.js')}}"></script>
<script src="{{asset('public/admin/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
 <script src="{{asset('Modules/Doctors/Resources/assets/app/controles.js')}}"></script>
 <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyDxSzbAmSaj6z6kgBSa134rcBIprKccHGo"></script>
    <script type="text/javascript">
        google.maps.event.addDomListener(window, 'load', function () {
            var places = new google.maps.places.Autocomplete(document.getElementById('location'));
            google.maps.event.addListener(places, 'place_changed', function () {
                var place = places.getPlace();
                var address = place.formatted_address;
                var latitude = place.geometry.location.lat();
                var longitude = place.geometry.location.lng();
                document.getElementById("latitude").value = latitude;
                document.getElementById("longitude").value = longitude;
            });
        });
    </script>
@stop