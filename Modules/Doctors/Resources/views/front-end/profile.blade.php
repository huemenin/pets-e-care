@extends('website::layouts.master')
@section('title', "pet e care- Owner")
@section('content')
 


  <section class="banner inner">
        <div class="banner-contant">
            <h3>Doctor Profile</h3>
        </div>
        <div class="banner-image">
            <div class="banner-slide-inner">
                <img src="{{asset('public/web/images/banner-inner.jpg')}}" alt="">
            </div>
        </div>
        <div class="banner-gradient"></div>
    </section>

   <section class="inner">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-sm-12 left-box">
                    <div class="profile-box">
                        <div class="row">
                            <div class="col-lg-3 col-sm-4 profile-pic">
                                <div class="img-box">
                                    @if(Auth::guard('doctor')->user()->image)
                                        @if(file_exists( public_path()."/doctors/img/".Auth::guard('doctor')->user()->image ))
                                            <img src="{{asset('public/doctors/img/'.Auth::guard('owner')->user()->image)}}" alt="">
                                        @else
                                            <img src="{{asset('public/doctors/default-profile.png')}}" alt="">
                                        @endif
                                    @else
                                        <img src="{{asset('public/doctors/default-profile.png')}}" alt="">
                                    @endif
                                    
                                </div>
                            </div>
                            <div class="col-lg-9 col-sm-12 profile-details">
                                <div class="row">
                                    <div class="col-lg-7 col-sm-12">
                                        <h2>{{Auth::guard('doctor')->user()->name}}</h2>
                                    </div>
                                    <div class="col-lg-5 col-sm-12">
                                        <h5>Unique ID: PECD{{Auth::guard('doctor')->user()->id}}</h5>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-7 col-sm-12">
                                        <span class="desig">Veterinarian</span>
                                        <span class="age">26 Years Experience</span>
                                        <div class="contact-det">
                                            <span><i class="fas fa-phone"></i> (+91) 9821234 123</span>
                                            <span><i class="fas fa-map-marker"></i> Kottayam</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-8 col-sm-12">
                                        <h4>Council Registration Number:</h4>
                                        <span>Council Registration Number:</span>
                                    </div>
                                    <div class="col-lg-4 col-sm-12">
                                        <h4>Valid Up To:</h4>
                                        <span>Jan - 22 - 2018</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 mt-4">
                                        <h4>State in which registered:</h4>
                                        <span>Kerala</span>
                                    </div>
                                    
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12 mt-4">
                                        <h4>Residential address:</h4>
                                        <p>Near Ganesh Temple, Seethampeta, Landmark: Opp Kaleswari Travels., Visakhapatnam Pin ; 865467</p>
                                    </div>
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="profile-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 profile-details">
                                <div class="row">
                                    <div class="col-lg-8 col-sm-12">
                                        <h4>Clinic Address</h4>
                                        <p>Near Ganesh Temple, Seethampeta, <br> Landmark: Opp Kaleswari Travels., <br> Visakhapatnam Pin ; 865467</p>
                                    </div>
                                    <div class="col-lg-4 col-sm-12">
                                        <h4>Mon - Sat</h4>
                                        <p>10:00 AM - 7:00 PM</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 profile-details">
                                <div class="row">
                                    <div class="col-lg-12 service-window">
                                        <h4>Service Window</h4>
                                        <ul class="d-flex">
                                            <li class="one"><h4>Time</h4></li>
                                            <li>
                                                <h4>Mon</h4>
                                                <span>8:00am</span>
                                                <span>3:00pm</span>
                                            </li>
                                            <li>
                                                <h4>Mon</h4>
                                                <span>8:00am</span>
                                                <span>3:00pm</span>
                                            </li>
                                            <li>
                                                <h4>Mon</h4>
                                                <span>8:00am</span>
                                                <span>3:00pm</span>
                                            </li>
                                            <li>
                                                <h4>Mon</h4>
                                                <span>8:00am</span>
                                                <span>3:00pm</span>
                                            </li>
                                            <li>
                                                <h4>Mon</h4>
                                                <span>8:00am</span>
                                                <span>3:00pm</span>
                                            </li>
                                            <li>
                                                <h4>Mon</h4>
                                                <span>8:00am</span>
                                                <span>3:00pm</span>
                                            </li>
                                            <li class="disabled">
                                                <h4>Mon</h4>
                                                <span>8:00am</span>
                                                <span>3:00pm</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 profile-details">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <h4>Educational Qualification:</h4>
                                        <span>MBBS - Andhra Medical College, Visakhapatnam, 1988</span>
                                        <span>DGO - Andhra Medical College, Visakhapatnam, 1992</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 profile-details">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <h4>Post Graduation If Any:</h4>
                                        <span>- -</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 profile-details">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <h4>Years Of Experience:</h4>
                                        <span>2001 - 2013 Assistant Professor of Surgery at KGH</span>
                                        <span>2013 - Present Associate Professor of Surgery at KGH</span>
                                        <span>1993 - 2015 Consultant at Sagar Durga Enclave</span>
                                        <span>2000 - 2015 Consultant at Seethammadara</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 profile-details">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <h4>Area Of Interest/ Specialisation:</h4>
                                        <span>2001 - 2013 Assistant Professor of Surgery at KGH</span>
                                        <span>2013 - Present Associate Professor of Surgery at KGH</span>
                                        <span>1993 - 2015 Consultant at Sagar Durga Enclave</span>
                                        <span>2000 - 2015 Consultant at Seethammadara</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 profile-details">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <h4>Area Of Interest/ Specialisation:</h4>
                                        <span>2001 - 2013 Assistant Professor of Surgery at KGH</span>
                                        <span>2013 - Present Associate Professor of Surgery at KGH</span>
                                        <span>1993 - 2015 Consultant at Sagar Durga Enclave</span>
                                        <span>2000 - 2015 Consultant at Seethammadara</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 profile-details">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <h4>Summary Of Papers Published:</h4>
                                        <ul class="d-flex">
                                            <li>
                                                <a href="" class="file-icon">
                                                    <img src="images/file-icon.jpg" alt="">
                                                    <span>Indian Medi...</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="" class="file-icon">
                                                    <img src="images/file-icon.jpg" alt="">
                                                    <span>Indian Medi...</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="" class="file-icon">
                                                    <img src="images/file-icon.jpg" alt="">
                                                    <span>Indian Medi...</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="" class="file-icon">
                                                    <img src="images/file-icon.jpg" alt="">
                                                    <span>Indian Medi...</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="" class="file-icon">
                                                    <img src="images/file-icon.jpg" alt="">
                                                    <span>Indian Medi...</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 profile-details">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <h4>Achivements</h4>
                                        <span>Best Doctor Award - 2014</span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="profile-box">
                        <div class="row">
                            <div class="col-lg-12 col-sm-12 profile-details">
                               <h4>Sevices</h4>
                                <div class="row">
                                    <div class="col-lg-4 col-sm-12"> 
                                        <span class="dot-icon">Cervical Cerclage</span>
                                    </div>
                                    <div class="col-lg-4 col-sm-12"> 
                                        <span class="dot-icon">Cervical Cerclage</span>
                                    </div>
                                    <div class="col-lg-4 col-sm-12"> 
                                        <span class="dot-icon">Cervical Cerclage</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-12 right-box">
                    <div class="v-add">
                        <span>Add area</span>
                    </div>
                </div>
            </div>
        </div>
    </section>


@stop
@section('js') 
<script src="{{asset('public/web/js/slick.min.js')}}"></script>
<script src="{{asset('public/web/js/pets.js')}}"></script>
@stop
