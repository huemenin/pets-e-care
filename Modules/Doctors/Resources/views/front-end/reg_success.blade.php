<div id="reg_success" class="modal fade email-popup " tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                  <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="logo-box">
                            <img src="{{asset('public/web/images/logo.png')}}" alt="">
                        </div>
                        <h3>A Verification link has been sent to your email address.</h3>
                        <p>Please click on the link that just been sent to yor email account to verify your email and continue the registration process.</p>
                    </div>
                  </div>  
</div>