 <!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> Pets e care :: Email verification success </title>
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    <link rel="stylesheet" href="{{asset('public/web/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('public/web/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('public/web/fonts/gotham/stylesheet.css')}}">
    <link rel="stylesheet" href="{{asset('public/web/css/fontawesome-all.css')}}">
    <link rel="stylesheet" href="{{asset('public/web/css/slick.css')}}">   
</head>
<body>
<!--    <div class="container">
        <div class="h-add hedarea">
           <div class="contant">
               <span>Add area</span>
           </div>
       </div>
    </div>-->
    <header>
        <div class="container">
            <nav class="navbar navbar-expand-lg">
                <a class="navbar-brand logo" href="#"> <img src="{{asset('public/web/images/logo.png')}}" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                     
                </div>
            </nav>
        </div>
    </header>
    <section class="banner inner">
        <div class="banner-contant">
             <h3>Please complete your profile</h3>
             <div style="color: #fff"> <img src="{{asset('public/web/images/success-icon.png')}}" alt="" style="margin-right:15px;" width="26px">Email verification success!</div>
           
        </div>
        <div class="banner-image">
            <div class="banner-slide-inner">
                <img src="{{asset('public/web/images/banner-inner.jpg')}}" alt="">
            </div>
        </div>
        <div class="banner-gradient"></div>
    </section>
    <div id="sectionCtr">
      <section class="inner">
        <div class="container" style="padding-top: 10px;padding-bottom: 10px;background: #eef3f7">
           <form method="post" autocomplete="off" name="proceed" id="proceed" action="{{URL('/doctors/proceed/')}}/{{$id}}"  enctype="multipart/form-data"  >
             
            <div class="form-area-dr">
                
               
                
                <div class="row pt-5">
                    <div class="col-lg-6 col-sm-6">
                        <div class="group input-wrap">      
                            <input type="text" disabled="" value="{{$doctors->name}}" class="w-100 bg-transparent">
                         
                        </div>
                    </div>
                    
                    <div class="col-lg-6 col-sm-6">
                        <div class="group input-wrap">      
                            <input type="text" disabled="" value="{{$doctors->email}}" class="w-100 bg-transparent">
                          
                        </div>
                    </div>
                    
                </div>
                
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="group input-wrap mobileBox">      
                            <input type="text" name="mobile" class="w-100 bg-transparent">
                          <label>Mobile No.</label>
                           <span class="has-error"></span>
                        </div>
                    </div>
                    
                    <div class="col-lg-6 col-sm-6">
                        <div class="group input-wrap council_reg_noBox">      
                            <input type="text" name="council_reg_no" class="w-100 bg-transparent">
                          <label>Council Reg.No<span>*</span></label>
                           <span class="has-error"></span>
                        </div>
                    </div>
                </div>
                
                 <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="group input-wrap experienceBox">      
                            <input type="text" name="experience" class="w-100 bg-transparent">
                          <label>Experience <span>*</span></label>
                           <span class="has-error"></span>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="group input-wrap servicesBox">      
                            <input type="text" name="services" class="w-100 bg-transparent">
                          <label>Services ( please separate with commas (,) )<span>*</span></label>
                         <span class="has-error"></span> 
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-lg-6 col-sm-6">
                        <div class="group input-wrap stateBox">      
                            <input type="text" name="state" class="w-100 bg-transparent">
                          <label>Registration state<span>*</span> </label>
                          
                           <span class="has-error"></span>
                        </div>
                        
                    </div>
                    <div class="col-lg-6 col-sm-6">
                        <div class="group input-wrap addressBox">      
                            <input type="text" name="address" class="w-100 bg-transparent">
                          <label>Residential address<span>*</span></label>
                           <span class="has-error"></span>
                        </div>
                    </div>
                </div>
                <div id="msg" align='center'> </div>
                <button class="btn proceed"  style="border: 1px solid #01dbba;    padding: 20px 70px;display: block; background: #01dbba; color: #2c4a9f; width: 224px; margin: 24px auto 25px auto; border-radius: 50px; font-size: 0.938em; font-weight: bold;">Proceed</button>
            </div>
          </form>   
        </div> 
    </section>
    </div>
   <script src="{{asset('public/admin/plugins/jquery/dist/jquery.min.js')}}"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <!-- Common  -->
    <script src="{{asset('public/web/js/bootstrap.min.js')}}"></script>
     <script src="{{asset('public/web/js/Common.js')}}"></script>
    <script src="{{asset('Modules/Website/Resources/assets/app/js/doctors_proceed.js')}}"></script>
    
</body>

</html>