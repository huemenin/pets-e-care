<?php

namespace Modules\Doctors\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Modules\Doctors\Entities\Doctor;
use Modules\Doctors\Entities\DoctorQualifications;
use Modules\Doctors\Entities\DoctorExperience;
use Modules\Doctors\Entities\DoctorExtras;
use Modules\Doctors\Entities\DoctorPublications;
use Mail;
use Config; 
class AdminDoctorsController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('doctors::admin.index');
    }

    public function AllDoctors()
    {
        return Datatables::of(Doctor::orderBy('id', 'DESC')->get())->make(true);
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('doctors::admin.create');
    }

     public function edit($id)
    {
        $doctors = Doctor::with('ug_qualifications')->with('pg_qualifications')->with('experiences')->with('expertises')->with('achievements')->with('services')->find($id);
        if($doctors==null){return redirect('/admin/404');}
        else{return view('doctors::admin.edit',compact('doctors'));  } 
    }


     public function store(Request $request)
    {   
        $time="";
        if($request->has('day') && !empty($request->day))
        {
            $obj = array();
            foreach($request->day as $key => $day)
            {
                $obj[$key]['day'] = $day;
                $obj[$key]['from'] = $request->from_time[$day];
                $obj[$key]['to'] = $request->to_time[$day];
            }
            $time = json_encode($obj);
        }
        if ($request->input('mobile'))  { $rules = ['email' => 'required|unique:doctors,email,NULL,id','mobile' => 'required|unique:doctors,mobile,NULL,id'];}
        else { $rules = ['email' => 'required|unique:doctors,email,NULL,id']; }
        $this->validate($request, $rules); 
        
        $doctor                  =   new Doctor;
        $doctor->name            =   $request->name; 
        $doctor->category            =   $request->category; 
        $doctor->experience            =   $request->experience;  
        $doctor->email           =   $request->email; 
        $doctor->mobile          =   $request->mobile; 
        $password               =   str_random(6);
        $doctor->password        =   bcrypt($password);  
        $doctor->location        =   $request->location;  
        $doctor->latitude        =   $request->latitude;  
        $doctor->longitude        =   $request->longitude;  
        $doctor->service_time        =   $time;  
        $doctor->council_reg_no        =   $request->council_reg_no;  
        $doctor->reg_validity        =   $request->reg_validity!="" ? date('Y-m-d',strtotime($request->reg_validity)) : '';  
        $doctor->reg_state        =   $request->reg_state;   
        if ($request->hasFile('ProfileImage'))
        {
            $image = $request->file('ProfileImage');
            $name = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/doctors/img');
            $image->move($destinationPath, $name);   
            $doctor->image =$name;
        }
        $doctor->resident_address =   $request->residential_address; 
        $doctor->clinical_address         =   $request->clinical_address; 
        $doctor->status          =   $request->Status; 
        $doctor->is_admin        =   1; 
        $doctor->created_ip      =   $request->ip(); 
                
        
        try
        {
            $doctor->save();
            $doc = Doctor::find($doctor->id);
            $number = "PECD".str_pad($doctor->id, 3, '0', STR_PAD_LEFT);
            $doc->unique_id = $number;
            $doc->save();
            if($request->has('qualification') && !empty($request->qualification))
            {
                $qualifications = $request->qualification;
                foreach($qualifications as $key =>$qualification){
                    if($qualification!=""):
                        $quali = new DoctorQualifications;
                        $quali->doctor_id = $doctor->id;
                        $quali->type = "ug";
                        $quali->qualification = $qualification;
                        $quali->college = $request->qualified_hospital[$key];
                        $quali->year = $request->passed_year[$key];
                        $quali->save();
                    endif;
                }
            }
            if($request->has('pg_qualification') && !empty($request->pg_qualification))
            {
                $qualifications = $request->pg_qualification;
                foreach($qualifications as $key =>$qualification){
                    if($qualification!=""):
                        $quali = new DoctorQualifications;
                        $quali->doctor_id = $doctor->id;
                        $quali->type = "pg";
                        $quali->qualification = $qualification;
                        $quali->college = $request->pg_hospital[$key];
                        $quali->year = $request->pg_passed_year[$key];
                        $quali->save();
                    endif;
                }
            }           

            if($request->has('experience_designation') && !empty($request->experience_designation))
            {
                $designations = $request->experience_designation;
                foreach($designations as $key =>$designation){
                    if($designation!=""):
                        $exp = new DoctorExperience;
                        $exp->doctor_id = $doctor->id;
                        $exp->from_year = $request->exp_from[$key];
                        $exp->to_year = $request->exp_to[$key];
                        $exp->designation = $designation;
                        $exp->hospital = $request->experience_hospital[$key];
                        $exp->save();
                    endif;
                }
            } 
            if($request->has('expertise') && !empty($request->expertise))
            {
                $expertises = $request->expertise;
                foreach($expertises as $key =>$expertise){
                    if($expertise!=""):
                        $extra = new DoctorExtras;
                        $extra->doctor_id = $doctor->id;
                        $extra->type = "expertise";
                        $extra->value = $expertise;
                        $extra->save();
                    endif;
                }
            } 
             if($request->has('achievement') && !empty($request->achievement))
            {
                $achievements = $request->achievement;
                foreach($achievements as $key =>$achievement){
                    if($achievement!=""):
                        $extra = new DoctorExtras;
                        $extra->doctor_id = $doctor->id;
                        $extra->type = "achievement";
                        $extra->value = $achievement;
                        $extra->save();
                    endif;
                }
            } 
             if($request->has('service') && !empty($request->service))
            {
                $services = $request->service;
                foreach($services as $key =>$service){
                    if($service!=""):
                        $extra = new DoctorExtras;
                        $extra->doctor_id = $doctor->id;
                        $extra->type = "service";
                        $extra->value = $service;
                        $extra->save();
                    endif;
                }
            } 
            if($request->has('publish_title') && !empty($request->publish_title))
            {
                $titles = $request->publish_title;
                if($request->hasfile('publish_file'))
                 {

                    foreach($request->file('publish_file') as $file)
                    {
                        $name = time().'.'.$file->getClientOriginalExtension();
                        $destinationPath = public_path('/doctors/publications');
                        $file->move($destinationPath, $name);   
                        $images[] = $name;  
                    }
                 }
                foreach($titles as $key =>$title){
                    if($title!=""):
                        $publish = new DoctorPublications;
                        $publish->doctor_id = $doctor->id;
                        $publish->paper_title = $title;
                        $publish->document = $images[$key];
                        $publish->save();
                    endif;
                }
            }
            // send email with password to doctor
            $data=array('password' => $password,'name'=>$request->name);
            $dname = "";
            $to_email = $request->email;
            Mail::send('doctors::emails.confirmation',  $data, function($message) use ($to_email, $dname)
            {
                $message->from(Config::get('constants.from'), Config::get('constants.mail_header'));
                $message->to($to_email);
                $message->subject(Config::get('constants.subject'));

            });
            
            $request->session()->flash('val', 1);
            $request->session()->flash('msg', "Doctor updated successfully !");
            return response()->json(['status'=>true,'url'=>URL('/doctors/'),'csrf' => csrf_token()]);
        }
        catch (\Exception $e)
        {
            $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
            return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);
   
        }
        
        
        
    }

    public function update($id,Request $request)
    {

        $time="";
        if($request->has('day') && !empty($request->day))
        {
            $obj = array();
            foreach($request->day as $key => $day)
            {
                $obj[$key]['day'] = $day;
                $obj[$key]['from'] = $request->from_time[$day];
                $obj[$key]['to'] = $request->to_time[$day];
            }
            $time = json_encode($obj);
        }
        
        if ($request->input('mobile'))  { $rules = ['email' => "required|unique:doctors,email,$id,id",'mobile' => "required|unique:doctors,mobile,$id,id"];}
        else { $rules = ['email' => "required|unique:doctors,email,$id,id"]; }
        $this->validate($request, $rules); 
        
        $doctor                  =   Doctor::find($id);
        if($doctor==null){return response()->json(['status'=>true,'url'=>URL('/admin/404'),'csrf' => csrf_token()]);}
        else
        {
            $doctor->name            =   $request->name; 
            $doctor->category            =   $request->category; 
            $doctor->experience            =   $request->experience;  
            $doctor->email           =   $request->email; 
            $doctor->mobile          =   $request->mobile; 
            $password               =   str_random(6);
            $doctor->password        =   bcrypt($password);  
            $doctor->location        =   $request->location;  
            $doctor->latitude        =   $request->latitude;  
            $doctor->longitude        =   $request->longitude;  
            $doctor->service_time        =   $time;  
            $doctor->council_reg_no        =   $request->council_reg_no;  
            $doctor->reg_validity        =   ($request->reg_validity!="") ? date('Y-m-d',strtotime($request->reg_validity)) : '';  
            $doctor->reg_state        =   $request->reg_state;   
            if ($request->hasFile('ProfileImage'))
            {
                $image = $request->file('ProfileImage');
                $name = time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/doctors/img');
                $image->move($destinationPath, $name);   
                $doctor->image =$name;
            }
            $doctor->resident_address =   $request->residential_address; 
            $doctor->clinical_address         =   $request->clinical_address; 
            $doctor->status          =   $request->Status; 
            $doctor->is_admin        =   1; 
            $doctor->created_ip      =   $request->ip();   
            try
            {
                $doctor->save();
                // send email with password to owner
                DoctorQualifications::where('doctor_id',$id)->delete();
                if($request->has('qualification') && !empty($request->qualification))
                {
                    $qualifications = $request->qualification;
                    foreach($qualifications as $key =>$qualification){
                        if($qualification!=""):
                            $quali = new DoctorQualifications;
                            $quali->doctor_id = $doctor->id;
                            $quali->type = "ug";
                            $quali->qualification = $qualification;
                            $quali->college = $request->qualified_hospital[$key];
                            $quali->year = $request->passed_year[$key];
                            $quali->save();
                        endif;
                    }
                }

                if($request->has('pg_qualification') && !empty($request->pg_qualification))
                {
                    $qualifications = $request->pg_qualification;
                    foreach($qualifications as $key =>$qualification){
                        if($qualification!=""):
                            $quali = new DoctorQualifications;
                            $quali->doctor_id = $doctor->id;
                            $quali->type = "pg";
                            $quali->qualification = $qualification;
                            $quali->college = $request->pg_hospital[$key];
                            $quali->year = $request->pg_passed_year[$key];
                            $quali->save();
                        endif;
                    }
                }

                DoctorExperience::where('doctor_id',$id)->delete();
                if($request->has('experience_designation') && !empty($request->experience_designation))
                {
                    $designations = $request->experience_designation;
                    foreach($designations as $key =>$designation){
                        if($designation!=""):
                            $exp = new DoctorExperience;
                            $exp->doctor_id = $doctor->id;
                            $exp->from_year = $request->exp_from[$key];
                            $exp->to_year = $request->exp_to[$key];
                            $exp->designation = $designation;
                            $exp->hospital = $request->experience_hospital[$key];
                            $exp->save();
                        endif;
                    }
                }

                DoctorExtras::where('doctor_id',$id)->delete();

                if($request->has('expertise') && !empty($request->expertise))
                {
                    $expertises = $request->expertise;
                    foreach($expertises as $key =>$expertise){
                        if($expertise!=""):
                            $extra = new DoctorExtras;
                            $extra->doctor_id = $doctor->id;
                            $extra->type = "expertise";
                            $extra->value = $expertise;
                            $extra->save();
                        endif;
                    }
                } 
                if($request->has('achievement') && !empty($request->achievement))
                {
                    $achievements = $request->achievement;
                    foreach($achievements as $key =>$achievement){
                        if($achievement!=""):
                            $extra = new DoctorExtras;
                            $extra->doctor_id = $doctor->id;
                            $extra->type = "achievement";
                            $extra->value = $achievement;
                            $extra->save();
                        endif;
                    }
                } 
                 if($request->has('service') && !empty($request->service))
                {
                    $services = $request->service;
                    foreach($services as $key =>$service){
                        if($service!=""):
                            $extra = new DoctorExtras;
                            $extra->doctor_id = $doctor->id;
                            $extra->type = "service";
                            $extra->value = $service;
                            $extra->save();
                        endif;
                    }
                }  

                $request->session()->flash('val', 1);
                $request->session()->flash('msg', "Doctor created successfully !");
                return response()->json(['status'=>true,'url'=>URL('/admin/doctors/'),'csrf' => csrf_token()]);
            }
            catch (\Exception $e)
            {
                $html='<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button><h4><i class="icon fa fa-ban">Alert!</i></h4>'.$e->getMessage().'</div>';
                return response()->json(['status'=>FALSE,'alert'=>$html,'message'=>$e->getMessage(),'csrf' => csrf_token()]);

            }
        }
        
        
    }

    
}

