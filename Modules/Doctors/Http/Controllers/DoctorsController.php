<?php

namespace Modules\Doctors\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Modules\Doctors\Entities\Doctor;
use Modules\Doctors\Entities\DoctorsServices;
use \Crypt;
use Dirape\Token\Token;
use Mail;
use URL;
use Config;
use Auth;
use Cache;
use Session;

class DoctorsController extends Controller
{
 
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    { 
        return view('doctors::front-end.profile');
    }
    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
         if($request->type)
        {
            if($request->type == 'doctor')
            {
                $this->validate($request, [ 'email' => 'required|unique:doctors,email,NULL,id,deleted_at,NULL']);
                $doctor                  =   new Doctor;
                $doctor->name            =   $request->name; 
                $doctor->email           =   $request->email; 
                $doctor->password        =   bcrypt($request->password); 
                $doctor->status          =   0; 
                $doctor->created_ip      =   $request->ip(); 
                $Tokens                  =   new Token();
                $token                   =  $Tokens->Random(16);
                $doctor->email_verification_id	=   $token;
                try
                {
                    $doctor->save();
                    $id = $doctor->id;
                    $toEmail=   $request->email; 
                    $contactName='';
                  /* send verification email */
                    $encrypted = Crypt::encrypt($token."==*==".$id);
                    $data=array('key' => $encrypted,'name'=>$request->name);
                    Mail::send('doctors::emails.verification',  $data, function($message) use ($toEmail, $contactName)
                    {
                        $message->from(Config::get('constants.from'), Config::get('constants.mail_header'));
                        $message->to($toEmail);
                        $message->subject(Config::get('constants.subject_doctor_reg'));

                    });
                    if( count(Mail::failures()) > 0 ) 
                    { 
                         
                        return response()->json(['status'=>0,'message'=>"some problem occured to send verification link to your email id",'csrf' => csrf_token()]);
                    } 
                    else {
                           $message = (String) view('doctors::front-end.reg_success');                    
                           return response()->json(['status'=>1,'message'=>$message,'csrf' => csrf_token()]); }

                } 
                catch (Exception $ex)
                { 
                    $message="Sorry Somthing went wrong, please try again later"; 
                    return response()->json(['status'=>0,'message'=>$message,'csrf' => csrf_token()]);
                }
            }else{return response()->json(['status'=>2,'url'=>URL('/404'),'csrf' => csrf_token()]);}
        }else
        {
            return response()->json(['status'=>2,'url'=>URL('/404'),'csrf' => csrf_token()]);
        }
    }
    
    /**
     * Show the specified resource.
     * @return Response
     */
    public function verify($id)
    {
        $decrypted = Crypt::decrypt($id); 
        $key_id = explode('==*==', $decrypted, 2);
        $doctors = Doctor::where('email_verification_id',$key_id[0])->find($key_id[1]);
       
        if($doctors != null)
        { 
            if($doctors->is_email == 0)
            {  
                try{ 
                    $doctors->is_email = 1;
                    $doctors->status = 2;
                    $doctors->save();
                    return view('doctors::front-end.verification_success',compact('doctors','id'));
                  
                } catch (Exception $ex) {return view('doctors::front-end.verification_fail');  }
               
            }else{return view('doctors::front-end.verification_fail'); }
        }else{ return redirect(URL('/404')); }
    }
    
    /**
     * Show the specified resource.
     * @return Response
     */
    public function proceed($id, Request $request) {

        $this->validate($request, [
                    'mobile' => 'required',
                    'council_reg_no' => 'required',
                    'experience' => 'required',
                    'state' => 'required',
                    'address' => 'required',
                    'services' => 'required'
                ]);
        
        $decrypted = Crypt::decrypt($id); 
        $key_id = explode('==*==', $decrypted, 2);
        $doctors = Doctor::where('email_verification_id',$key_id[0])->find($key_id[1]);
        if($doctors != null)
        {
            $doctors->mobile            =   $request->mobile; 
            $doctors->council_reg_no    =   $request->council_reg_no; 
            $doctors->experience        =   $request->experience; 
            $doctors->reg_state         =   $request->state; 
            $doctors->resident_address  =   $request->address; 
            $doctors->status            =   3;
            $services                   =   explode(",",$request->services);  
            foreach ($services as $key => $value)
            {
                if(strlen(trim($value)) > 0)
                {
                    $dServices = new DoctorsServices;
                    $dServices->doctor_id=$doctors->id;
                    $dServices->name=$value; 
                    $dServices->save();
                }     
           }
             try{
                $doctors->save();
                $message = (String) view('doctors::front-end.proceed_step_success');                    
                return response()->json(['status'=>1,'message'=>$message,'csrf' => csrf_token()]); 

             } catch (Exception $ex) {
                 
                $message="Sorry Somthing went wrong, please try again later"; 
                return response()->json(['status'=>0,'message'=>$message,'csrf' => csrf_token()]); 

             }
            
        } else{   return response()->json(['status'=>2,'url'=>URL('/404'),'csrf' => csrf_token()]); }
    }
    
    /**
     * Show the specified resource.
     * @return Response
     */
    public function login(Request $request)
    {
        
         if($request->type)
        {
            if($request->type == 'doctor')
            {
                 $this->validate($request, [
                    'logEmail' => 'required|email',
                    'logPassword' => 'required'
                ]);

            
                $credentials['status']  =   1;
                $credentials['is_email']  =   1; 
                $credentials['email']  =   $request->logEmail;
                $credentials['password']  =   $request->logPassword;
                 
                 
                if (Auth::guard('doctor')->attempt($credentials, $request->has('remember')))
                {
                    Auth::guard('owner')->logout();
                    Cache::flush();
                    Session::put('user_type','doctor');
                    return response()->json(['status'=>1,'csrf' => csrf_token(),'url'=>URL::to('/doctors/')]);   
                }
                else
                {
                    return response()->json(['status' => 0, 'message' => trans('owners::auth.failed')]); 
                }
                
            }else{return response()->json(['status'=>2,'url'=>URL('/404'),'csrf' => csrf_token()]);}
                 
            
        }else
        {
            return response()->json(['status'=>2,'url'=>URL('/404'),'csrf' => csrf_token()]);
        }
    }
    
    
     /**
     * logout for admin users.
     * @return Response
     */
    public function logout()
    {
        Auth::guard('doctor')->logout();
        Cache::flush();
        Session::flush();
        return redirect('/');
    }
    
    
    

}
