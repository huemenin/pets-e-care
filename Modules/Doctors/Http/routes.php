<?php

Route::group(['middleware' => 'web', 'prefix' => 'admin/doctors', 'namespace' => 'Modules\Doctors\Http\Controllers'], function()
{
//    Route::get('/', 'DoctorsController@index');
 
    /* logged users opertaions */
    Route::group(['middleware' =>  'admin_auth:admin'], function()
    {
        Route::get('/', 'AdminDoctorsController@index');
        Route::get('/AdminDoctorsList',[ 'middleware' => 'check_perm:view-doctors', 'uses' => 'AdminDoctorsController@AllDoctors', ]); 
        Route::get('/add', ['middleware' => 'check_perm:create-doctors', 'uses' => 'AdminDoctorsController@create']);
        Route::post('/create', ['middleware' => 'check_perm:create-doctors', 'uses' => 'AdminDoctorsController@store']);
       	Route::get('/edit/{id}', ['middleware' => 'check_perm:edit-doctors', 'uses' => 'AdminDoctorsController@edit']);
        Route::post('/update/{id}', ['middleware' => 'check_perm:edit-doctors', 'uses' => 'AdminDoctorsController@update']);
        
    });
});




Route::group(['middleware' => 'web', 'prefix' => 'doctors', 'namespace' => 'Modules\Doctors\Http\Controllers'], function()
{
    /* website users opertaions */
    Route::post('/register', 'DoctorsController@store');
    Route::get('/verification/{id}', 'DoctorsController@verify');
    Route::post('/login', 'DoctorsController@login');
    Route::get('/logout', 'DoctorsController@logout');
    Route::post('/proceed/{id}', 'DoctorsController@proceed');
    
       /* logged  doctor users opertaions */
    Route::group(['middleware' =>  'doctor_auth:doctor'], function()
    {   
        
        Route::get('/', 'DoctorsController@index');   
        
    });
});
