<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255)->collation('utf8mb4_unicode_ci')->nullable(false);
            $table->string('category',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->integer('experience')->nullable();
            $table->string('email',255)->collation('utf8mb4_unicode_ci')->nullable(false);
//            $table->string('unique_id',255)->collation('utf8mb4_unicode_ci')->nullable(false);
            $table->string('email_verification_id',255)->nullable();
            $table->integer('is_email')->default('0')->comment('0-not verified,1-verified')->nullable();
            $table->string('mobile',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('password',255)->collation('utf8mb4_unicode_ci')->nullable(false);
            $table->string('image',255)->collation('utf8mb4_unicode_ci')->nullable(); 
            $table->string('location',255)->collation('utf8mb4_unicode_ci')->nullable(); 
            $table->string('latitude',255)->collation('utf8mb4_unicode_ci')->nullable(); 
            $table->string('longitude',255)->collation('utf8mb4_unicode_ci')->nullable(); 
            $table->string('council_reg_no',255)->collation('utf8mb4_unicode_ci')->nullable(); 
            $table->date('reg_validity')->nullable(); 
            $table->string('reg_state',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->text('resident_address')->collation('utf8mb4_unicode_ci')->nullable();
            $table->text('clinical_address')->collation('utf8mb4_unicode_ci')->nullable();
            $table->tinyInteger('status')->default('0')->comment('0-inactive , 1-active, 2-need to complete profile, 3-profile completed')->nullable(false);
            $table->integer('is_admin')->unsigned()->comment('1-if admin created the owner')->nullable();
            $table->foreign('is_admin')->references('id')->on('admin_users');
            $table->string('created_ip',255)->collation('utf8mb4_unicode_ci')->nullable(false);
            $table->string('deleted_ip',255)->collation('utf8mb4_unicode_ci')->nullable();
            $table->string('remember_token',100)->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
