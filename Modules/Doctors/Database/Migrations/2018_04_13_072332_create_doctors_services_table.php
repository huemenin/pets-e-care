<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors_services', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',255)->collation('utf8_unicode_ci')->nullable(false); 
            $table->integer('doctor_id')->unsigned()->nullable(false);
            $table->foreign('doctor_id')->references('id')->on('doctors') ->onDelete('cascade');; 
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable();
            $table->softDeletesTz(); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors_services');
    }
}
