function ConfirmDelete()
{
      var x = confirm("Are you sure you want to delete?");
      if (x)
          return true;
      else
        return false;
}
   
/* ************************************************************************** */  
/* ************************************************************************** */ 
 
function generate_slug(str) {
    var $slug = '';
    var trimmed = $.trim(str);
    $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').
    replace(/-+/g, '-').
    replace(/^-|-$/g, '');
    return $slug.toLowerCase();
}


$(function() 
{
 
/* ************************************************************************** */  
/* ************************************************************************** */ 
 
 $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
 
/* ************************************************************************** */  
/* ************************************************************************** */ 
 
});
