$(document).ready(function () {

    $(".bc-main").unbind('click').click(function () {
        if ($(".bc-main").hasClass("active")) {
            $(".bc-main").removeClass("active");
            $(".menu-dropdown").removeClass("active");
        } else {
            $(".bc-main").addClass("active");
            $(".menu-dropdown").addClass("active");
        }
    });

    if ($(".banner-slide").length) {
        $(".banner-slide").not('.slick-initialized').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            arrows: false,
            dots: true
        });
    }

    if ($(".slide-box-slider").length) {

        $(".slide-box-slider").not('.slick-initialized').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 2000,
            arrows: true,
            fade: true,
            cssEase: 'linear'
        });
    }

});
//edit popup start
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image_upload_preview, #image_upload_preview2').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
if ($("#inputFile,#inputFile2").length) 
{
$("#inputFile, #inputFile2").change(function () {
    readURL(this);
});
}
//edit popup end
